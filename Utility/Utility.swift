

import UIKit
import AssetsLibrary
import AVFoundation

class Utility {
     func getActualValue(key : String, dict : [String : Any]) -> String {
        if dict[key] is String {
            return dict[key] as! String
        } else if dict[key] is Int {
            return String(dict[key] as! Int)
        } else if dict[key] is Float {
            return String(format: "%.2f", dict[key] as! Float)
        } else if dict[key] is Double {
            return String(format: "%.2f", dict[key] as! Double)
        } else if dict[key] is Bool {
            return String(dict[key] as! Bool)
        } else {
            return ""
        }
    }
    
    static func tokenExpiredAction() {
        
    }
    
    static func logoutAction() {
        Defaults.isUserLoggedIn = false
        Defaults.isFirstTimeLogin = false
        var navigationController : UINavigationController?
        Defaults.userPhoneNumber = ""
        Defaults.Token = ""
        Defaults.userId = ""
        Defaults.userName = ""
        Defaults.userEmail = ""
        Defaults.userImageUrl = ""
        Defaults.userId = ""
        let loginView = LoginView()
        navigationController = UINavigationController(rootViewController: loginView)
        navigationController?.isNavigationBarHidden = true
        let sceneDelegate = SceneDelegate.getSceneDelegate()
        sceneDelegate.window!.rootViewController = navigationController
    }
    
    static func gotoTabbar() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SideMenuController") as! SideMenuController
        let sceneDelegate = SceneDelegate.getSceneDelegate()
        sceneDelegate.window!.rootViewController = viewController
    }
    
    static func gotoInitialView() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "InitialViewController") as! InitialViewController
        let sceneDelegate = SceneDelegate.getSceneDelegate()
        sceneDelegate.window!.rootViewController = viewController
    }
    
    static func gotoLoginView() {
        let loginView = LoginView.init(nibName: "LoginView", bundle: nil)
        SceneDelegate.getSceneDelegate().navigationController = UINavigationController(rootViewController: loginView)
        SceneDelegate.getSceneDelegate().navigationController?.isNavigationBarHidden = true
        SceneDelegate.getSceneDelegate().window!.rootViewController = SceneDelegate.getSceneDelegate().navigationController
    }
    
    static func getUniqueName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmssSSSS"
        let dateString = dateFormatter.string(from: Date())
        return dateString
    }
    
    static func getMediaDirectoryPath() -> NSURL {
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("UploadMedia")
        if !fileManager.fileExists(atPath: path) {
            try! fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        return NSURL(string: path)!
    }
    
    static func saveImageDocumentDirectory(image: UIImage?) -> String {
        let imageNamePng = Utility.getUniqueName() + ".png"
        let url = Utility.getMediaDirectoryPath()
        let imagePath = url.appendingPathComponent(imageNamePng)
        let urlString: String = imagePath!.absoluteString
        print("Image Path : \(urlString)")
        let imageData = image!.jpegData(compressionQuality: 0.5)
        FileManager.default.createFile(atPath: urlString as String, contents: imageData, attributes: nil)
        return urlString
    }
    
    static var isDarkMode: Bool {
        guard #available(iOS 12.0, *) else {
            return false
        }
        let sceneDelegate = SceneDelegate.getSceneDelegate()
        return sceneDelegate.window?.traitCollection.userInterfaceStyle == .dark
    }
    
    static func saveImageDocumentDirectory(dataUrl : String) -> String {
        let imageNamePng = Utility.getUniqueName() + ".png"
        let url = Utility.getMediaDirectoryPath()
        let imagePath = url.appendingPathComponent(imageNamePng)
        let urlString: String = imagePath!.absoluteString
        print("Image Path : \(urlString)")
        do  {
            let imageData = try Data(contentsOf: URL(string: dataUrl)!)
            FileManager.default.createFile(atPath: urlString as String, contents: imageData, attributes: nil)
        } catch {
        }
        return urlString
    }
    
    static func getValidUrl(url : String) -> String {
        return url.addingPercentEncoding(withAllowedCharacters:  NSCharacterSet.urlQueryAllowed)!
    }
    
    static func callInitialData() {
        let profileOperation = ProfileOperation()
        profileOperation.getUserDetails(userId: Defaults.userId ?? "") { error, userDetails in
            Defaults.userName = userDetails?["fname"] as? String ?? ""
            Defaults.userEmail = userDetails?["email"] as? String ?? ""
            Defaults.userPhoneNumber = userDetails?["mobile"] as? String ?? ""
            let imageArray = userDetails?["profileimage"] as? [[String : Any]] ?? []
            if !imageArray.isEmpty {
                let imageDict = imageArray[0]
                Defaults.userImageUrl =  "\(BaseURL)/images/\(imageDict["filename"] as? String ?? "")"
            }
            NotificationCenter.default.post(name: Notification.Name("ProfileUpdated"), object: nil)
        }
        
        profileOperation.getAllUsers { error, users in
            GlobalData.allUsers = users ?? []
        }
        
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.execute { str, categories in
            GlobalData.allCategories = categories ?? []
        }
    }
    
    class func openGoogleMap(pickup_lat:Double, pickup_long:Double) {
        let googleMapsInstalled = UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
        if googleMapsInstalled {
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {  //if phone has an app
                
                if let url = URL(string: "comgooglemaps-x-callback://?saddr=&daddr=\(pickup_lat),\(pickup_long)&directionsmode=driving") {
                    UIApplication.shared.open(url, options: [:])
                }}
            else {
                //Open in browser
                if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(pickup_lat),\(pickup_long)&directionsmode=driving") {
                    UIApplication.shared.open(urlDestination)
                }
            }
        } else {
            guard let url = URL(string:"http://maps.apple.com/?daddr=\(pickup_lat),\(pickup_long)") else { return }
            UIApplication.shared.open(url)
        }
    }
    
    static func applyTaskFilter(allTasks : [DetailDataModel]) -> [DetailDataModel] {
        let tasks = allTasks.filter( { $0.userId == Defaults.userId || $0.assignId!.contains(where: { $0["userId"] as? String == Defaults.userId && $0["assigneeStatus"] as? String == "Accepted"}) } )
        return tasks
    }

    static func applyTaskFilterForMyRequest(allTasks : [DetailDataModel]) -> [DetailDataModel] {
        let tasks = allTasks.filter( { $0.userId != Defaults.userId && $0.assignId!.contains(where: { $0["userId"] as? String == Defaults.userId }) } )
        return tasks
    }
}


