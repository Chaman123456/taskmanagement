//
//  ThemeUtility.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 07/12/21.
//

import UIKit
import Foundation

class ThemeUtility {
    static func backgorundGradient(frame: CGRect) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = frame
        gradientLayer.colors = [UIColor(named: "BlueHeader")!.cgColor, UIColor(named: "HomeBackground")!.cgColor]
        gradientLayer.locations = [0.0, 0.2]
        return gradientLayer
    }
    
     static let ButtonBlackColor: UIColor = {
        if #available(iOS 13, *) {
            if Utility.isDarkMode {
                return UIColor(red: 108.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            } else {
                return UIColor(red: 55.0/255.0, green: 55.0/255.0, blue: 55.0/255.0, alpha: 1.0)
            }
        } else {
            return UIColor(red: 108.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        }
    }()
    
    static let TabbarBackgroundColor: UIColor = {
        if #available(iOS 13, *) {
            if Utility.isDarkMode {
                return UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0)
            } else {
                return UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0)
            }
        } else {
            return UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.0)
        }
    }()
    
}
