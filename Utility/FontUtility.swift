//
//  FontUtility.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 07/12/21.
//

import UIKit

struct FontUtility {
    enum TraitType: String {
        case SemiboldItalic
        case Italic
        case HeavyItalic
        case LightItalic
        case Regular
        case ThinItalic
        case Bold
        case Medium
        case UltraLightItalic
        
        func trait() -> String {
            return rawValue
        }
    }

    enum FontType {
        case display
        case text
        case custom(String)
        
        func value() -> String {
            var newFont = ""
            switch self {
            case .display:
                newFont = "SFProDisplay-"
            case .text:
                newFont = "SFProDisplay-"
            case .custom(let fontName):
                newFont = "\(fontName)-"
            }
            
            return newFont
        }
    }

    enum FontSize {
        case standard
        case heading
        case custom(CGFloat)
        
        var value: CGFloat {
            switch self {
            case .standard:
                return 16.0
            case .heading:
                return 28.0
            case .custom(let customSize):
                return customSize
            }
        }
    }
    
    static func getFont(with type: FontType, withTrait trait: TraitType, ofSize size: FontSize = .standard) -> UIFont {
        var fontName: String
        switch type {
        case .text:
            fontName = "\(type.value())\(trait.rawValue)"
        case .display:
            fontName = "\(type.value())\(trait.rawValue)"
        default:
            fontName = "\(type.value())\(trait.rawValue)"
        }
        return UIFont.init(name: fontName, size: size.value)!
    }
}
