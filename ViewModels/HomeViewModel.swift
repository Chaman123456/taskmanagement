//
//  HomeViewModel.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 12/12/21.
//

import UIKit
 
protocol HomeViewModelProtocol {
    func setupCategoryScorllDone(categoryArray: [Category])
    func populateCategries(categoryItemArray: [Any])
}

class HomeViewModel: NSObject {
    var delegate: HomeViewModelProtocol?
    let categoryItemArray = [
        [
            "header": "Recently Created",
            "names": ["House Party", "Swimming"],
            "images": ["party", "swimming"],
            "dates": ["12 Aug, 20:00", "12 Aug, 20:00"]
        ],
        [
            "header": "Today",
            "names": ["Groceries", "General Items", "Things to do"],
            "images": ["Grocery", "generalItems", "things"],
            "dates": ["12 Aug, 20:00", "12 Aug, 20:00", "12 Aug, 20:00"]
        ],
        [
            "header": "Upcoming",
            "names": ["Groceries", "General Items", "Things to do"],
            "images": ["Grocery", "generalItems", "things"],
            "dates": ["12 Aug, 20:00", "12 Aug, 20:00", "12 Aug, 20:00"]
        ],
        [
            "header": "Later",
            "names": ["Groceries", "General Items", "Things to do"],
            "images": ["Grocery", "generalItems", "things"],
            "dates": ["12 Aug, 20:00", "12 Aug, 20:00", "12 Aug, 20:00"]
        ],
        [
            "header": "Completed",
            "names": ["Groceries", "General Items", "Things to do"],
            "images": ["Grocery", "generalItems", "things"],
            "dates": ["12 Aug, 20:00", "12 Aug, 20:00", "12 Aug, 20:00"]
        ]
    ]
    

    func setupCategoryScorll(categoryArray: [Category]) {
        //let category = ["All Categories", "Groceries", "General Items"]
        self.delegate?.setupCategoryScorllDone(categoryArray: categoryArray)
    }
    
    func populateCategries(categoryName: [String]) {
        delegate?.populateCategries(categoryItemArray: categoryName)
    }
}
