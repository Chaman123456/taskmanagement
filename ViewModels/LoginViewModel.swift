//
//  LoginViewModel.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 09/12/21.
//

import UIKit

protocol LoginViewModelProtocol {
    func loginButtonPressed(sucess: Bool, error: String)
}

class LoginViewModel: NSObject {
    var delegate: LoginViewModelProtocol?
    
    func loginButtonAction(phoneNumber: String, email: String) {
        if email.isEmpty && phoneNumber.isEmpty {
            delegate?.loginButtonPressed(sucess: false, error: "Please enter email or phone number.")
            return
        }
        if !email.isEmpty && !Validation.isValidEmail(email) {
            delegate?.loginButtonPressed(sucess: false, error: "Please enter valid email address.")
            return
        }
        delegate?.loginButtonPressed(sucess: true, error: "")
    }
}
