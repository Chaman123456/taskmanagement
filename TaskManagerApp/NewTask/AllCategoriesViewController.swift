//
//  AllCategoriesViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 15/03/22.
//

import UIKit

class AllCategoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let refreshControl = UIRefreshControl()
    var homeViewObject: HomeView?
    var taskViewObject: TaskView?
    var priorityViewObject: PriorityView?
    var categoriesViewObject: CategoriesViewController?
    @IBOutlet var catTableView : UITableView?
    @IBOutlet var noCategoryLabel : UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCategories()
        refreshControl.addTarget(self, action: #selector(self.getCategories), for: .valueChanged)
        self.catTableView?.refreshControl = refreshControl
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            catTableView?.backgroundColor = .black
        } else {
            view.backgroundColor = UIColor.white
            catTableView?.backgroundColor = .white
        }
    }
    
    @objc func getCategories() {
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.execute { str, categories in
            GlobalData.allCategories = categories ?? []
            self.catTableView?.reloadData()
            self.refreshControl.endRefreshing()
            if GlobalData.allCategories.isEmpty {
                self.noCategoryLabel?.isHidden = false
            } else {
                self.noCategoryLabel?.isHidden = true
            }
        }
    }
    
    @IBAction func backButtonAction() {
        self.dismiss(animated: false)
        navigationController?.popViewController(animated: false)
    }
    
    @IBAction func moreAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Priority", bundle: nil)
        let vc = (sb.instantiateViewController(identifier: "PriortyFilterViewController")) as! PriortyFilterViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.clear
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func NotificationAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GlobalData.allCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllCategoriesTableViewCell", for: indexPath) as! AllCategoriesTableViewCell
        if self.traitCollection.userInterfaceStyle == .dark {
            cell.backgroundColor = .black
            cell.lblCategories.textColor = .white
            cell.mainBackground.backgroundColor = .black
        } else {
            cell.backgroundColor = .white
            cell.lblCategories.textColor = .black
            cell.mainBackground.backgroundColor = .white
        }
        cell.lblCategories.text = GlobalData.allCategories[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = homeViewObject {
            let index = homeViewObject!.categoryScroll?.itemArray.firstIndex(where: { cat in
                cat.name == GlobalData.allCategories[indexPath.row].name!
            })
            if let _ = index {
                homeViewObject!.categoryScroll!.updateItem(selectedValue: index!)
            } else {
                homeViewObject!.categoryScroll!.itemArray.remove(at: 1)
                homeViewObject!.categoryScroll!.itemArray.insert(GlobalData.allCategories[indexPath.row], at: 1)
                homeViewObject!.categoryScroll!.setupScroll(itemArray: homeViewObject!.categoryScroll!.itemArray)
                homeViewObject!.categoryScroll!.updateItem(selectedValue: 1)
            }
            homeViewObject!.categoryScroll!.categoryScrollDelegate?.categoryChoosed(selectedItemName:  GlobalData.allCategories[indexPath.row].name!)
        }
        if let _ = taskViewObject {
            let index = taskViewObject!.categoryScroll?.itemArray.firstIndex(where: { cat in
                cat.name == GlobalData.allCategories[indexPath.row].name!
            })
            if let _ = index {
                taskViewObject!.categoryScroll!.updateItem(selectedValue: index!)
            } else {
                taskViewObject!.categoryScroll!.itemArray.remove(at: 1)
                taskViewObject!.categoryScroll!.itemArray.insert(GlobalData.allCategories[indexPath.row], at: 1)
                taskViewObject!.categoryScroll!.setupScroll(itemArray: taskViewObject!.categoryScroll!.itemArray)
                taskViewObject!.categoryScroll!.updateItem(selectedValue: 1)
            }
            taskViewObject!.categoryScroll!.categoryScrollDelegate?.categoryChoosed(selectedItemName:  GlobalData.allCategories[indexPath.row].name!)
        }
        if let _ = priorityViewObject {
            let index = priorityViewObject!.categoryScroll?.itemArray.firstIndex(where: { cat in
                cat.name == GlobalData.allCategories[indexPath.row].name!
            })
            if let _ = index {
                priorityViewObject!.categoryScroll!.updateItem(selectedValue: index!)
            } else {
                priorityViewObject!.categoryScroll!.itemArray.remove(at: 1)
                priorityViewObject!.categoryScroll!.itemArray.insert(GlobalData.allCategories[indexPath.row], at: 1)
                priorityViewObject!.categoryScroll!.setupScroll(itemArray: priorityViewObject!.categoryScroll!.itemArray)
                priorityViewObject!.categoryScroll!.updateItem(selectedValue: 1)
            }
            priorityViewObject!.categoryScroll!.categoryScrollDelegate?.categoryChoosed(selectedItemName:  GlobalData.allCategories[indexPath.row].name!)
        }
        if let _ = categoriesViewObject {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectCategory"), object: GlobalData.allCategories[indexPath.row])
        }
        backButtonAction()
    }
}
