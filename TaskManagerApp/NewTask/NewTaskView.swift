//
//  NewTaskView.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 10/12/21.
//

import UIKit
import MobileCoreServices
import MapKit
import LocationPicker
import MobileCoreServices
import UniformTypeIdentifiers
import EventKit
import Toast

class NewTaskView: UIViewController, UITextFieldDelegate, CategoryDelegate, UITextViewDelegate {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblCat: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblAssignTo: UILabel!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var txtNotes: UITextField!
    @IBOutlet var customTopView: UIView!
    @IBOutlet var customDetailView: UIView!
    @IBOutlet var customDetailImgView: UIView!
    @IBOutlet var navTitle: UIBarButtonItem!
    @IBOutlet var tf_name: UITextField!
    @IBOutlet var tfCategory: UITextField!
    @IBOutlet var tfDescription: UITextField!
    @IBOutlet var imgCoverImage: UIImageView!
    @IBOutlet weak var timeDatePicker: UIDatePicker!
    @IBOutlet var collectionAssignTo: UICollectionView!
    @IBOutlet var collectionSubTask: UICollectionView!
    @IBOutlet var subtaskHeight: NSLayoutConstraint!
    @IBOutlet var collectionAttachment: UICollectionView!
    @IBOutlet var collectionMap: UICollectionView!
    @IBOutlet var titleAttachment: UILabel!
    @IBOutlet var attachmentHeight: NSLayoutConstraint!
    @IBOutlet var titleLocation: UILabel!
    @IBOutlet var locationHeight: NSLayoutConstraint!
    let locationPicker = LocationPickerViewController()
    var imagePicker: UIImagePickerController!
    var selectedDate = ""
    var selectedCategories = [Category]()
    var isAttachmentSelected = false
    var arrUser = [User]()
    var arrAttachment = [[String: Any]]()
    var arrSubTask = [UIImage]()
    var arrLocation = [[String: Any]]()
    var selectedLocationIndex : Int?
    //@IBOutlet var noteText: UITextField?
    @IBOutlet weak var noteText: UITextView!
    let eventStore = EKEventStore()
    var titles : [String] = []
    var startDates : [Date] = []
    var endDates : [Date] = []
    @IBOutlet var doneButton: UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectFilter(notification:)), name: Notification.Name("SelectFilter"), object: nil)
        coverImageGesture()
        setInitialUI()
        fetchCategories()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        //requestAccess()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedLocationIndex = nil
        navigationController?.isNavigationBarHidden = true
        let _ = self.validatedForPhoneNumber()
    }
    
    @objc func setUpMode() {
        checkDoneButton()

//        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.noteText!.frame.height))
//        noteText?.leftView = paddingView
//        noteText?.leftViewMode = UITextField.ViewMode.always
        
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            btnDone.titleLabel?.textColor = .white
            titleAttachment.textColor = .white
            titleLocation.textColor = .white
            customDetailView.backgroundColor = .black
            customDetailImgView.backgroundColor = .black
            customTopView.backgroundColor = UIColor(patternImage: UIImage(named: "bgImage.png")!)
            lblName.textColor = .gray
            tf_name.placeholderColor = .white
            tf_name.textColor = .white
            tf_name.tintColor = .white
            lblCat.textColor = .gray
            tfCategory.placeholderColor = .white
            tfCategory.textColor = .white
            tfCategory.tintColor = .white
            lblDesc.textColor = .gray
            tfDescription.placeholderColor = .white
            tfDescription.textColor = .white
            tfDescription.tintColor = .white
            lblDueDate.textColor = .gray
            lblAssignTo.textColor = .gray
            bottomView.backgroundColor = UIColor.init(red: 35/255, green: 35/255, blue: 35/255, alpha: 1)
            bottomView.borderColor = .clear
            btnCamera.setTitleColor(.white, for: .normal)
            btnImage.setTitleColor(.white, for: .normal)
            btnLocation.setTitleColor(.white, for: .normal)
            noteText?.borderColor = .lightGray
            noteText?.backgroundColor = .black
            noteText?.textColor = .white
            noteText?.textColor = .white
            noteText?.tintColor = .white
            lblNote.textColor = .gray
            timeDatePicker.overrideUserInterfaceStyle = .dark
        } else {
            view.backgroundColor = UIColor.white
            titleAttachment.textColor = .black
            titleLocation.textColor = .black
            btnDone.titleLabel?.textColor = .white
            customDetailView.backgroundColor = .white
            bottomView.backgroundColor = UIColor.init(red: 225/255, green: 225/255, blue: 225/255, alpha: 1)
            customDetailImgView.backgroundColor = UIColor.init(red: 225/255, green: 225/255, blue: 225/255, alpha: 1)
            customTopView.backgroundColor = UIColor(patternImage: UIImage(named: "bgImage.png")!)
            lblName.textColor = .black
            tf_name.placeholderColor = .gray
            tf_name.textColor = .darkGray
            tf_name.tintColor = .black
            lblCat.textColor = .black
            tfCategory.placeholderColor = .gray
            tfCategory.textColor = .darkGray
            tfCategory.tintColor = .black
            lblDesc.textColor = .black
            tfDescription.placeholderColor = .gray
            tfDescription.textColor = .darkGray
            tfDescription.tintColor = .black
            lblDueDate.textColor = .black
            lblAssignTo.textColor = .black
            bottomView.borderColor = .gray
            btnCamera.setTitleColor(.black, for: .normal)
            btnImage.setTitleColor(.black, for: .normal)
            btnLocation.setTitleColor(.black, for: .normal)
            noteText?.borderColor = UIColor.init(red: 65/255, green: 65/255, blue: 65/255, alpha: 1)
            noteText?.backgroundColor = .clear
            noteText?.textColor = .gray
            noteText?.textColor = .darkGray
            noteText?.tintColor = .black
            lblNote.textColor = .black
            timeDatePicker.overrideUserInterfaceStyle = .light
        }
    }
    
    @objc func selectFilter(notification: Notification) {
        if notification.object as! Int == 1{
            let activityViewController = UIActivityViewController(activityItems: ["Please install Task Management App to manage tasks and money together click https://apps.apple.com/us/app/task-mgmt/id1609609713"], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }else{
            let leaveAlert = UIAlertController(title: "Message", message: LEAVE_TASK_TEXT, preferredStyle: UIAlertController.Style.alert)
            leaveAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                self.tf_name.text = ""
                self.tfCategory.text = ""
                self.tfDescription.text = ""
                self.imgCoverImage.image = nil
                self.arrAttachment.removeAll()
                self.collectionAttachment.reloadData()
                selectedTabIndex = 0
                Utility.gotoTabbar()
            }))
            leaveAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            }))
            self.present(leaveAlert, animated: true, completion: nil)
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Add Note"{
            textView.text = ""
        }
    }
    
    func requestAccess() {
        eventStore.requestAccess(to: .event) { (granted, error) in
            if granted {
                DispatchQueue.main.async {
                    self.fetchCalenderEvent()
                }
            }
        }
    }
    
    func fetchCalenderEvent(){
        let calendars = eventStore.calendars(for: .event)
        for calendar in calendars {
            if calendar.title == "Work" {
                let oneMonthAgo = NSDate(timeIntervalSinceNow: -30*24*3600)
                let oneMonthAfter = NSDate(timeIntervalSinceNow: +30*24*3600)
                let predicate = eventStore.predicateForEvents(withStart: oneMonthAgo as Date, end: oneMonthAfter as Date, calendars: [calendar])
                let events = eventStore.events(matching: predicate)
                for event in events {
                    titles.append(event.title)
                    startDates.append(event.startDate as Date)
                    endDates.append(event.endDate as Date)
                    
                }
            }
        }
        tf_name.text = titles.first
    }
    
    func fetchCategories() {
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.execute { str, categories in
            GlobalData.allCategories = categories ?? []
            if GlobalData.allCategories.count < 1{
                self.addCategory()
            }else if GlobalData.allCategories.count == 1{
                self.selectedCategories = GlobalData.allCategories
                self.tfCategory.text = self.selectedCategories.map({ $0.name ?? "" }).joined(separator: ", ")
            }
        }
    }
    
    @IBAction func attachAction(_ sender: Any) {
        attachDocument()
    }
    
    @IBAction func AddSubTaskAction(_ sender: Any) {
        subtaskHeight.constant = 140
    }
    
    @IBAction func locationAction() {
        navigationController?.isNavigationBarHidden = false
        locationPicker.showCurrentLocationButton = true
        locationPicker.currentLocationButtonBackground = .black
        locationPicker.showCurrentLocationInitially = true
        locationPicker.mapType = .standard
        locationPicker.useCurrentLocationAsHint = true
        locationPicker.searchBarPlaceholder = "Search places"
        locationPicker.searchTextFieldColor = .gray
        locationPicker.searchBarStyle = .minimal
        locationPicker.searchHistoryLabel = "Search History"
        locationPicker.resultRegionDistance = 600000
        if self.traitCollection.userInterfaceStyle == .dark {
        } else {
            
        }
        if let index = selectedLocationIndex {
            let locData = self.arrLocation[index]
            locationPicker.location = Location(name: locData["address"] as? String, location: locData["location"] as? CLLocation, placemark: locData["placemark"] as! CLPlacemark)
        } else {
            locationPicker.location = nil
        }
        locationPicker.completion = { [self] location in
            let locData = ["address":location!.address,
                           "location":location!.location,
                           "placemark" : location!.placemark
            ] as [String : Any]
            self.arrLocation.append(locData)
            if self.arrLocation.count > 0{
                self.locationHeight.constant = 180
                self.titleLocation.isHidden = false
            } else {
                self.locationHeight.constant = 0
                self.titleLocation.isHidden = true
            }
            collectionMap.reloadData()
            checkDoneButton()
        }
        navigationController?.pushViewController(locationPicker, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func takeImage(sender: AnyObject) {
        isAttachmentSelected = true
        openCamera()
    }
    
    @IBAction func galaryAction(_ sender: Any) {
        isAttachmentSelected = true
        openGalary()
    }
    
    @IBAction func moreAction(_ sender: Any) {
        self.view.endEditing(true)
        let vc = (storyboard?.instantiateViewController(identifier: "CompletePopUpViewController")) as! CompletePopUpViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = .clear
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func selectDate(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSZ"
        selectedDate = dateFormatter.string(from: sender.date)
    }
    
    func validatedForPhoneNumber() -> Bool {
        if Defaults.userPhoneNumber?.isEmpty ?? true || Defaults.userPhoneNumber == nil {
            let refreshAlert = UIAlertController(title: "", message: ADD_PHONE, preferredStyle: UIAlertController.Style.alert)
            refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                self.tabBarController?.selectedIndex = 4
            }))
            self.present(refreshAlert, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    @IBAction func createTask(_ sender: UIButton) {
        if !validatedForPhoneNumber() {
            return
        }
        let task = Task()
        task.name = tf_name.text ?? ""
        task.description = tfDescription.text ?? ""
        task.notes = noteText?.text ?? "No Notes"
        task.categoryIds = selectedCategories
        task.assignedDate = "\(selectedDate)"
        Defaults.recentCategoryTypeTwo =  Defaults.recentCategoryTypeOne
        Defaults.recentCategoryTypeOne = tfCategory.text
        var assignedUsers = [[String : Any]]()
        for user in arrUser {
            var dict = [String : Any]()
            if user.userId == nil {
                dict = ["userId" : "", "type": user.userName ?? "","assigneeStatus": AssigneeStatus.invited.rawValue, "phoneNumber": user.phoneNumber ?? ""]
            }else{
                 dict = ["userId" : user.userId ?? "", "type": user.userName ?? "","assigneeStatus": AssigneeStatus.pending.rawValue, "phoneNumber": ""]
            }
            assignedUsers.append(dict as [String : Any])
        }
        var locations = [[String : Any]]()
        for location in arrLocation {
            let latLong = location["location"] as! CLLocation
            let dict = ["latitude" : latLong.coordinate.latitude, "logitude": latLong.coordinate.longitude, "address": location["address"] ?? ""]
            locations.append(dict as [String : Any])
        }
        
        task.assignedUser = assignedUsers
        task.location = locations
        
        if noteText!.text!.isEmpty {
            task.notes = "No Notes"
        }
        
        if task.name.isEmpty {
            self.presentAlert(withTitle: "", message: PLEASE_ENTER_THE_TASK_NAME)
            return
        }
        
        if  task.categoryIds.isEmpty {
            self.presentAlert(withTitle: "", message: PLEASE_ENTER_THE_CATEGORY)
            return
        }
        
        if task.assignedDate.isEmpty  {
            self.presentAlert(withTitle: "", message: PLEASE_ENTER_THE_ASSIGN_DATE)
            return
        }
        
        if arrUser.count < 1 {
            self.presentAlert(withTitle: "", message: PLEASE_ASSIGN_USER)
            return
        }
        
        let createTaskOperation = CreateTaskOperation(withTask: task, image: (imgCoverImage.image ?? UIImage(named: "Grocery"))!, arrimage: arrAttachment)
        createTaskOperation.execute { error, response, success in
            if success! {
                self.imgCoverImage.image = UIImage()
                self.tf_name.text = ""
                self.tfCategory.text = ""
                self.tfDescription.text = ""
                self.selectedCategories.removeAll()
                self.arrUser.removeAll()
                selectedTabIndex = 0
                Utility.gotoTabbar()
                var style = ToastManager.shared.style
                style.backgroundColor = self.titleLocation.textColor
                style.messageColor = self.view.backgroundColor!
                SceneDelegate.getSceneDelegate().window?.makeToast("Task created successfully.", duration: 3, position: .center, title: nil, image: nil, style: style) { didTap in
                }
            } else {
                self.presentAlert(withTitle: "", message: "\(error ?? "")")
            }
        }
    }
    
    func checkDoneButton() {
        doneButton?.isEnabled = false
        doneButton?.backgroundColor = .gray
        if !(tf_name.text ?? "").isEmpty && !selectedCategories.isEmpty && !selectedDate.isEmpty && !arrUser.isEmpty {
            doneButton?.backgroundColor = UIColor(red: 115.0/255.0, green: 146.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            doneButton?.isEnabled = true
        }
    }
    
    func setInitialUI() {
        navTitle.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont(name:"HelveticaNeue-Bold", size: 18.0)!,
            NSAttributedString.Key.foregroundColor : UIColor.white,
        ], for: .normal)
        navTitle.title = "New Task"
        timeDatePicker.overrideUserInterfaceStyle = .dark
        timeDatePicker.minimumDate = Date()
        timeDatePicker.contentHorizontalAlignment = .left
        timeDatePicker.datePickerMode = .date
        
        //Initialize Date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSZ"
        selectedDate = dateFormatter.string(from: Date())
    }
    
    @IBAction func addCategoryAction() {
        addCategory()
    }
    
    func addCategory(){
        let alertVC = UIAlertController(title: "Category", message: "Please enter category details.", preferredStyle: .alert)
        alertVC.addTextField { (textField) in textField.placeholder = "Enter category Name"
        }
        let submitAction = UIAlertAction(title: "Submit", style: .default, handler: {
            (alert) -> Void in
            let categoryName = alertVC.textFields![0] as UITextField
            if categoryName.text!.isEmpty {
                self.presentAlert(withTitle: "", message: "Please enter the category details.")
            } else {
                self.callCreateCategoryApi(categoryName: categoryName.text!, desc: "")
            }
            self.checkDoneButton()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert) -> Void in
        })
        alertVC.addAction(cancelAction)
        alertVC.addAction(submitAction)
        alertVC.view.tintColor = UIColor.black
        present(alertVC, animated: true)
    }
    
    
    func callCreateCategoryApi(categoryName : String, desc : String) {
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.createCategory(parameters: ["name" : categoryName, "description" : desc, "published" : true]) { error, category in
            self.fetchCategories()
            self.presentAlert(withTitle: "", message: "Category created successfully.")
        }
    }
    
    func coverImageGesture() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgCoverImage.isUserInteractionEnabled = true
        imgCoverImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func actionsheet() {
        let alert  = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (handler) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (handler) in
            self.openGalary()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (handler) in
            self.isAttachmentSelected = false
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let image = UIImagePickerController()
            image.allowsEditing = true
            image.delegate = self
            image.sourceType = .camera
            image.mediaTypes = [kUTTypeImage as String]
            self.present(image, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Camera", message: "Camera is Not available", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGalary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let image = UIImagePickerController()
            image.allowsEditing = true
            image.delegate = self
            self.present(image, animated: true, completion: nil)
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        isAttachmentSelected = false
        actionsheet()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1{
            DispatchQueue.main.async {
                self.view.endEditing(true)
            }
            if GlobalData.allCategories.count < 1{
                self.addCategory()
                return
            }
            let vc = (storyboard?.instantiateViewController(identifier: "CategoriesViewController")) as! CategoriesViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            vc.allcategories = GlobalData.allCategories
            vc.delegate = self
            vc.selectedCategories = selectedCategories
            self.present(vc, animated: false, completion: nil)
        }
        checkDoneButton()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkDoneButton()
    }
    
    //MARK:  CategoryDelegate
    func categoriesDidSelected(categoriesSelected: [Category],categoriesSelectedLongPress: [Category]) {
        selectedCategories = categoriesSelected
        tfCategory.text = selectedCategories.map({ $0.name ?? "" }).joined(separator: ", ")
        if categoriesSelectedLongPress.count > 0 {
            tfCategory.text = categoriesSelectedLongPress.map({ $0.name ?? "" }).joined(separator: ", ")
        }
        checkDoneButton()
    }
    
    func didShowAll() {
        let sb = UIStoryboard(name: "NewTask", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "AllCategoriesViewController") as! AllCategoriesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func attachDocument() {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import)
        //Call Delegate
        documentPicker.delegate = self
        self.present(documentPicker, animated: true)
    }
}

class AssignToCell: UICollectionViewCell {
    @IBOutlet var imageUser: UIImageView!
    @IBOutlet var assignedNameLabel: UILabel?
}

class SubtaskCell: UICollectionViewCell {
    
}

class AttachmentCell: UICollectionViewCell {
    
    @IBOutlet var imageAttach: UIImageView!
}

class LocationCell: UICollectionViewCell {
    @IBOutlet weak var lblLocationName: UILabel!
}

extension NewTaskView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case collectionAssignTo:
            return arrUser.count + 1
        case collectionSubTask:
            return 1
        case collectionAttachment:
            return arrAttachment.count
        case collectionMap:
            return arrLocation.count
        default:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionAssignTo {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssignToCell", for: indexPath) as! AssignToCell
            cell.imageUser.layer.cornerRadius = cell.imageUser.frame.height/2
            if indexPath.row < arrUser.count {
                cell.imageUser.backgroundColor = .white
                cell.imageUser.image = UIImage(named: "one-1")
                cell.assignedNameLabel?.isHidden = false
                cell.assignedNameLabel?.text = (arrUser[indexPath.row].userName ?? "")
                if self.traitCollection.userInterfaceStyle == .dark {
                    cell.assignedNameLabel?.textColor = .white
                } else {
                    cell.assignedNameLabel?.textColor = .black
                }
            } else {
                cell.assignedNameLabel?.text = ""
                cell.imageUser.backgroundColor = .clear
                cell.imageUser.image = UIImage(named: "add-friend")
            }
            return cell
        } else if collectionView == collectionSubTask{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubtaskCell", for: indexPath) as! SubtaskCell
            return cell
        } else if collectionView == collectionAttachment{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
            if arrAttachment[indexPath.row]["type"] as? Int == 0 {
                cell.imageAttach.image = arrAttachment[indexPath.row]["Info"] as? UIImage
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationCell", for: indexPath) as! LocationCell
            let name = arrLocation[indexPath.row]
            let randomValue = name["address"]!
            cell.lblLocationName.text = randomValue as? String
            if self.traitCollection.userInterfaceStyle == .dark {
                cell.lblLocationName.textColor = .white
                cell.lblLocationName.backgroundColor = .black
            } else {
                cell.lblLocationName.backgroundColor = .white
                cell.lblLocationName.textColor = .black
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionAssignTo {
            if indexPath.row >= arrUser.count {
                let vc = (storyboard?.instantiateViewController(identifier: "UserListViewController")) as! UserListViewController
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            }else{
                let leaveAlert = UIAlertController(title: "", message: "Name: \(arrUser[indexPath.row].userName ?? "")\nPhone: \(arrUser[indexPath.row].phoneNumber ?? "")", preferredStyle: UIAlertController.Style.alert)
                leaveAlert.addAction(UIAlertAction(title: "Remove", style: .destructive, handler: { (action: UIAlertAction!) in
                    self.arrUser.remove(at: indexPath.row)
                    self.collectionAssignTo.reloadData()
                    self.collectionAssignTo.scrollToItem(at: IndexPath(item: self.arrUser.count, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
                    self.checkDoneButton()
                }))
                leaveAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                }))
                self.present(leaveAlert, animated: true, completion: nil)
            }
        } else if collectionView == collectionMap {
            selectedLocationIndex = indexPath.row
            locationAction()
        } else if collectionView == collectionAttachment{
            let sb = UIStoryboard(name: "Home", bundle: nil)
            let vc = (sb.instantiateViewController(identifier: "FullImageViewController")) as! FullImageViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.9)
            if arrAttachment[indexPath.row]["type"] as? Int == 0 {
                vc.image = arrAttachment[indexPath.row]["Info"] as? UIImage
            } else {
                let path = arrAttachment[indexPath.row]["Info"] as! String
                let fullPathArr = path.components(separatedBy: "public")
                vc.img = "\(BaseURL)\(fullPathArr[1])"
            }
            vc.fileName = arrAttachment[indexPath.row]["fileName"] as? String ?? ""
            vc.delegate = self
            vc.index = indexPath.row
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionAssignTo {
            return CGSize(width: 50, height: 55)
        } else if collectionView == collectionMap {
            return CGSize(width: 130, height: 180)
        } else {
            return CGSize(width: collectionView.frame.width/1.5, height: 140)
        }
    }
}

extension NewTaskView: assignedUserProtocol {
    func userAssigned(user: User) {
        if arrUser.count >= 10 {
            let alert = UIAlertController(title: "", message: "Assign user limit exceeded.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
                
        if arrUser.contains(where: { $0.userId == user.userId && $0.phoneNumber == user.phoneNumber}) {
            //found
            DispatchQueue.main.async {
                let leaveAlert = UIAlertController(title: "", message: USER_ALREADR_ADDED, preferredStyle: UIAlertController.Style.alert)
                leaveAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                }))
                self.present(leaveAlert, animated: true, completion: nil)
            }
            return
        }
        arrUser.append(user)
        collectionAssignTo.reloadData()
        collectionAssignTo.scrollToItem(at: IndexPath(item: arrUser.count, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)
        checkDoneButton()
    }
}

extension NewTaskView: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        if isAttachmentSelected == true{
            let arr = ["type": 0,
                       "Info": tempImage] as [String : Any]
            arrAttachment.append(arr)
            if arrAttachment.count > 0{
                titleAttachment.isHidden = false
                attachmentHeight.constant = 140
            } else {
                titleAttachment.isHidden = true
                attachmentHeight.constant = 0
            }
            collectionAttachment.reloadData()
        } else {
            imgCoverImage.image = tempImage
        }
        self.dismiss(animated: true, completion: nil)
        checkDoneButton()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


extension NewTaskView: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let arr = ["type": 1,
                   "Info": urls[0]] as [String : Any]
        arrAttachment.append(arr)
        if arrAttachment.count > 0{
            titleAttachment.isHidden = false
            attachmentHeight.constant = 140
        } else {
            titleAttachment.isHidden = true
            attachmentHeight.constant = 0
        }
        collectionAttachment.reloadData()
        checkDoneButton()
    }
}

extension NewTaskView: FullImageViewControllerDelegate {
    func attachmentDeleted(index: Int) {
        arrAttachment.remove(at: index)
        if arrAttachment.count > 0{
            titleAttachment.isHidden = false
            attachmentHeight.constant = 140
        } else {
            titleAttachment.isHidden = true
            attachmentHeight.constant = 0
        }
        collectionAttachment.reloadData()
        checkDoneButton()
    }
}
