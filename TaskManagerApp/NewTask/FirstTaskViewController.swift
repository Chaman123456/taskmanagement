//
//  FirstTaskViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 21/12/21.
//

import UIKit

class FirstTaskViewController: UIViewController {
   
    @IBOutlet var txtFld: UITextField!
    @IBOutlet var mainView: UIView!
    @IBOutlet var itemsCollectionView: UICollectionView!
    
    var categories = [Category]()
    var selectedCategories = [Category]()

    let arrCatImageName = ["Groceries", "General Items", "Things To Do", "Shopping Items", "Place to be", "Random List","Groceries", "General Items", "Things To Do", "Groceries", "General Items", "Things To Do", "Shopping Items", "Place to be", "Random List","Groceries", "General Items", "Things To Do"]

    override func viewDidLoad() {
        super.viewDidLoad()
        itemsCollectionView.showsHorizontalScrollIndicator = false
        itemsCollectionView.showsVerticalScrollIndicator = false
        topTextField()
        setupRightVw()
        textfieldCustomise()
        self.view.layer.insertSublayer(ThemeUtility.backgorundGradient(frame: self.view.bounds), at: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.execute { str, categories in
            GlobalData.allCategories = categories ?? []
            self.categories = GlobalData.allCategories
            self.itemsCollectionView.reloadData()
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: false, completion: nil)
    }
    
    func topTextField() {
        txtFld.layer.cornerRadius = txtFld.frame.size.height/2
        let myColor = UIColor.white
        txtFld.layer.borderColor = myColor.cgColor
        txtFld.layer.borderWidth = 1.0
        txtFld.clipsToBounds = true
        txtFld.textColor = UIColor.white
        txtFld.font = UIFont.init(name: "AvenirNext-Medium", size: 15)
        txtFld.setLeftPaddingPoints(20)
    }
    
    func textfieldCustomise() {
        txtFld.attributedPlaceholder = NSAttributedString(
            string: "Type Something...",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]
        )
    }

    func setupRightVw() {
            txtFld.rightViewMode = .unlessEditing
            let button = UIButton(type: .custom)
            button.setImage(UIImage(named: "VoiceRecordingImg"), for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
            button.frame = CGRect(x: CGFloat(txtFld.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
            button.addTarget(self, action:#selector(handleRegister(sender:)), for: .touchUpInside)
            txtFld.rightView = button
            txtFld.rightViewMode = .always
        }
    
    @objc func handleRegister(sender: UIButton){
       //...
    }
}

extension FirstTaskViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ItemsCollectionViewCell
        cell.contentView.backgroundColor = .red
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1.0
        cell.itemsLabel.text = self.categories[indexPath.row].name
        //cell.itemsLabel.text = self.arrCatTitleName[indexPath.row]
        cell.itemsImage.image = UIImage(named: arrCatImageName[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            self.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: Notification.Name("UpdateFirstTaskView"), object: nil)
        }
}

extension FirstTaskViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ( self.itemsCollectionView.frame.size.width) / 3,height:( self.itemsCollectionView.frame.size.width ) / 3)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}

class ItemsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var itemsCell: UIImageView!
    @IBOutlet var itemsLabel: UILabel!
    @IBOutlet var itemsImage: UIImageView!
}
