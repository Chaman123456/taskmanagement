//
//  AllCategoriesTableViewCell.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 15/03/22.
//

import UIKit

class AllCategoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var mainBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
