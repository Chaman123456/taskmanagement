//
//  CompletePopUpViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 16/12/21.
//

import UIKit

class CompletePopUpViewController: UIViewController {
    
    @IBOutlet var bottomView: UIView!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var btnLeaveTask: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
    }
    
    @objc func setUpMode() {
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
            if self.traitCollection.userInterfaceStyle == .dark {
                self.bottomView.backgroundColor = UIColor.init(displayP3Red: 50/255, green: 50/255, blue: 50/255, alpha: 1)
                self.btnShare.setTitleColor(.white, for: .normal)
                self.btnLeaveTask.setTitleColor(.white, for: .normal)
                self.bottomView.isHidden = false
            } else {
                self.view.backgroundColor = .clear
                self.btnShare.setTitleColor(.black, for: .normal)
                self.btnLeaveTask.setTitleColor(.black, for: .normal)
                self.bottomView.isHidden = false
                self.bottomView.backgroundColor = .white
            }
        }
    }
    
    @IBAction func shareAction() {
        self.dismiss(animated: false, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectFilter"), object: 1)  
    }
    
    @IBAction func leaveAction() {
        self.dismiss(animated: false, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectFilter"), object: 2)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        let touch = touches.first
        if touch?.view != self.bottomView
        { self.dismiss(animated: false, completion: nil) }
    }
    
    @IBAction func btnMakeAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
}
