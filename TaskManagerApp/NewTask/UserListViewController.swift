//
//  UserListViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 15/12/21.
//

import UIKit
import MessageUI
import ContactsUI
import Contacts

protocol assignedUserProtocol {
    func userAssigned(user: User)
}

class UserListViewController: UIViewController, CNContactViewControllerDelegate, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var btnAddContact: UIButton!
    @IBOutlet var lblNewContact: UILabel!
    @IBOutlet var searchField: UITextField!
    @IBOutlet var tableList: UITableView!
    var delegate: assignedUserProtocol?
    var allUser = [User]()
    var searchUser = [User]()
    var isSearch:Bool = false
    var searchText = String()
    private let contactsVM = ContactsViewModel()
    @IBOutlet var backButton: UIButton?
    @IBOutlet var mainView : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableList.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        if isFromMenu {
            backButton?.setTitle("Invite Friends", for: .normal)
            askUserForContactsPermission()
        } else {
            backButton?.setTitle("Add Members", for: .normal)
        }
        searchField.addTarget(self, action: #selector(UserListViewController.textFieldDidChange(_:)), for: .editingChanged)
        mainView.layer.cornerRadius = 12
        getAllContacts()
    }
    
    func getAllContacts() {
        let profileOperation = ProfileOperation()
        profileOperation.getAllUsers { error, users in
            var myUser = [User]()
            GlobalData.allUsers = users ?? []
            GlobalData.allUsers.forEach({$0.isContect = false})
            myUser = GlobalData.allUsers.filter({ $0.userId != Defaults.userId })
            let contacts = PhoneContacts.getContacts()
            for contact in contacts {
                let user = User()
                for contctNumVar: CNLabeledValue in contact.phoneNumbers {
                    if let fulMobNumVar = contctNumVar.value as? CNPhoneNumber {
                        if let MccNamVar = fulMobNumVar.value(forKey: "digits") as? String {
                            user.phoneNumber = "\(MccNamVar)"
                        }
                    }
                }
                user.isContect = true
                user.userName = contact.givenName
                myUser.removeAll {userCheck in
                    let check = (user.phoneNumber ?? "0").contains(userCheck.phoneNumber ?? "")
                    if check {
                        user.isContect = false
                    }
                    return check
                }
                myUser.append(user)
            }
            var set = Set<String>()
            let arraySet: [User] = myUser.compactMap {
                guard let phone = $0.phoneNumber else { return nil }
                return set.insert(phone).inserted ? $0 : nil
            }
            self.allUser = arraySet
            self.tableList.reloadData()
        }
    }
    
    
    @IBAction func addNewContact(_ sender: Any) {
        let contactVc = CNContact()
        let contactViewController = CNContactViewController(forNewContact: contactVc)
        contactViewController.contactStore = CNContactStore()
        contactViewController.delegate = self
        let navigationController = UINavigationController(rootViewController: contactViewController)
        self.present(navigationController, animated: false)
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            backButton?.setTitleColor(.white, for: .normal)
            view.backgroundColor = .black
            searchField.placeholderColor = .gray
            searchField.textColor = .white
            tableList.backgroundColor = .black
            searchField.backgroundColor = .lightGray
            btnAddContact.backgroundColor = .clear
            btnAddContact.layer.cornerRadius = btnAddContact.frame.size.width/2
            btnAddContact.clipsToBounds = true
            btnAddContact.layer.borderWidth = 1
            btnAddContact.layer.borderColor = UIColor.lightGray.cgColor
            lblNewContact.textColor = .white
        } else {
            lblNewContact.textColor = .black
            backButton?.setTitleColor(.black, for: .normal)
            view.backgroundColor = .white
            searchField.placeholderColor = .gray
            searchField.textColor = .black
            tableList.backgroundColor = .white
            searchField.backgroundColor = .lightGray
        }
        tableList.reloadData()
    }
    
    @objc func textFieldDidChange(_ searchTextField: UITextField) {
        mainView.isHidden = true
        searchText = searchTextField.text!.uppercased()
        if isFromMenu {
            contactsVM.filterContact(searchText: searchText)
            if contactsVM.getNumberOfContacts(forSection: 0) <= 0 {
                mainView.isHidden = false
            } else {
                mainView.isHidden = true
            }
        } else {
            if searchText.isEmpty {
                isSearch = false
            } else {
                isSearch = true
                searchUser = allUser.filter({ data in
                    if data.userName!.uppercased().contains(searchText) || data.phoneNumber!.contains(searchText){
                        return true
                    } else {
                        return false
                    }
                })
                if searchUser.isEmpty {
                    mainView.isHidden = false
                } else {
                    mainView.isHidden = true
                }
            }
        }
        tableList.reloadData()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        if isFromMenu {
            Utility.gotoTabbar()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        isFromMenu = false
    }
    
    @IBAction func backAction(_ sender: Any) {
        if isFromMenu == true {
            Utility.gotoTabbar()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        isFromMenu = false
    }
    
    @objc func sendSMS(indexPath: IndexPath) {
        
    }
    
    private func askUserForContactsPermission() {
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { (granted: Bool, err: Error?) in
            if let err = err {
                print("Failed to request access with error \(err)")
                return
            }
            if granted {
                print("User has granted permission for contacts")
                self.contactsVM.fetchContacts()
                DispatchQueue.main.async {
                    self.tableList.reloadData()
                }
            } else {
                print("Access denied..")
            }
        }
    }
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        self.dismiss(animated: true, completion: nil)
        getAllContacts()
        mainView.isHidden = true
        searchField.text = ""
        tableList.reloadData()
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}

class UserListCell: UITableViewCell {
    @IBOutlet var imageUser: UIImageView!
    @IBOutlet var lblFullName: UILabel!
    @IBOutlet var btnInvite: UIButton!
}

extension UserListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFromMenu {
            return contactsVM.getNumberOfSectionsForContacts()
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if isFromMenu {
            return contactsVM.getNumberOfContacts(forSection: section)
        } else {
            if isSearch {
                return searchUser.count
            } else {
                return allUser.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFromMenu {
            guard let contactsTableViewCell = tableList.dequeueReusableCell(withIdentifier: "UserListCell", for: indexPath) as? UserListCell else {
                return UITableViewCell()
            }
            contactsTableViewCell.btnInvite.tag = indexPath.row
            contactsTableViewCell.lblFullName.text = contactsVM.getContactName(forIndexPath: indexPath)
            contactsTableViewCell.btnInvite.addTarget(self, action: #selector(sendSMS), for: UIControl.Event.touchUpInside)
            if self.traitCollection.userInterfaceStyle == .dark {
                contactsTableViewCell.backgroundColor = .black
                contactsTableViewCell.lblFullName.textColor = .white
            } else {
                contactsTableViewCell.backgroundColor = .white
                contactsTableViewCell.lblFullName.textColor = UIColor.init(red: 30/255, green: 30/255, blue: 30/255, alpha: 1)
            }
            return contactsTableViewCell
        } else {
            var userData = User()
            if isSearch {
                userData = searchUser[indexPath.row]
            } else {
                userData = allUser[indexPath.row]
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell", for: indexPath) as! UserListCell
            if userData.userName != ""{
                cell.lblFullName.text = userData.userName
            } else {
                cell.lblFullName.text = userData.phoneNumber
            }
            cell.btnInvite.tag = indexPath.row
            if isFromMenu {
                cell.btnInvite.setTitle("Invite", for: .normal)
                cell.btnInvite.setImage(nil, for: .normal)
                cell.btnInvite.addTarget(self, action: #selector(sendSMS), for: UIControl.Event.touchUpInside)
            } else {
                if userData.isContect {
                    cell.btnInvite.setTitle("Invite", for: .normal)
                    cell.btnInvite.setImage(nil, for: .normal)
                    cell.btnInvite.isEnabled = true
                } else {
                    cell.btnInvite.setTitle("", for: .normal)
                    cell.btnInvite.setImage(UIImage(named: "add"), for: .normal)
                    cell.btnInvite.isEnabled = false
                }
            }
            let path = userData.imageUrl
            if path != ""{
                let fullPathArr = path.components(separatedBy: "public")
                cell.imageUser.backgroundColor = .white
                cell.imageUser.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr[1])"), placeholder: UIImage(named: "one-1"), options: .none) { result in
                }
            }
            if self.traitCollection.userInterfaceStyle == .dark {
                cell.backgroundColor = .black
                cell.lblFullName.textColor = .white
            } else {
                cell.backgroundColor = .white
                cell.lblFullName.textColor = UIColor.init(red: 30/255, green: 30/255, blue: 30/255, alpha: 1)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! UserListCell
        var name: String?
        name = cell.lblFullName.text
        if isFromMenu {
            let actionSheetController = UIAlertController(title: "Select Option", message: "", preferredStyle: .actionSheet)
            let invitebytext = UIAlertAction(title: "Invite By Text", style: .default) { action -> Void in
                //by text
                let composeVC = MFMessageComposeViewController()
                composeVC.messageComposeDelegate = self
                composeVC.recipients = [self.contactsVM.getContact(forIndexPath: indexPath).phoneNumbers[0].value.stringValue]
                composeVC.body = "Please install Task Management App to manage tasks and money together click https://apps.apple.com/us/app/task-mgmt/id1609609713"
                if MFMessageComposeViewController.canSendText() {
                    self.present(composeVC, animated: true, completion: nil)
                    self.view.makeToast("Invite is sent to \(name!)")
                }
            }
            actionSheetController.addAction(invitebytext)
            let invitebymail = UIAlertAction(title: "Invite By Mail", style: .default) { action -> Void in
                //by mail
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients([""])
                    mail.setSubject("Task Management")
                    mail.setMessageBody("<b>Please install Task Management App to manage tasks and money together click:\n<a href=\"https://apps.apple.com/us/app/task-mgmt/id1609609713\">https://apps.apple.com/us/app/task-mgmt/id1609609713</a></b>", isHTML: true)
                    self.present(mail, animated: true, completion: nil)
                } else {
                    let warningAlert = UIAlertController(title: "Message", message: "Your device is not configured to send emails", preferredStyle: .alert)
                    let okButton = UIAlertAction(title: "Ok", style: .default) { action -> Void in
                    }
                    warningAlert.addAction(okButton)
                    self.present(warningAlert, animated: true, completion: nil)
                }
            }
            actionSheetController.addAction(invitebymail)
            actionSheetController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
             }))
            self.present(actionSheetController, animated: true, completion: nil)
        } else {
            let cell = tableView.cellForRow(at: indexPath) as! UserListCell
            if cell.btnInvite.title(for: UIControl.State.normal) == "Invite" {
                let actionSheetController = UIAlertController(title: "Select Option", message: "", preferredStyle: .actionSheet)
                let invitebytext = UIAlertAction(title: "Invite By Text", style: .default) { action -> Void in
                    //by text
                    let composeVC = MFMessageComposeViewController()
                    composeVC.messageComposeDelegate = self
                    if self.isSearch {
                        composeVC.recipients = ["\(self.searchUser[indexPath.row].phoneNumber!)"]
                    } else {
                        composeVC.recipients = ["\(self.allUser[indexPath.row].phoneNumber!)"]
                    }
                    composeVC.body = "Please install Task Management App to manage tasks and money together click https://apps.apple.com/us/app/task-mgmt/id1609609713"
                    if MFMessageComposeViewController.canSendText() {
                        self.present(composeVC, animated: true, completion: nil)
                        self.view.makeToast("Invite is sent to \(name!)")
                    }
                }
                actionSheetController.addAction(invitebytext)
                let invitebymail = UIAlertAction(title: "Invite By Mail", style: .default) { action -> Void in
                    //by mail
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self
                        mail.setToRecipients([""])
                        mail.setSubject("Task Management")
                        mail.setMessageBody("<b>Please install Task Management App to manage tasks and money together click:\n<a href=\"https://apps.apple.com/us/app/task-mgmt/id1609609713\">https://apps.apple.com/us/app/task-mgmt/id1609609713</a></b>", isHTML: true)
                        self.present(mail, animated: true, completion: nil)
                    } else {
                        print("Cannot send mail")
                        // give feedback to the user
                    }
                }
                actionSheetController.addAction(invitebymail)
                actionSheetController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                 }))
                self.present(actionSheetController, animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
                if isSearch {
                    self.delegate?.userAssigned(user: searchUser[indexPath.row])
                } else {
                    self.delegate?.userAssigned(user: allUser[indexPath.row])
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isFromMenu {
            return 30
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isFromMenu {
            let headerView: UIView = {
                let view = UIView()
                view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                if self.traitCollection.userInterfaceStyle == .dark {
                    view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                } else {
                    view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                return view
            }()
            
            let initialLetterLabel: UILabel = {
                let label = UILabel()
                label.font = UIFont.boldSystemFont(ofSize: 18)
                label.text = contactsVM.getSectionTitle(forSection: section)
                label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                if self.traitCollection.userInterfaceStyle == .dark {
                    label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                } else {
                    label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                }
                return label
            }()
            
            headerView.addSubview(initialLetterLabel)
            
            initialLetterLabel.setAnchorsWithConstants(leadingAnchor: headerView.leadingAnchor, paddingLeading: 20, trailingAnchor: headerView.trailingAnchor, height: 20)
            initialLetterLabel.setAnchorsWithoutConstants(centerYAnchor: headerView.centerYAnchor)
            
            return headerView
        }
        return nil
    }
    
}

extension UserListViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true)
    }
}

class PhoneContacts {
    static func getContacts() -> [CNContact] {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactThumbnailImageDataKey] as [Any]
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching containers")
            }
        }
        return results
    }
}
