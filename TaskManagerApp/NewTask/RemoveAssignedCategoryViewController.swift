//
//  RemoveAssignedCategoryViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 08/03/22.
//

import UIKit

class RemoveAssignedCategoryViewController: UIViewController {
    var categoryId = String()
    var deleteselectedCat = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func removeAction(_ sender: Any) {
        deleteMultipleCategory()
    }
    
    @IBAction func closeAction() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func deleteMultipleCategory() {
        let selectedCategoriesToDelete = deleteselectedCat.map { $0._id ?? "" }
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.deleteMultipleCategory(parameters: ["cat_id": selectedCategoriesToDelete]) { error, category in
            NotificationCenter.default.post(name: Notification.Name("RemoveCategory"), object: nil)
        }
    }
}
