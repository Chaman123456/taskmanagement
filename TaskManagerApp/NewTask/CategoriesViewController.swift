//
//  CategoriesViewController.swift
//  SampleUi
//
//  Created by Chaman Sharma on 16/12/21.
//

import UIKit

protocol CategoryDelegate {
    func categoriesDidSelected(categoriesSelected: [Category],categoriesSelectedLongPress: [Category])
    func didShowAll()
}

class CategoriesViewController: UIViewController {
    public var saveCategoriesId = String()
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var lblSelectCat: UILabel!
    @IBOutlet weak var showAllButton: UIButton!
    @IBOutlet weak var btnAddNew: UIButton!
    @IBOutlet weak var selectedDeleteButton: UIButton!
    @IBOutlet weak var selectCategoriesLabel: UILabel!
    @IBOutlet var selectCategoriesView: UIView!
    @IBOutlet var categoriesCollectionView: UICollectionView!
    var allcategories = [Category]()
    var selectedCategories = [Category]()
    var delegate: CategoryDelegate?
    var longPressBool : Bool = false
    var selectedTags = [Int]()
    var selectedCategoriesLongPress = [Category]()
    var newAddedCategory: String?
    
    //MARK:  View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        NotificationCenter.default.addObserver(self, selector: #selector(removeCategory), name: Notification.Name("RemoveCategory"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.selectCategory(notification:)), name: Notification.Name("SelectCategory"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
    }
    
    @IBAction func addNewAction(_ sender: Any) {
        addCategory()
    }
    
    @IBAction func showAllAction(_ sender: Any) {
        dismiss(animated: false) {
            if let _delegate = self.delegate {
                _delegate.didShowAll()
            }
        }
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "RemoveAssignedCategoryViewController") as! RemoveAssignedCategoryViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        vc.deleteselectedCat = selectedCategoriesLongPress
        self.present(vc, animated: false, completion: nil)
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            customView.backgroundColor = .black
            lblSelectCat.textColor = .white
            selectCategoriesView.backgroundColor = .black
        } else {
            customView.backgroundColor = UIColor.white
            lblSelectCat.textColor = UIColor.black
            selectCategoriesView.backgroundColor = .white
        }
    }
    
    @IBAction func doneAction() {
        if let _delegate = delegate {
            _delegate.categoriesDidSelected(categoriesSelected: selectedCategories, categoriesSelectedLongPress: selectedCategoriesLongPress)
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func directFullPreviewLongPressAction(g: UILongPressGestureRecognizer){
        if g.state == UIGestureRecognizer.State.began{
            if longPressBool == true {
                longPressBool = false
                self.selectCategoriesLabel.text = "Select Category(s)"
            } else {
                longPressBool = true
            }
            selectedDeleteButton.isHidden = false
            self.selectCategoriesLabel.text = "Selected"
            self.categoriesCollectionView.reloadData()
        }
    }
    
    @objc func removeCategory() {
        fetchCategories()
        if let _delegate = delegate {
            _delegate.categoriesDidSelected(categoriesSelected: [], categoriesSelectedLongPress: [])
        }
        self.dismiss(animated: false, completion: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func selectCategory(notification: Notification) {
        DispatchQueue.main.async {
            self.selectedCategories.removeAll()
            self.selectedCategories.append(notification.object as! Category)
            if let _delegate = self.delegate {
                _delegate.categoriesDidSelected(categoriesSelected: self.selectedCategories, categoriesSelectedLongPress: self.selectedCategoriesLongPress)
            }
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        let touch = touches.first
        if touch?.view != self.selectCategoriesView
        { self.dismiss(animated: false, completion: nil) }
    }
    
    func fetchCategories() {
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.execute { str, categories in
            self.selectedCategories = [Category]()
            GlobalData.allCategories = categories ?? []
            self.allcategories = GlobalData.allCategories
            
            let indexx = self.allcategories.firstIndex(where: {$0.name == self.newAddedCategory})
            if (indexx != nil){
                self.selectedCategories.append(self.allcategories[indexx!])
                if let _delegate = self.delegate {
                    _delegate.categoriesDidSelected(categoriesSelected: self.selectedCategories, categoriesSelectedLongPress: self.selectedCategoriesLongPress)
                }
            }
            self.categoriesCollectionView.reloadData()
        }
    }
    
    @IBAction func addCategoryAction() {
        addCategory()
    }
    
    func addCategory() {
        let alertVC = UIAlertController(title: "Category", message: "Please enter category details.", preferredStyle: .alert)
        alertVC.addTextField { (textField) in textField.placeholder = "Enter category Name"
        }
        let submitAction = UIAlertAction(title: "Submit", style: .default, handler: {
            (alert) -> Void in
            let categoryName = alertVC.textFields![0] as UITextField
            if categoryName.text!.contains("Add New Category"){
                self.view.makeToast("Invalid category name")
                return
            }
            if categoryName.text!.isEmpty{
                self.presentAlert(withTitle: "", message: "Please enter the category details.")
            } else {
                self.callCreateCategoryApi(categoryName: categoryName.text!, desc: "")
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert) -> Void in
        })
        alertVC.addAction(cancelAction)
        alertVC.addAction(submitAction)
        present(alertVC, animated: true)
    }
    
    
    func callCreateCategoryApi(categoryName : String, desc : String) {
        if allcategories.contains(where: { $0.name?.lowercased() == categoryName.lowercased() } ){
            let alertVC = UIAlertController(title: "Category", message: "This category already exists.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: {
                (alert) -> Void in
            })
            alertVC.addAction(okAction)
            present(alertVC, animated: true)
            return
        }
        newAddedCategory = categoryName
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.createCategory(parameters: ["name" : categoryName, "description" : desc, "published" : true]) { error, category in
            self.fetchCategories()
            self.presentAlert(withTitle: "", message: "Category created successfully.")
        }
    }
}

class CategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet var lblCategory: UILabel!
    @IBOutlet weak var categoriesImage: UIImageView!
}

extension CategoriesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allcategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCollectionViewCell", for: indexPath) as! CategoriesCollectionViewCell
        
        cell.lblCategory.text = allcategories[indexPath.row].name
        if selectedCategories.map({ $0._id ?? "" }).contains(where: { ($0 == allcategories[indexPath.row]._id) } ) && longPressBool == false {
            if self.traitCollection.userInterfaceStyle == .dark {
                cell.contentView.backgroundColor = .white
                cell.lblCategory.textColor = .black
            } else {
                cell.contentView.backgroundColor = .black
                cell.lblCategory.textColor = .white
            }
        } else {
            if self.traitCollection.userInterfaceStyle == .dark {
                cell.contentView.backgroundColor = .black
                cell.lblCategory.textColor = .white
            } else {
                cell.contentView.backgroundColor = .white
                cell.lblCategory.textColor = .black
            }
        }
        
        cell.gestureRecognizers?.removeAll()
        cell.tag = indexPath.row
        let directFullPreviewer = UILongPressGestureRecognizer(target: self, action: #selector(directFullPreviewLongPressAction))
        cell.addGestureRecognizer(directFullPreviewer)
        if longPressBool == true {
            cell.categoriesImage.isHidden = false
            if(selectedTags.contains(indexPath.item)){
                cell.categoriesImage.image = UIImage.init(named: "Checkmark")
            } else {
                cell.categoriesImage.image = UIImage.init(named: "dry-clean")
            }
        } else {
            cell.categoriesImage.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if longPressBool == true {
            if(selectedTags.contains(indexPath.item)){
                selectedTags = selectedTags.filter { $0 != indexPath.item }
            } else {
                selectedTags.append(indexPath.item)
            }
            
            if selectedTags.count > 0 {
                selectCategoriesLabel.text = "\(selectedTags.count) \("Selected")"
            } else {
                selectCategoriesLabel.text = "Select Category(s)"
            }
            
            if let index = selectedCategoriesLongPress.map({ $0._id ?? ""}).firstIndex(where: { ($0 == allcategories[indexPath.row]._id) } ) {
                selectedCategoriesLongPress.remove(at: index)
            } else {
                selectedCategories.removeAll()
                selectedCategoriesLongPress.append(allcategories[indexPath.row])
            }
            categoriesCollectionView.reloadData()
        } else {
            if indexPath.row >= allcategories.count {
                addCategory()
            }else{
                if let index = selectedCategories.map({ $0._id ?? ""}).firstIndex(where: { ($0 == allcategories[indexPath.row]._id) } ) {
                    selectedCategories.remove(at: index)
                } else {
                    selectedCategories.removeAll()
                    selectedCategories.append(allcategories[indexPath.row])
                }
                categoriesCollectionView.reloadData()
            }
        }
    }
}

extension CategoriesViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ( self.categoriesCollectionView.frame.size.width)/2.1,height:(self.categoriesCollectionView.frame.size.height)/4.7)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
