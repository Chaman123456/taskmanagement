//
//  TaskView.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 09/12/21.
//

import UIKit
import BubblePictures
import Kingfisher

class TaskView: UIViewController {
    @IBOutlet var viewCategory: UIView!
    @IBOutlet var tableViewTask: UITableView!
    var homeViewModel = HomeViewModel()
    var allTask: [DetailDataModel] = []
    var filteredTask: [DetailDataModel] = []
    var selCateogry: String?
    @IBOutlet var noTaskLabel: UILabel?
    var categoryScroll : CategoryScroll?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCategories()
        getCompletedTask()
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            tableViewTask.separatorColor = UIColor.clear
            tableViewTask.backgroundColor = .black
            noTaskLabel?.textColor = .white
        } else {
            view.backgroundColor = UIColor.white
            tableViewTask.separatorColor = UIColor.clear
            tableViewTask.backgroundColor = .white
            noTaskLabel?.textColor = .black
        }
    }
    
    func getCategories() {
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.execute { str, categories in
            GlobalData.allCategories = categories ?? []
            self.homeViewModel.delegate = self
            self.homeViewModel.setupCategoryScorll(categoryArray: GlobalData.allCategories)
        }
    }
    
    func getCompletedTask() {
        let completedTaskOperation = CompletedTaskOperation()
        completedTaskOperation.executeCompletedTask() { str, response in
            self.allTask.removeAll()
            if response?.recent?.count ?? 0 > 0{
                for a in (response?.recent)!{
                    self.allTask.append(a)
                }
            }
            if response?.today?.count ?? 0 > 0{
                for a in (response?.today)!{
                    self.allTask.append(a)
                }
            }
            if response?.upcoming?.count  ?? 0 > 0{
                for a in (response?.upcoming)!{
                    self.allTask.append(a)
                }
            }
            if response?.later?.count  ?? 0 > 0{
                for a in (response?.later)!{
                    self.allTask.append(a)
                }
            }
            if response?.completed?.count ?? 0 > 0{
                for a in (response?.completed)!{
                    self.allTask.append(a)
                }
            }
            self.filteredTask = self.allTask
            
            if self.filteredTask.isEmpty {
                self.noTaskLabel?.isHidden = false
            } else {
                self.noTaskLabel?.isHidden = true
            }
            self.tableViewTask.reloadData()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        sideMenuController?.revealMenu()
    }
    
    @IBAction func moreAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Priority", bundle: nil)
        let vc = (sb.instantiateViewController(identifier: "PriortyFilterViewController")) as! PriortyFilterViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.clear
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func NotificationAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: true, completion: nil)
    }
}

class TaskViewTableCell: UITableViewCell{
    @IBOutlet var bubbleCollection: UICollectionView!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var imageBg: UIImageView!
    @IBOutlet var btnStar: UIButton!
    @IBOutlet var image_stack_bg: UIImageView!
    
    var bubblePictures: BubblePictures!
    func getConfigFiles() -> [BPCellConfigFile] {
        return [
        ]
    }
}

extension TaskView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredTask.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskViewTableCell", for: indexPath) as! TaskViewTableCell
        let data = filteredTask[indexPath.row]
        if data.getting_started == true {
            cell.lblStatus.text = "Getting Started"
        }else if data.in_progress == true {
            cell.lblStatus.text = "In Progress"
        }else if data.incomplete == true {
            cell.lblStatus.text = "Completed"
        }
        cell.lblTitle.text = data.name
        cell.lblDate.text = data.date
        let path = data.backgroundImages?.first?.path
        if path != ""{
            let fullPathArr = path?.components(separatedBy: "public")
            cell.imageBg.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "user"), options: .none) { result in
            }
        }
        
        if data.isFavorite ?? false {
            cell.btnStar.isHidden = false
        } else {
            cell.btnStar.isHidden = true
        }
        
        var configFiles = cell.getConfigFiles()
        if !data.assignId!.isEmpty {
            for item in data.assignId! {
                let user = User()
                user.userId = item["id"] as? String ?? ""
                user.userName = item["type"] as? String ?? ""
                user.assigneeStatus = item["assigneeStatus"] as? String ?? ""
                configFiles.append(BPCellConfigFile(
                    imageType: BPImageType.image(UIImage(named: "one-1")!), title: ""))
            }
        }
        let layoutConfigurator = BPLayoutConfigurator(
            colorForBubbleBorders: UIColor.gray,
            colorForBubbleTitles: UIColor.clear,
            maxCharactersForBubbleTitles: 0,
            maxNumberOfBubbles: 10,
            displayForTruncatedCell: BPTruncatedCellDisplay.image(BPImageType.image(UIImage(named: "one-1")!)),
            direction: .leftToRight,
            alignment: .right)
        cell.bubblePictures = BubblePictures(collectionView: cell.bubbleCollection, configFiles: configFiles, layoutConfigurator: layoutConfigurator)
        cell.bubblePictures.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableViewTask.frame.width, height: 50))
        headerView.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.1176470588, blue: 0.1176470588, alpha: 1)
        let label = UILabel(frame: headerView.bounds)
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont(name: "SFProDisplay-Bold", size: 16)
        label.text = " Today"
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "HomeDetailViewController") as! HomeDetailViewController
        vc.detailData = filteredTask[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension TaskView : HomeViewModelProtocol {
    func populateCategries(categoryItemArray: [Any]) {
    }
    
    func setupCategoryScorllDone(categoryArray: [Category]) {
        if !categoryArray.isEmpty {
            for view in (categoryScroll?.subviews ?? []) {
                view.removeFromSuperview()
            }
            
            let category = Category(dictionary: [:])
            category?._id = "all_cat_id"
            category?.name = "All Category"
            category?.updatedAt = categoryArray[0].updatedAt
            category?.createdAt = categoryArray[0].createdAt
            category?.description = categoryArray[0].description
            category?.published = categoryArray[0].published
            
            let categoryAhowAll = Category(dictionary: [:])
            categoryAhowAll?._id = "show_all_cat_id"
            categoryAhowAll?.name = "+Show All"
            category?.updatedAt = categoryArray[0].updatedAt
            categoryAhowAll?.createdAt = categoryArray[0].createdAt
            categoryAhowAll?.description = categoryArray[0].description
            categoryAhowAll?.published = categoryArray[0].published
            
            var addedCategoryArray = [Category]()
            if categoryArray.count > 2{
                for a in categoryArray{
                    if addedCategoryArray.count < 2{
                        addedCategoryArray.append(a)
                    }
                }
                addedCategoryArray.insert(category!, at: 0)
                addedCategoryArray.insert(categoryAhowAll!, at: 3)
            }else{
                addedCategoryArray = categoryArray
                addedCategoryArray.insert(category!, at: 0)
            }
            
            categoryScroll = CategoryScroll(frame: CGRect(x: 0, y: 100, width: ScreenSize.SCREEN_WIDTH, height: 38))
            categoryScroll!.setupScroll(itemArray: addedCategoryArray)
            categoryScroll!.categoryScrollDelegate = self
            self.view.addSubview(categoryScroll!)
        }
    }
}

extension TaskView : CategoryScrollProtocol {
    func categoryChoosed(selectedItemName: String) {
        selCateogry = selectedItemName
        if selCateogry == "All Category" {
            self.filteredTask = self.allTask
        } else if selCateogry == "+Show All"{
            let sb = UIStoryboard(name: "NewTask", bundle: nil)
            let vc = (sb.instantiateViewController(identifier: "AllCategoriesViewController")) as! AllCategoriesViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.taskViewObject = self
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.present(vc, animated: false, completion: nil)
        } else {
            self.filteredTask = self.allTask.filter({ myTask in
                let categoryArr = myTask.category
                return categoryArr!.contains(where: { cat in
                    cat.name == self.selCateogry
                })
            })
        }
        if self.filteredTask.isEmpty {
            self.noTaskLabel?.isHidden = false
        } else {
            self.noTaskLabel?.isHidden = true
        }
        self.tableViewTask.reloadData()
    }
}

extension TaskView: BPDelegate {
    func didSelectTruncatedBubble() {
        print("Selected truncated bubble")
    }
    
    func didSelectBubble(at index: Int) {
        print(index)
    }
}
