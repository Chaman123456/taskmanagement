
import UIKit

class Preferences {
    static let shared = Preferences()
    var enableTransitionAnimation = false
}

class MenuViewController: UIViewController {
    var isDarkModeEnabled = false
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
        }
    }
    
    @IBOutlet weak var profile_image: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    let arrTitle = ["Invite Friends", "My Tasks", "Logout"]
    let arrImage = ["invite_icon_menu", "mytask", "logout_icon_menu"]
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenuController?.cache(viewControllerGenerator: {
            UIStoryboard(name: "NewTask", bundle: nil).instantiateViewController(withIdentifier: "UserListViewController")
        }, with: "0")
        sideMenuController?.cache(viewControllerGenerator: {
            UIStoryboard(name: "MyTask", bundle: nil).instantiateViewController(withIdentifier: "MyTask")
        }, with: "1")
        sideMenuController?.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
    }
    override func viewWillAppear(_ animated: Bool) {
        setDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            self.view.backgroundColor = UIColor.black
            lblVersion.textColor = .white
        } else {
            self.view.backgroundColor = UIColor.white
            lblVersion.textColor = .gray
        }
        tableView.reloadData()
    }
    
    func setDetails() {
        self.lbl_name.text = Defaults.userName
        if Defaults.userName!.isEmpty {
            self.lbl_name.text = "No Name"
        }
        profile_image.kf.setImage(with: URL(string: Defaults.userImageUrl ?? ""), placeholder: UIImage(named: "one-1"), options: .none) { result in
        }
    }
    
    @IBAction func ProfileAction(_ sender: Any) {
        sideMenuController?.hideMenu()
    }
    
    @IBAction func EditAction(_ sender: Any) {
        selectedTabIndex = 4
        Utility.gotoTabbar()
        sideMenuController?.hideMenu()
    }
}

extension MenuViewController: SideMenuControllerDelegate {
    func sideMenuController(_ sideMenuController: SideMenuController,
                            animationControllerFrom fromVC: UIViewController,
                            to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BasicTransitionAnimator(options: .transitionFlipFromLeft, duration: 0.6)
    }
    
    func sideMenuController(_ sideMenuController: SideMenuController, willShow viewController: UIViewController, animated: Bool) {
        viewController.tabBarController?.tabBar.isHidden = false
        print("[Example] View controller will show [\(viewController)]")
    }
    
    func sideMenuController(_ sideMenuController: SideMenuController, didShow viewController: UIViewController, animated: Bool) {
        print("[Example] View controller did show [\(viewController)]")
    }
    
    func sideMenuControllerWillHideMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu will hide")
    }
    
    func sideMenuControllerDidHideMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu did hide.")
    }
    
    func sideMenuControllerWillRevealMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu will reveal.")
    }
    
    func sideMenuControllerDidRevealMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu did reveal.")
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionCell", for: indexPath) as! SelectionCell
        cell.lbl_title.text = arrTitle[indexPath.row]
        cell.btn_image.setImage( UIImage(named: arrImage[indexPath.row])
                                 , for: .normal)
        if self.traitCollection.userInterfaceStyle == .dark {
            cell.backgroundColor = UIColor.black
            cell.lbl_title.textColor = UIColor.white
        } else {
            cell.backgroundColor = UIColor.white
            cell.lbl_title.textColor = UIColor.black
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isFromMenu = false
        if indexPath.row == 0 {
            isFromMenu = true
        }
        if indexPath.row == 2 {
            let refreshAlert = UIAlertController(title: "", message: LOG_OUT, preferredStyle: UIAlertController.Style.alert)
            refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                Utility.logoutAction()
            }))
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                refreshAlert .dismiss(animated: true, completion: nil)
            }))
            self.present(refreshAlert, animated: true, completion: nil)
        } else {
            sideMenuController?.setContentViewController(with: "\(indexPath.row)", animated: Preferences.shared.enableTransitionAnimation)
            sideMenuController?.hideMenu()
        }
    }
}

class SelectionCell: UITableViewCell {
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var btn_image: UIButton!
}
