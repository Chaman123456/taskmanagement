//
//  ChatViewController.swift
//  TaskManagerApp
//
//  Created by chaman Sharma on 28/12/21.
//

import UIKit

class ChatViewController: UIViewController {
    @IBOutlet var messageTblView: UITableView!
    @IBOutlet var TagsCollectionView: UICollectionView!
    @IBOutlet var addNoteTextField: UITextField!
    @IBOutlet var btnVoiceTotext: UIButton!
    var messageArray = [String]()
    let tagMembersName = ["@Sam", "@Niole", "@Raj", "@Sam", "@Niole", "@Raj"]
    let senderMessage = ["Hello", "How are you", "Test1", "Test2", "Test3"]
    let receiverMessage = ["Earth has two such belts and sometimes others may be temporarily created.", "Earth has two such belts and sometimes others may be temporarily created.", "Earth has two such belts and sometimes others may be temporarily created.", "  Test2", "  Test3"]
    let senderMessageTime = ["10:34AM", "10:34AM", "10:34AM", "10:34AM"]
    let receiverMessageTime = ["10:34AM", "10:34AM", "10:34AM", "10:34AM", "10:34AM"]
    let receiverImage = ["download", "download", "download", "download", "Place to be" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnVoiceTotext.layer.cornerRadius = 25
    }
    
    @IBAction func sendMessage(sender: AnyObject) {
        messageArray.append(addNoteTextField.text!)
        messageTblView.reloadData()
        addNoteTextField.text = ""
    }
    @IBAction func beckAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

class TagMemberCollectionViewCell: UICollectionViewCell {
    @IBOutlet var lblTagMember: UILabel!
}

class ReceiverTableViewCell: UITableViewCell {
    @IBOutlet var imgMessage: UIImageView!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var lblMessageTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class SenderTableViewCell: UITableViewCell {
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var lblMessageTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

extension ChatViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return senderMessage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagMemberCollectionViewCell", for: indexPath) as! TagMemberCollectionViewCell
        cell.lblTagMember.text = self.tagMembersName[indexPath.row]
        return cell
    }
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section % 2 != 0) {
            return messageArray.count
        }
        return receiverMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section % 2 != 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SenderTableViewCell", for: indexPath) as! SenderTableViewCell
            cell.lblMessage.text = messageArray[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverTableViewCell", for: indexPath) as! ReceiverTableViewCell
            return cell
        }
    }
}

