//
//  HomeDetailViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 27/12/21.
//

import UIKit
import MapKit
import LocationPicker
import MobileCoreServices
import UniformTypeIdentifiers
import Kingfisher
import ContactsUI
import Contacts

class HomeDetailViewController: UIViewController, CategoryDelegate, CNContactViewControllerDelegate {
    var selectedCategories = [Category]()
    var userlist = UserListViewController()
    @IBOutlet var tfName: UITextField!
    @IBOutlet var viewCategory: UIView!
    @IBOutlet var textViewDetail: UITextView!
    @IBOutlet var tfDueDate: UITextField!
    @IBOutlet var lblAssign: UILabel!
    @IBOutlet var lblSubTaskDetail: UILabel!
    @IBOutlet var lblCat: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var collectionAssign: UICollectionView!
    @IBOutlet var collectionSubtask: UICollectionView!
    @IBOutlet var collectionAttachment: UICollectionView!
    @IBOutlet var collectionLocation: UICollectionView!
    @IBOutlet var tableViewMain: UITableView!
    @IBOutlet var titleAttachment: UILabel!
    @IBOutlet var attachmentHeight: NSLayoutConstraint!
    @IBOutlet var titleLocation: UILabel!
    @IBOutlet var locationHeight: NSLayoutConstraint!
    @IBOutlet var imageBg: UIImageView!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var btnFavorite: UIButton!
    @IBOutlet var errorNoSubtask: UILabel!
    @IBOutlet weak var noteText: UITextView!
    @IBOutlet var noteLabel: UILabel!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnStatus: UIButton!
    @IBOutlet var btnAddSubtask: UIButton!
    @IBOutlet var lblSubtask: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var titleDueDate: UILabel!
    @IBOutlet var createdDateLabel: UILabel?
    @IBOutlet var updateTaskButton: UIButton!
    @IBOutlet var creationView: UIView!
    @IBOutlet var gredientView: UIView!
    @IBOutlet var topViewForTable: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var btnCamera: UIButton!
    @IBOutlet var btnImage: UIButton!
    @IBOutlet var btnLocation: UIButton!
    let locationPicker = LocationPickerViewController()
    var selectedLocationIndex : Int?
    var arrAttachment = [[String: Any]]()
    var isAttachmentSelected = false
    var arrUser = [User]()
    var arrLocation = [[String: Any]]()
    var loginViewModel = HomeViewModel()
    var detailData : DetailDataModel?
    var arrSubTask: [Subtasks]?
    var statusType: Int?
    var selectedDate = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.taskDeleted(notification:)), name: Notification.Name("TaskDeleted"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.subTaskAdded(notification:)), name: Notification.Name("SubTaskAdded"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.shareTask(notification:)), name: Notification.Name("ShareTask"), object: nil)
        loginViewModel.delegate = self
        loginViewModel.setupCategoryScorll(categoryArray: detailData?.category ?? [])
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
            swipeRight.direction = .right
            self.view.addGestureRecognizer(swipeRight)
        setUpMode()
        setUpData()
        setupUI()
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                self.navigationController?.popViewController(animated: true)
            default:
                break
            }
        }
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            tableViewMain.backgroundColor = UIColor.clear
            bottomView.backgroundColor =  UIColor(red: 35.0/255.0, green: 35.0/255.0, blue: 35.0/255.0, alpha: 1.0)
            noteText?.backgroundColor = .black
            noteText?.layer.cornerRadius = 7
            btnCamera.setTitleColor(.white, for: .normal)
            btnImage.setTitleColor(.white, for: .normal)
            btnLocation.setTitleColor(.white, for: .normal)
            gredientView.isHidden = false
            createdDateLabel?.textColor = .white
            lblAssign.textColor = .white
            lblSubTaskDetail.textColor = .white
            errorNoSubtask.textColor = .white
            lblCat.textColor = .white
            titleAttachment.textColor = .white
            titleLocation.textColor = .white
            lblDesc.textColor = .white
            textViewDetail.textColor = .white
            noteLabel.textColor = .white
        } else {
            gredientView.isHidden = true
            noteLabel.textColor = .black
            view.backgroundColor = UIColor.white
            bottomView.backgroundColor =  UIColor(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0)
            noteText?.backgroundColor = .white
            noteText?.layer.borderColor = UIColor.black.cgColor
            noteText?.layer.borderWidth = 1
            noteText?.layer.cornerRadius = 7
            noteText?.textColor = .gray
            noteText?.textColor = .gray
            btnCamera.setTitleColor(.black, for: .normal)
            btnImage.setTitleColor(.black, for: .normal)
            btnLocation.setTitleColor(.black, for: .normal)
            createdDateLabel?.textColor = .black
            lblAssign.textColor = .black
            lblSubTaskDetail.textColor = .black
            errorNoSubtask.textColor = .black
            lblCat.textColor = .black
            titleAttachment.textColor = .black
            titleLocation.textColor = .black
            lblDesc.textColor = .black
            textViewDetail.textColor = .gray
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func taskDeleted(notification: Notification) {
        let alertController = UIAlertController(title: title, message: "Would you like to delete this task?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "No", style: .default) { action in
        }
        let yesAction = UIAlertAction(title: "Yes", style: .default) { action in
            let getAllCategoriesOperation = GetAllCategoriesOperation()
            getAllCategoriesOperation.deleteTask(taskId:self.detailData?._id ?? "") { error, response in
                if response == nil{
                    self.view.makeToast(SOMETHING_WENT_WRONG_STR)
                    return
                } else {
                    self.navigationController?.popViewController(animated: true)
                    self.navigationController?.view.makeToast(TASK_DELETED)
                }
            }
        }
        alertController.addAction(okAction)
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func subTaskAdded(notification: Notification) {
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func EditButton(_ sender: Any) {
        tfName.becomeFirstResponder()
    }
    
    @objc func shareTask(notification: Notification){
        DispatchQueue.main.async {
            let activityViewController = UIActivityViewController(activityItems: ["Please install Task Management App to manage tasks and money together click https://apps.apple.com/us/app/task-mgmt/id1609609713"], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectDate(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSZ"
        selectedDate = dateFormatter.string(from: sender.date)
    }
    
    func setupUI(){
        if Defaults.userId != detailData?.userId {
            tfName.isUserInteractionEnabled = false
            btnEdit.isHidden = true
            textViewDetail.isUserInteractionEnabled = false
            tfDueDate.isUserInteractionEnabled = false
            btnAddSubtask.isHidden = true
            noteText?.isUserInteractionEnabled = false
            updateTaskButton.isHidden = true
        }
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.preferredDatePickerStyle = .inline
        datePickerView.minimumDate = Date()
        tfDueDate.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        tfDueDate.text = dateFormatter.string(from: sender.date)
        selectedDate = dateFormatter.string(from: sender.date)
    }
    
    func setUpData() {
        tfName.text = detailData?.name
        textViewDetail.text = detailData?.description
        if detailData?.getting_started == true{
            statusType = 1
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            lblStatus.text = "Getting Started"
        } else if detailData?.in_progress == true {
            statusType = 2
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            lblStatus.text = "In Progress"
        } else if detailData?.incomplete == true {
            statusType = 3
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            lblStatus.text = "Completed"
        } else if detailData?.wont_abble_to_perform == true {
            statusType = 4
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            lblStatus.text = "Won't be able to perform"
        } else {
            statusType = 1
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            lblStatus.text = "Getting Started"
        }
        
        let isoDate = detailData?.assignDate ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSZ"
        let date = dateFormatter.date(from: isoDate)
        tfDueDate.text = Defaults.getFormattedDate(string: isoDate, formatter: "MMM dd, yyyy")
        createdDateLabel?.text = Defaults.getFormattedDate(string: detailData?.created_at ?? "" , formatter: "MMM dd, yyyy")
        
        if date != nil {
            selectedDate = dateFormatter.string(from: date!)
        }
        
        if Date().compare(date!) == ComparisonResult.orderedSame {
            titleDueDate.textColor = .yellow
            tfDueDate.textColor = .yellow
        }else if Date().compare(date!) == ComparisonResult.orderedDescending {
            titleDueDate.textColor = .red
            tfDueDate.textColor = .red
        }
        
        let path = detailData?.backgroundImages?.first?.path
        if path != ""{
            let fullPathArr = path?.components(separatedBy: "public")
            imageBg.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "no_image"), options: .none) { result in
            }
        }
        if detailData?.isFavorite == true {
            btnFavorite.setImage(UIImage(named: "star_gold"), for: .normal)
        } else {
            btnFavorite.setImage(UIImage(named: "star"), for: .normal)
        }
        if !detailData!.assignId!.isEmpty {
            for item in detailData!.assignId! {
                let user = User()
                user.userId = item["userId"] as? String ?? ""
                user.userName = item["type"] as? String ?? ""
                user.assigneeStatus = item["assigneeStatus"] as? String ?? ""
                let resultUser = GlobalData.allUsers.filter({ checkUser in
                    return checkUser.userId == user.userId
                })
                if !resultUser.isEmpty {
                    if resultUser.last?.phoneNumber != ""{
                        user.phoneNumber = resultUser.last?.phoneNumber
                    }
                    if resultUser.last?.userName != ""{
                        user.userName = resultUser.last?.userName
                    }
                }
                arrUser.append(user)
            }
            collectionAssign.reloadData()
            Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { timer in
                self.setScrollOfAssignedUserCollectionView()
            }
        }
        if !detailData!.latlong_location!.isEmpty {
            for location in detailData!.latlong_location! {
                let clLocation =  CLLocation(latitude: Double(location.latitude!)!, longitude: Double(location.logitude!)!)
                let locData = ["address":location.address ?? "",
                               "location":clLocation,
                               "placemark" : ""
                ] as [String : Any]
                self.arrLocation.append(locData)
            }
            if self.arrLocation.count > 0 {
                self.locationHeight.constant = 180
                self.titleLocation.isHidden = false
            }else{
                self.locationHeight.constant = 0
                self.titleLocation.isHidden = true
            }
            collectionLocation.reloadData()
        }
        selectedCategories = detailData!.category!
        arrSubTask = detailData?.subtasks
        if arrSubTask?.count ?? 0 < 1 {
            lblSubtask.text = "Crunches"
        } else {
            if arrSubTask?.count == 1 {
                lblSubtask.text = "\(arrSubTask?.count ?? 0)  Crunch"
            }else{
                lblSubtask.text = "\(arrSubTask?.count ?? 0)  Crunches"
            }
        }
        if arrSubTask?.count ?? 0 > 0 {
            errorNoSubtask.isHidden = true
        } else {
            errorNoSubtask.isHidden = false
        }
        noteText!.text = detailData?.notes
        
        for imageData in detailData!.multifilesImages ?? [] {
            arrAttachment.append(["type": 1, "Info": imageData.path!, "fileName" : imageData.filename!])
        }
        if arrAttachment.count > 0 {
            titleAttachment.isHidden = false
            attachmentHeight.constant = 140
        } else {
            titleAttachment.isHidden = true
            attachmentHeight.constant = 0
        }
        collectionAttachment.reloadData()
    }
    
    func setScrollOfAssignedUserCollectionView() {
        if Defaults.userId != detailData?.userId {
            self.collectionAssign.scrollToItem(at: IndexPath(item: self.arrUser.count - 1, section: 0), at: UICollectionView.ScrollPosition.right, animated: false)
        } else {
            self.collectionAssign.scrollToItem(at: IndexPath(item: self.arrUser.count, section: 0), at: UICollectionView.ScrollPosition.right, animated: false)
        }
    }
    
    @IBAction func btnFavAction(_ sender: Any) {
        let favoriteTaskOperation = FavoriteTaskOperation()
        favoriteTaskOperation.createTaskFavorite(isFavorite : !detailData!.isFavorite!, taskId: detailData?._id ?? "") { str, response, success in
            if success {
                if self.detailData!.isFavorite! {
                    self.detailData?.isFavorite = false
                    self.btnFavorite.setImage(UIImage(named: "star"), for: .normal)
                } else {
                    self.detailData?.isFavorite = true
                    self.btnFavorite.setImage(UIImage(named: "star_gold"), for: .normal)
                }
            }
        }
    }
    
    @IBAction func beckAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addNewSubTaskAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SubTaskViewController") as! SubTaskViewController
        vc.taskId = detailData?._id
        vc.userId = detailData?.userId
        vc.maxDate = detailData?.assignDate
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addNewCategory(_ sender: Any) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        let sb = UIStoryboard(name: "NewTask", bundle: nil)
        let vc = (sb.instantiateViewController(identifier: "CategoriesViewController")) as! CategoriesViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        vc.allcategories = GlobalData.allCategories
        vc.delegate = self
        vc.selectedCategories = selectedCategories
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func taskStatusAction(_ sender: Any) {
        if Defaults.userId != detailData?.userId{
            self.view.makeToast(NOT_AUTHORISED)
            return
        }
        let vc = (storyboard?.instantiateViewController(identifier: "AddStatusViewController")) as! AddStatusViewController
        vc.currentStatus = statusType
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.clear
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    @IBAction func updateTaskAPI(_ sender: Any) {
        let task = Task()
        task.name = tfName.text ?? ""
        task.description = textViewDetail.text ?? ""
        task.notes = noteText?.text ?? ""
        task.categoryIds = selectedCategories
        task.assignedDate = "\(selectedDate)"
        var assignedUsers = [[String : Any]]()
        for user in arrUser {
            var dict = [String : Any]()
            if user.userId == nil {
                dict = ["userId" : "", "type": user.userName ?? "","assigneeStatus": AssigneeStatus.invited.rawValue, "phoneNumber": user.phoneNumber ?? ""]
            }else{
                dict = ["userId" : user.userId ?? "", "type": user.userName ?? "","assigneeStatus": AssigneeStatus.pending.rawValue, "phoneNumber": ""]
            }
            assignedUsers.append(dict as [String : Any])
        }
        var locations = [[String : Any]]()
        for location in arrLocation {
            let latLong = location["location"] as! CLLocation
            let dict = ["latitude" : latLong.coordinate.latitude, "logitude": latLong.coordinate.longitude, "address": location["address"] ?? ""]
            locations.append(dict as [String : Any])
        }
        task.assignedUser = assignedUsers
        task.location = locations
        
        if task.name.isEmpty{
            self.view.makeToast(PLEASE_ENTER_THE_TASK_NAME)
            return
        }
        
        if  task.categoryIds.isEmpty {
            self.view.makeToast(PLEASE_ENTER_THE_CATEGORY)
            return
        }
        
        if task.assignedDate.isEmpty  {
            self.view.makeToast(PLEASE_ENTER_THE_ASSIGN_DATE)
            return
        }
        
        if arrUser.count < 1 {
            self.view.makeToast(PLEASE_ASSIGN_USER)
            return
        }
        
        let attachmentArray : [[String : Any]] = arrAttachment.filter { data in
            return (data["type"] as? Int) == 0
        }
        let updateTaskOperation = UpdateTaskOperation(withTask: task, image: imageBg.image!, arrimage: attachmentArray)
        updateTaskOperation.execute(taskId: detailData?._id ?? "") { str, upcomingData in
            self.navigationController?.view.makeToast(TASK_UPDATED)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func attachAction(_ sender: Any) {
        attachDocument()
    }
    
    @IBAction func takeImage(sender: AnyObject) {
        isAttachmentSelected = true
        openCamera()
    }
    
    @IBAction func galaryAction(_ sender: Any) {
        isAttachmentSelected = true
        openGalary()
    }
    
    @IBAction func locationAction() {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.tintColor = .white
        locationPicker.showCurrentLocationButton = true
        locationPicker.currentLocationButtonBackground = .black
        locationPicker.showCurrentLocationInitially = true
        locationPicker.mapType = .standard
        locationPicker.useCurrentLocationAsHint = true
        locationPicker.searchBarPlaceholder = "Search places"
        locationPicker.searchTextFieldColor = .gray
        locationPicker.searchBarStyle = .minimal
        locationPicker.searchHistoryLabel = "Search History"
        locationPicker.resultRegionDistance = 600000
        if let index = selectedLocationIndex {
            let locData = self.arrLocation[index]
            locationPicker.location = Location(name: locData["address"] as? String, location: locData["location"] as? CLLocation, placemark: locData["placemark"] as! CLPlacemark)
        } else {
            locationPicker.location = nil
        }
        locationPicker.completion = { [self] location in
            let locData = ["address":location!.address,
                           "location":location!.location,
                           "placemark" : location!.placemark
            ] as [String : Any]
            self.arrLocation.append(locData)
            if self.arrLocation.count > 0{
                self.locationHeight.constant = 180
                self.titleLocation.isHidden = false
            } else {
                self.locationHeight.constant = 0
                self.titleLocation.isHidden = true
            }
            DispatchQueue.main.async {
                self.collectionLocation.reloadData()
            }
        }
        navigationController?.pushViewController(locationPicker, animated: true)
    }
    
    @IBAction func moreAction(_ sender: Any) {
        let vc = (storyboard?.instantiateViewController(identifier: "DeleteTaskViewController")) as! DeleteTaskViewController
        if Defaults.userId == detailData?.userId {
            vc.sideMenuType = SideMenuType.both
        } else {
            vc.sideMenuType = SideMenuType.share
        }
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.present(vc, animated: false, completion: nil)
    }
    
    private func attachDocument() {
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypeText),String(kUTTypeContent),String(kUTTypeItem),String(kUTTypeData)], in: .import)
        documentPicker.delegate = self
        self.present(documentPicker, animated: true)
    }
    
    func callCreateCategoryApi(categoryName : String, desc : String) {
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.createCategory(parameters: ["name" : categoryName, "description" : desc, "published" : true]) { error, category in
            self.view.makeToast("Category created successfully.")
        }
    }
    
    func actionsheet() {
        let alert  = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (handler) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (handler) in
            self.openGalary()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (handler) in
            self.isAttachmentSelected = false
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let image = UIImagePickerController()
            image.allowsEditing = true
            image.delegate = self
            image.sourceType = .camera
            image.mediaTypes = [kUTTypeImage as String]
            self.present(image, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Camera", message: "Camera is Not available", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGalary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let image = UIImagePickerController()
            image.allowsEditing = true
            image.delegate = self
            self.present(image, animated: true, completion: nil)
        }
    }
    
    func callTaskStatusApi(taskId: String, getting_started:Int, in_progress:Int, completed:Int, wont_abble_to_perform:Int) {
        let getStatusTaskOperation = GetStatusTaskOperation()
        let params = [
            "userId" : Defaults.userId ?? "",
            "taskId": taskId,
            "getting_started": getting_started,
            "in_progress": in_progress,
            "incomplete": completed,
            "wont_abble_to_perform": wont_abble_to_perform
        ] as [String : Any]
        getStatusTaskOperation.getTaskStatus(parameters: params) { error, taskStatus in
            if getting_started == 1{
                self.lblStatus.textColor = #colorLiteral(red: 0.4500938654, green: 0.9813225865, blue: 0.4743030667, alpha: 1)
                self.lblStatus.text = "Getting Started"
            } else if in_progress == 1 {
                self.lblStatus.textColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
                self.lblStatus.text = "In Progress"
            } else if completed == 1 {
                self.lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
                self.lblStatus.text = "Completed"
            }  else if wont_abble_to_perform == 1 {
                self.lblStatus.textColor = #colorLiteral(red: 1, green: 0.4932718873, blue: 0.4739984274, alpha: 1)
                self.lblStatus.text = "Won’t be able to perform"
            } else {
                self.lblStatus.text = "Getting Started"
            }
            self.view.makeToast("Task status changed successfully.")
        }
    }
    
    //MARK:  CategoryDelegate
    func categoriesDidSelected(categoriesSelected: [Category],categoriesSelectedLongPress: [Category]) {
        selectedCategories = categoriesSelected
        for vw in self.viewCategory.subviews {
            if vw is CategoryScroll{
                vw.removeFromSuperview()
            }
        }
        let categoryScroll = CategoryScroll(frame: viewCategory.bounds)
        categoryScroll.setupScroll(itemArray: selectedCategories)
        categoryScroll.updateItem(selectedValue: -1)
        categoryScroll.categoryScrollDelegate = self
        for vw in categoryScroll.subviews {
            vw.isUserInteractionEnabled = false
        }
        self.viewCategory.addSubview(categoryScroll)
    }
    
    func didShowAll() {
        
    }
    
    @IBAction func deleteLocationAction(sender:UIButton) {
        let alertController = UIAlertController(title: title, message: "Would you like to delete this Location?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "No", style: .default) { action in
        }
        let yesAction = UIAlertAction(title: "Yes", style: .default) { action in
            self.arrLocation.remove(at: sender.tag)
            if self.arrLocation.count > 0 {
                self.locationHeight.constant = 180
                self.titleLocation.isHidden = false
            }else{
                self.locationHeight.constant = 0
                self.titleLocation.isHidden = true
            }
            self.collectionLocation.reloadData()
        }
        alertController.addAction(okAction)
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

class AssignCollectionCell: UICollectionViewCell{
    @IBOutlet var imageUser: UIImageView!
    @IBOutlet weak var lblAssignedName: UILabel!
}

class SubtaskCollectionCell: UICollectionViewCell{
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}

class AttachmentCollectionCell: UICollectionViewCell{
    @IBOutlet weak var itemImageView: UIImageView!
}

class LocationCollectionCell: UICollectionViewCell{
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var lblLocationName: UILabel!
    @IBOutlet var btnRemoveLocation: UIButton!
}

extension HomeDetailViewController : HomeViewModelProtocol {
    func setupCategoryScorllDone(categoryArray: [Category]) {
        for vw in self.viewCategory.subviews {
            if vw is CategoryScroll{
                vw.removeFromSuperview()
            }
        }
        let categoryScroll = CategoryScroll(frame: viewCategory.bounds)
        categoryScroll.setupScroll(itemArray: categoryArray)
        categoryScroll.updateItem(selectedValue: -1)
        categoryScroll.categoryScrollDelegate = self
        for vw in categoryScroll.subviews {
            vw.isUserInteractionEnabled = false
        }
        self.viewCategory.addSubview(categoryScroll)
    }
    
    func populateCategries(categoryItemArray: [Any]) {
    }
    
    func setupCategoryScorllDone(categoryArray: [Any]) {
    }
}

extension HomeDetailViewController : CategoryScrollProtocol {
    func categoryChoosed(selectedItemName: String) {
    }
}

extension HomeDetailViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionAssign{
            if Defaults.userId != detailData?.userId {
                return arrUser.count
            }
            return arrUser.count + 1
        } else if collectionView == collectionAttachment{
            return arrAttachment.count
        } else if collectionView == collectionLocation {
            return arrLocation.count
        } else {
            return arrSubTask?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionAssign {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssignCollectionCell", for: indexPath) as! AssignCollectionCell
            cell.imageUser.layer.cornerRadius = cell.imageUser.frame.height/2
            if indexPath.row < arrUser.count {
                cell.imageUser.backgroundColor = .white
                cell.imageUser.image = UIImage(named: "one-1")
                cell.lblAssignedName.isHidden = false
                print("\(arrUser[indexPath.row].userName ?? "")\n\(arrUser[indexPath.row].assigneeStatus)")
                cell.lblAssignedName.text = "\(arrUser[indexPath.row].userName ?? "")\n\(arrUser[indexPath.row].assigneeStatus)"
                if self.traitCollection.userInterfaceStyle == .dark {
                    cell.lblAssignedName.textColor = .white
                } else {
                    cell.lblAssignedName.textColor = .black
                }
            } else {
                cell.lblAssignedName.text = ""
                cell.imageUser.backgroundColor = .clear
                cell.imageUser.image = UIImage(named: "add-friend")
            }
            return cell
        } else if collectionView == collectionAttachment {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCollectionCell", for: indexPath) as! AttachmentCollectionCell
            if arrAttachment[indexPath.row]["type"] as? Int == 0 {
                cell.itemImageView.image = arrAttachment[indexPath.row]["Info"] as? UIImage
            } else {
                let path = arrAttachment[indexPath.row]["Info"] as! String
                let fullPathArr = path.components(separatedBy: "public")
                cell.itemImageView.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr[1])"), placeholder: UIImage(named: "no_image"), options: .none) { result in
                }
            }
            return cell
        } else if collectionView == collectionLocation {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationCollectionCell", for: indexPath) as! LocationCollectionCell
            cell.btnRemoveLocation.tag = indexPath.row
            if Defaults.userId != detailData?.userId {
                cell.btnRemoveLocation.isHidden = true
            } else {
                cell.btnRemoveLocation.isHidden = false
            }
            let name = arrLocation[indexPath.row]
            let randomValue = name["address"]!
            cell.lblLocationName.text = randomValue as? String
            if self.traitCollection.userInterfaceStyle == .dark {
                cell.lblLocationName.textColor = .white
                cell.lblLocationName.backgroundColor = .black
            } else {
                cell.lblLocationName.backgroundColor = .white
                cell.lblLocationName.textColor = .black
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubtaskCollectionCell", for: indexPath) as! SubtaskCollectionCell
            let data = arrSubTask?[indexPath.row]
            let path = data?.backgroundImages?.first?.path
            if path != ""{
                let fullPathArr = path?.components(separatedBy: "public")
                cell.itemImageView.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "no_image"), options: .none) { result in
                }
            }
            cell.dateLabel.text = Defaults.getFormattedDate(string: data?.sub_assignDate ?? "", formatter: "MMM dd,yyyy")
            cell.itemNameLabel.text = data?.sub_name
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionAssign {
            if indexPath.row >= arrUser.count {
                let sb = UIStoryboard(name: "NewTask", bundle: nil)
                let vc = (sb.instantiateViewController(identifier: "UserListViewController")) as! UserListViewController
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            } else {
                if Defaults.userId != detailData?.userId {
                    let leaveAlert = UIAlertController(title: "", message: "Name: \(arrUser[indexPath.row].userName ?? "")\nPhone: \(arrUser[indexPath.row].phoneNumber ?? "")", preferredStyle: UIAlertController.Style.alert)
                    leaveAlert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action: UIAlertAction!) in
                    }))
                    self.present(leaveAlert, animated: true, completion: nil)
                } else {
                    let leaveAlert = UIAlertController(title: "", message: "Name: \(arrUser[indexPath.row].userName ?? "")\nPhone: \(arrUser[indexPath.row].phoneNumber ?? "")", preferredStyle: UIAlertController.Style.alert)
                    leaveAlert.addAction(UIAlertAction(title: "Remove", style: .destructive, handler: { (action: UIAlertAction!) in
                        self.arrUser.remove(at: indexPath.row)
                        self.collectionAssign.reloadData()
                        self.setScrollOfAssignedUserCollectionView()
                    }))
                    leaveAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                    }))
                    self.present(leaveAlert, animated: true, completion: nil)
                }
            }
        } else if collectionView == collectionLocation {
            let dict = self.arrLocation[indexPath.item]
            let latLong = dict["location"]
            let clLocation:CLLocation =  latLong as! CLLocation
            Utility.openGoogleMap(pickup_lat: clLocation.coordinate.latitude, pickup_long: clLocation.coordinate.longitude)
        }else if collectionView == collectionSubtask {
            let data = arrSubTask?[indexPath.row]
            let vc = storyboard?.instantiateViewController(withIdentifier: "SubTaskViewController") as! SubTaskViewController
            vc.userId = detailData?.userId
            vc.taskId = detailData?._id
            vc.sub_taskId = detailData?.subtasks?[indexPath.row]._id
            vc.isPreviousTask = true
            vc.selectedTask = arrSubTask?[indexPath.row]
            vc.selIndex = indexPath.row
            vc.maxDate = detailData?.assignDate
            vc.sub_assignDate = data?.sub_assignDate
            self.navigationController?.pushViewController(vc, animated: true)
        } else if collectionView == collectionAttachment {
            let sb = UIStoryboard(name: "Home", bundle: nil)
            let vc = (sb.instantiateViewController(identifier: "FullImageViewController")) as! FullImageViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.9)
            if arrAttachment[indexPath.row]["type"] as? Int == 0 {
                vc.image = arrAttachment[indexPath.row]["Info"] as? UIImage
            } else {
                let path = arrAttachment[indexPath.row]["Info"] as! String
                let fullPathArr = path.components(separatedBy: "public")
                vc.img = "\(BaseURL)\(fullPathArr[1])"
            }
            vc.taskId = self.detailData?._id ?? ""
            vc.fileName = arrAttachment[indexPath.row]["fileName"] as? String ?? ""
            vc.delegate = self
            vc.index = indexPath.row
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionAssign {
            return CGSize(width: 60, height: 70)
        } else if collectionView == collectionLocation {
            return CGSize(width: 130, height: 180)
        } else {
            return CGSize(width: collectionView.frame.width/1.5, height: 140)
        }
    }
}

extension HomeDetailViewController: assignedUserProtocol {
    func userAssigned(user: User) {
        if arrUser.count >= 10 {
            let alert = UIAlertController(title: "", message: "Assign user limit exceeded.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if arrUser.contains(where: { $0.userId == (user).userId }) {
            DispatchQueue.main.async {
                let leaveAlert = UIAlertController(title: "", message: USER_ALREADR_ADDED, preferredStyle: UIAlertController.Style.alert)
                leaveAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                }))
                self.present(leaveAlert, animated: true, completion: nil)
            }
            return
        }
        arrUser.append(user)
        collectionAssign.reloadData()
        setScrollOfAssignedUserCollectionView()
    }
}

extension HomeDetailViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        if isAttachmentSelected == true{
            let arr = ["type": 0,
                       "Info": tempImage] as [String : Any]
            arrAttachment.append(arr)
            if arrAttachment.count > 0{
                titleAttachment.isHidden = false
                attachmentHeight.constant = 140
            } else {
                titleAttachment.isHidden = true
                attachmentHeight.constant = 0
            }
            collectionAttachment.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension HomeDetailViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let arr = ["type": 1,
                   "Info": urls[0]] as [String : Any]
        arrAttachment.append(arr)
        collectionAttachment.reloadData()
    }
}

extension HomeDetailViewController: addStatusDelegate {
    func messageData(data: Int) {
        if let taskId = detailData?._id {
            statusType = data
            if data == 1 {
                self.callTaskStatusApi(taskId: taskId, getting_started: data, in_progress: 0, completed: 0, wont_abble_to_perform: 0)
            } else if data == 2 {
                self.callTaskStatusApi(taskId: taskId, getting_started: 0, in_progress: 1, completed: 0, wont_abble_to_perform: 0)
            } else if data == 3 {
                self.callTaskStatusApi(taskId: taskId, getting_started: 0, in_progress: 0, completed: 1, wont_abble_to_perform: 0)
            } else if data == 4 {
                self.callTaskStatusApi(taskId: taskId, getting_started: 0, in_progress: 0, completed: 0, wont_abble_to_perform: 1)
            }
        }
    }
}

extension HomeDetailViewController: FullImageViewControllerDelegate {
    func attachmentDeleted(index: Int) {
        arrAttachment.remove(at: index)
        collectionAttachment.reloadData()
    }
}
