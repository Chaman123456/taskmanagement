//
//  FriendsViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 16/05/22.
//

import UIKit

class FriendsViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var btnBack: UIButton!
    @IBOutlet weak var tableFriendList: UITableView!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var lblfriends: UILabel!
    @IBOutlet var txtSearchField: UITextField! {
        didSet{
            txtSearchField.setLeftView(image: UIImage.init(named: "search-24px")!)
        }
    }
    var allUser = [User]()
    var arrFilter = [User]()
    @IBOutlet var noItemView: UIView?
    @IBOutlet var noItemLabel1: UILabel?
    @IBOutlet var noItemLabel2: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearchField.delegate = self
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        textfieldCustomise()
        getUser()
    }
    
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            topview.backgroundColor  = .black
            lblfriends.textColor = .white
            txtSearchField.backgroundColor = .gray
            txtSearchField.textColor = .white
            noItemLabel1?.textColor = .white
            noItemLabel2?.textColor = .white
        } else {
            view.backgroundColor = UIColor.white
            topview.backgroundColor  = .white
            lblfriends.textColor = .black
            txtSearchField.backgroundColor = .lightGray
            txtSearchField.textColor = .black
            txtSearchField.placeholderColor = UIColor.init(red: 105/255, green: 105/255, blue: 105/255, alpha: 1)
            noItemLabel1?.textColor = .black
            noItemLabel2?.textColor = .black
        }
        tableFriendList.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func getUser(){
        let profileOperation = ProfileOperation()
        profileOperation.getAllUsers { error, users in
            GlobalData.allUsers = users ?? []
            self.allUser = GlobalData.allUsers.filter({ data in
                if (GlobalData.assignedUser.contains(data.userId!)){
                    return true
                } else {
                    return false
                }
            })
            let temp = GlobalData.allUsers.filter({ $0.userId != Defaults.userId })
            self.allUser = temp
            self.arrFilter = self.allUser
            self.tableFriendList.reloadData()
        }
    }
    
    func textfieldCustomise() {
        txtSearchField.attributedPlaceholder = NSAttributedString(
            string: "Search by friend name",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = txtSearchField.text as NSString? {
            let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
            arrFilter = allUser.filter { ($0.userName?.localizedCaseInsensitiveContains(txtAfterUpdate))!}
            if arrFilter.count < 1 {
                arrFilter = allUser.filter{ $0.phoneNumber!.localizedCaseInsensitiveContains(txtAfterUpdate) }
            }
            if arrFilter.isEmpty {
                noItemView?.isHidden = false
            } else {
                noItemView?.isHidden = true
            }
            tableFriendList.reloadData()
        }
        return true
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        noItemView?.isHidden = true
        txtSearchField.text = ""
        self.arrFilter.removeAll()
        self.arrFilter = self.allUser
        self.tableFriendList?.reloadData()
    }
    
    @IBAction func beckAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionNotification(_ sender: Any) {
    }
    
    @IBAction func actionMore(_ sender: Any) {
    }

}


class FriendsCell: UITableViewCell{
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    @IBOutlet var lblIMG: UILabel!
}


extension FriendsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userData = arrFilter[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsCell", for: indexPath) as! FriendsCell
        if userData.userName != "" {
            cell.lblTitle.text = userData.userName
        } else {
            cell.lblTitle.text = userData.phoneNumber
        }
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            cell.lblTitle.textColor = .white
            cell.lblSubTitle.textColor = .white
        } else {
            view.backgroundColor = UIColor.white
            cell.lblTitle.textColor = UIColor.init(red: 30/255, green: 30/255, blue: 30/255, alpha: 1)
            cell.lblSubTitle.textColor = UIColor.init(red: 30/255, green: 30/255, blue: 30/255, alpha: 1)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userData = arrFilter[indexPath.row]
        let vc = storyboard?.instantiateViewController(identifier: "TaskListViewController") as! TaskListViewController
        if userData.userName != ""{
            vc.myTitle = userData.userName
        }else{
            vc.myTitle = userData.phoneNumber
        }
        vc.userId = userData.userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
