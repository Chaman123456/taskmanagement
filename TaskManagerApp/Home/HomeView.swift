//
//  HomeView.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 09/12/21.
//

import UIKit
import BubblePictures
import Lottie

class HomeView: UIViewController {
    var biometricIDAuth: BiometricIDAuth? = nil
    @IBOutlet weak var noTaskView: UIView!
    @IBOutlet var searchText: UITextField!
    @IBOutlet var itemsTableView: UITableView!
    @IBOutlet var customListView: UIView!
    var homeViewModel = HomeViewModel()
    var storedOffsets = [Int: CGFloat]()
    var allTask: [DetailDataModel] = []
    var filteredTask: [DetailDataModel] = []
    var selCateogry: String?
    var categoryScroll : CategoryScroll?
    var sections = [GroupedSection<Date, DetailDataModel>]()
    var selectedSection = 0
    @IBOutlet var TopLbl : UILabel!
    @IBOutlet var welcomeLabel : UILabel!
    var welcomeAnimation : AnimationView?
    @IBOutlet var welcomeAnimationView : UIView?

    override func viewDidLoad() {
        super.viewDidLoad()
        //Check For Biomeric Configuration
        //checkAuthentication()
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.filterData(notification:)), name: Notification.Name("FilterData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFirstTaskView(notification:)), name: Notification.Name("UpdateFirstTaskView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTableUIView(notification:)), name: Notification.Name("UpdateTableUIView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.viewedBy(notification:)), name: Notification.Name("ViewedBy"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        
        //        if !(Defaults.isFirstTimeLogin ?? false) {
        //            firstTimeLoginUser()
        //            Defaults.isFirstTimeLogin = true
        //        }
        
        //Lottie animation for thumb animation
        let animationPath = Bundle.main.path(forResource: "welcome", ofType: "json") ?? ""
        welcomeAnimation = AnimationView(filePath: animationPath)
        welcomeAnimation!.frame = welcomeAnimationView!.bounds
        welcomeAnimation!.loopMode = .repeat(100)
        welcomeAnimationView?.addSubview(welcomeAnimation!)
        welcomeAnimation?.play()
    }
    
    @objc func setUpMode() {
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
            if self.traitCollection.userInterfaceStyle == .dark {
                self.view.backgroundColor = .black
                self.welcomeLabel?.textColor = .white
            } else {
                self.view.backgroundColor = .white
                self.welcomeLabel?.textColor = .black
            }
            self.itemsTableView.reloadData()
        }
        Utility.callInitialData()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        // Trait collection has already changed
    }

    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        // Trait collection will change. Use this one so you know what the state is changing to.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCategories()
        getAllTask()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func getCategories() {
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.showLoader = false
        getAllCategoriesOperation.execute { str, categories in
            GlobalData.allCategories = categories ?? []
            self.homeViewModel.delegate = self
            self.homeViewModel.setupCategoryScorll(categoryArray: GlobalData.allCategories)
        }
    }
    
    private func firstDayOfMonth(date: Date) -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        return calendar.date(from: components)!
    }

    private func parseDate(_ str : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSZ"
        let date = dateFormatter.date(from: str) ?? Date()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: dateFormatter.string(from: date)) ?? Date()
    }
    
    func checkAuthentication() {
        biometricIDAuth = BiometricIDAuth(policy: .deviceOwnerAuthentication)
        biometricIDAuth?.canEvaluate { (canEvaluate, _, canEvaluateError) in
            if canEvaluate {
                biometricIDAuth?.evaluate {(success, error) in
                    if success {
                        print("Success")
                    } else {
                        self.checkAuthentication()
                    }
                }
            }
        }
    }
    
    @objc func updateTableUIView(notification: Notification){
        itemsTableView.reloadData()
    }
    
    @objc func viewedBy(notification: Notification){
        if notification.object as! Int == 0{
            let vc = storyboard?.instantiateViewController(withIdentifier: "FriendsViewController") as! FriendsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func filterData(notification: Notification) {
        if Defaults.sortBy == 1 {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.allTask).sorted {($0.created_at)! > ($1.created_at)!}
        } else if Defaults.sortBy == 2 {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.allTask).sorted {($0.created_at)! < ($1.created_at)!}
        }
       
        if Defaults.filterBy == 1 {
            TopLbl.text = "All Tasks"
        } else if Defaults.filterBy == 2 {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.allTask).filter({ $0.in_progress == true })
            TopLbl.text = "In Progress"
        } else if Defaults.filterBy == 3 {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.allTask).filter({ $0.incomplete == true })
            TopLbl.text = "In Complete"
        }
        
        //create section
        self.sections = GroupedSection.group(rows: self.filteredTask, by: { self.firstDayOfMonth(date: self.parseDate($0.updated_at ?? "")) })
        if Defaults.sortBy == 1 {
            self.sections.sort { lhs, rhs in lhs.sectionItem > rhs.sectionItem }
        } else if Defaults.sortBy == 2 {
            self.sections.sort { lhs, rhs in lhs.sectionItem < rhs.sectionItem }
        }
        
        if self.filteredTask.isEmpty {
            self.noTaskView?.isHidden = false
            self.welcomeAnimation?.play()
        } else {
            self.noTaskView?.isHidden = true
        }
        itemsTableView.reloadData()
    }
    
    @objc func updateFirstTaskView(notification: Notification) {
        selectedTabIndex = 2
        Utility.gotoTabbar()
    }
        
    func getAllTask() {
        let getAllTasksOperation = GetAllTasksOperation()
        getAllTasksOperation.execute {str, response in
            self.allTask.removeAll()
            if response?.recent?.count ?? 0 > 0{
                for a in (response?.recent)!{
                    self.allTask.append(a)
                    let detail : DetailDataModel = a as DetailDataModel
                    let array = detail.assignId
                    for assignUserList in array! {
                        let userId = assignUserList["userId"]
                        GlobalData.assignedUser.append(userId as! String)
                    }
                }
            }
            if response?.today?.count ?? 0 > 0{
                for a in (response?.today)!{
                    self.allTask.append(a)
                    let detail : DetailDataModel = a as DetailDataModel
                    let array = detail.assignId
                    for assignUserList in array! {
                        let userId = assignUserList["userId"]
                        GlobalData.assignedUser.append(userId as! String)
                    }
                }
            }
            if response?.upcoming?.count  ?? 0 > 0{
                for a in (response?.upcoming)!{
                    self.allTask.append(a)
                    let detail : DetailDataModel = a as DetailDataModel
                    let array = detail.assignId
                    for assignUserList in array! {
                        let userId = assignUserList["userId"]
                        GlobalData.assignedUser.append(userId as! String)
                    }
                }
            }
            if response?.later?.count  ?? 0 > 0{
                for a in (response?.later)!{
                    self.allTask.append(a)
                    let detail : DetailDataModel = a as DetailDataModel
                    let array = detail.assignId
                    for assignUserList in array! {
                        let userId = assignUserList["userId"]
                        GlobalData.assignedUser.append(userId as! String)
                    }
                }
            }
            if response?.completed?.count ?? 0 > 0{
                for a in (response?.completed)!{
                    self.allTask.append(a)
                    let detail : DetailDataModel = a as DetailDataModel
                    let array = detail.assignId
                    for assignUserList in array! {
                        let userId = assignUserList["userId"]
                        GlobalData.assignedUser.append(userId as! String)
                    }
                }
            }
                  
            GlobalData.allTask = Utility.applyTaskFilter(allTasks: self.allTask)
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.allTask)
                        
            //Apply Filter
            if Defaults.sortBy == 1 {
                self.filteredTask = self.filteredTask.sorted {($0.created_at)! > ($1.created_at)!}
            } else if Defaults.sortBy == 2 {
                self.filteredTask = self.filteredTask.sorted {($0.created_at)! < ($1.created_at)!}
            }
            
            if Defaults.filterBy == 2 {
                self.filteredTask = self.filteredTask.filter({ $0.in_progress == true })
            } else if Defaults.filterBy == 3 {
                self.filteredTask = self.filteredTask.filter({ $0.incomplete == true })
            }
            
            //create section
            self.sections = GroupedSection.group(rows: self.filteredTask, by: { self.firstDayOfMonth(date: self.parseDate($0.updated_at ?? "")) })
            if Defaults.sortBy == 1 {
                self.sections.sort { lhs, rhs in lhs.sectionItem > rhs.sectionItem }
            } else if Defaults.sortBy == 2 {
                self.sections.sort { lhs, rhs in lhs.sectionItem < rhs.sectionItem }
            }
            
            if self.filteredTask.isEmpty {
                self.noTaskView?.isHidden = false
                self.welcomeAnimation?.play()
            } else {
                self.noTaskView?.isHidden = true
            }
            self.itemsTableView.reloadData()
        }
    }
    
    func firstTimeLoginUser() {
        let sb = UIStoryboard(name: "NewTask", bundle: nil)
        let vc = (sb.instantiateViewController(identifier: "FirstTaskViewController")) as! FirstTaskViewController
        vc.view.backgroundColor = UIColor.clear
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func createTaskAction(_ sender: Any) {
        tabBarController?.selectedIndex = 2
    }
    
    @IBAction func calendarAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        sideMenuController?.revealMenu()
    }
    
    @IBAction func moreAction(_ sender: Any) {
        let vc = (storyboard?.instantiateViewController(identifier: "FilterDropDownViewController")) as! FilterDropDownViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.clear
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func NotificationAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: true, completion: nil)
    }
}

extension HomeView : HomeViewModelProtocol {
    func populateCategries(categoryItemArray: [Any]) {
        itemsTableView.reloadData()
    }
    
    func setupCategoryScorllDone(categoryArray: [Category]) {
        var tempArray = [Category]()
        let catArray = categoryArray.filter { category in
            if category.name == Defaults.recentCategoryTypeOne! {
                tempArray.append(category)
                return false
            }
            if category.name == Defaults.recentCategoryTypeTwo! {
                tempArray.append(category)
                return false
            }
            return true
        }
        tempArray.append(contentsOf: catArray)
        let categoryArray = tempArray
        if !categoryArray.isEmpty {
            for view in (categoryScroll?.subviews ?? []) {
                view.removeFromSuperview()
            }
            let category = Category(dictionary: [:])
            category?._id = "all_cat_id"
            category?.name = "All Category"
            category?.updatedAt = categoryArray[0].updatedAt
            category?.createdAt = categoryArray[0].createdAt
            category?.description = categoryArray[0].description
            category?.published = categoryArray[0].published
            
            let categoryAhowAll = Category(dictionary: [:])
            categoryAhowAll?._id = "show_all_cat_id"
            categoryAhowAll?.name = "+Show All"
            category?.updatedAt = categoryArray[0].updatedAt
            categoryAhowAll?.createdAt = categoryArray[0].createdAt
            categoryAhowAll?.description = categoryArray[0].description
            categoryAhowAll?.published = categoryArray[0].published
            
            var addedCategoryArray = [Category]()
            if categoryArray.count > 2 {
                for a in categoryArray{
                    if addedCategoryArray.count < 2 {
                        addedCategoryArray.append(a)
                    }
                }
                addedCategoryArray.insert(category!, at: 0)
                
                addedCategoryArray.insert(categoryAhowAll!, at: 3)
            }else{
                addedCategoryArray = categoryArray
                addedCategoryArray.insert(category!, at: 0)
            }
            
            categoryScroll = CategoryScroll(frame: CGRect(x: 0, y: 100, width: ScreenSize.SCREEN_WIDTH, height: 38))
            categoryScroll!.setupScroll(itemArray: addedCategoryArray)
            categoryScroll!.categoryScrollDelegate = self
            self.view.addSubview(categoryScroll!)
        }
    }
}

extension HomeView : CategoryScrollProtocol {
    func categoryChoosed(selectedItemName: String) {
        selCateogry = selectedItemName
        if selCateogry == "All Category" {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.allTask)
        } else if selCateogry == "+Show All"{
            let sb = UIStoryboard(name: "NewTask", bundle: nil)
            let vc = (sb.instantiateViewController(identifier: "AllCategoriesViewController")) as! AllCategoriesViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.homeViewObject = self
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.present(vc, animated: false, completion: nil)
        } else {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.allTask).filter({ myTask in
                let categoryArr = myTask.category
                return categoryArr!.contains(where: { cat in
                    cat.name == self.selCateogry
                })
            })
        }
        
        
        //Apply Filter
        if Defaults.sortBy == 1 {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.filteredTask).sorted {($0.created_at)! > ($1.created_at)!}
        } else if Defaults.sortBy == 2 {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.filteredTask).sorted {($0.created_at)! < ($1.created_at)!}
        }
        if Defaults.filterBy == 2 {
            self.filteredTask = Utility.applyTaskFilter(allTasks:  self.filteredTask).filter({ $0.in_progress == true })
        } else if Defaults.filterBy == 3 {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.filteredTask).filter({ $0.incomplete == true })
        }
        
        //create section
        self.sections = GroupedSection.group(rows: self.filteredTask, by: { self.firstDayOfMonth(date: self.parseDate($0.updated_at ?? "")) })
        if Defaults.sortBy == 1 {
            self.sections.sort { lhs, rhs in lhs.sectionItem > rhs.sectionItem }
        } else if Defaults.sortBy == 2 {
            self.sections.sort { lhs, rhs in lhs.sectionItem < rhs.sectionItem }
        }
        
        if self.filteredTask.isEmpty {
            self.noTaskView?.isHidden = false
            self.welcomeAnimation?.play()
        } else {
            self.noTaskView?.isHidden = true
        }
        self.itemsTableView.reloadData()
    }
}

extension HomeView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.itemsTableView.frame.width, height: 30))
        headerView.backgroundColor = .clear
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(headerTapped(_:))
        )
        headerView.tag = section
        headerView.addGestureRecognizer(tapGestureRecognizer)
        let label = UILabel(frame: headerView.bounds)
        label.textColor = .lightGray
        label.textAlignment = .left
        label.font = UIFont(name: "Helvetica Neue", size: 12)
        let section = self.sections[section]
        let date = section.sectionItem
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        label.text = " \(section.rows.count) Task \(dateFormatter.string(from: date))"
        headerView.addSubview(label)
        return headerView
    }
    
    @objc func headerTapped(_ sender: UITapGestureRecognizer?) {
        guard let section = sender?.view?.tag else { return }
//        if selectedSection == section {
//            self.selectedSection = -1
//        } else {
//            self.selectedSection = section
//        }
        self.itemsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sec = self.sections[section]
        //if selectedSection == section {
            return sec.rows.count
        //}
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sect = self.sections[indexPath.section]
        if Defaults.isListView == false{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaskViewTableCell", for: indexPath) as! TaskViewTableCell
            let data = sect.rows[indexPath.row]
            cell.image_stack_bg.isHidden = true

//            if selectedSection == indexPath.section {
//                cell.image_stack_bg.isHidden = true
//            }else{
//                if sect.rows.count > 1 {
//                    cell.image_stack_bg.isHidden = false
//                }else{
//                    cell.image_stack_bg.isHidden = true
//                }
//            }
            if data.getting_started == true {
                cell.lblStatus.textColor = #colorLiteral(red: 0.9995340705, green: 0.988355577, blue: 0.4726552367, alpha: 1)
                cell.lblStatus.text = "Getting Started"
            } else if data.in_progress == true {
                cell.lblStatus.textColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
                cell.lblStatus.text = "In Progress"
            } else if data.incomplete == true {
                cell.lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
                cell.lblStatus.text = "Completed"
            } else if data.wont_abble_to_perform == true {
                cell.lblStatus.textColor = #colorLiteral(red: 1, green: 0.4932718873, blue: 0.4739984274, alpha: 1)
                cell.lblStatus.text = "Won’t be able to perform"
            } else if data.getting_started == false || data.in_progress == false || data.incomplete == false || data.wont_abble_to_perform == false {
                cell.lblStatus.textColor = #colorLiteral(red: 0.9995340705, green: 0.988355577, blue: 0.4726552367, alpha: 1)
                cell.lblStatus.text = "Getting Started"
            }
            cell.lblTitle.text = data.name
            let isoDate = data.updated_at ?? ""
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSZ"
            let date = dateFormatter.date(from: isoDate)
            dateFormatter.dateFormat = "dd MMM,yy   |   hh:mm a"
            if date != nil {
                let mydate = dateFormatter.string(from: date!)
                cell.lblDate.text = "\(mydate)"
            }
            let path = data.backgroundImages?.first?.path
            if path != ""{
                let fullPathArr = path?.components(separatedBy: "public")
                cell.imageBg.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "no_image"), options: .none) { result in
                }
            }
            if data.isFavorite ?? false {
                cell.btnStar.isHidden = false
            } else {
                cell.btnStar.isHidden = true
            }
            var configFiles = cell.getConfigFiles()
            if !data.assignId!.isEmpty {
                for item in data.assignId! {
                    let user = User()
                    user.userId = item["id"] as? String ?? ""
                    user.userName = item["type"] as? String ?? ""
                    user.assigneeStatus = item["assigneeStatus"] as? String ?? ""
                    configFiles.append(BPCellConfigFile(
                        imageType: BPImageType.image(UIImage(named: "one-1")!), title: ""))
                }
            }
            let layoutConfigurator = BPLayoutConfigurator(
                colorForBubbleBorders: UIColor.clear,
                colorForBubbleTitles: UIColor.clear,
                maxCharactersForBubbleTitles: 0,
                maxNumberOfBubbles: 10,
                displayForTruncatedCell: BPTruncatedCellDisplay.image(BPImageType.image(UIImage(named: "one-1")!)),
                direction: .leftToRight,
                alignment: .right)
            cell.bubblePictures = BubblePictures(collectionView: cell.bubbleCollection, configFiles: configFiles, layoutConfigurator: layoutConfigurator)
            cell.bubblePictures.delegate = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListViewTableCell", for: indexPath) as! ListViewTableCell
            let data = sect.rows[indexPath.row]
            if data.getting_started == true {
                cell.lblStatus.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                cell.lblStatus.text = "Getting Started"
            } else if data.in_progress == true {
                cell.lblStatus.textColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
                cell.lblStatus.text = "In Progress"
            } else if data.incomplete == true {
                cell.lblStatus.textColor = #colorLiteral(red: 0.1978324652, green: 0.6840401292, blue: 0.07083017379, alpha: 1)
                cell.lblStatus.text = "Completed"
            } else if data.wont_abble_to_perform == true {
                cell.lblStatus.text = "Won’t be able to perform"
            } else if data.getting_started == false || data.in_progress == false || data.incomplete == false || data.wont_abble_to_perform == false {
                cell.lblStatus.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                cell.lblStatus.text = "Getting Started"
            }
            cell.itemTitleLabel.text = data.name
            if data.subtasks?.count ?? 0 > 0{
                cell.badge.isHidden = false
                cell.itemSubTitleLabel.text = "\(data.subtasks?.count ?? 0) Subtasks"
                cell.badge.text = "\(data.subtasks?.count ?? 0)"
            } else {
                cell.badge.isHidden = true
                cell.itemSubTitleLabel.text = "No Subtasks"
            }
            let path = data.backgroundImages?.first?.path
            if path != ""{
                let fullPathArr = path?.components(separatedBy: "public")
                cell.itemImageView.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "no_image"), options: .none) { result in
                }
            }
            if data.isFavorite ?? false {
                cell.btnStar.isHidden = false
            } else {
                cell.btnStar.isHidden = true
            }
            var configFiles = cell.getConfigFiles()
            if !data.assignId!.isEmpty {
                for item in data.assignId! {
                    let user = User()
                    user.userId = item["id"] as? String ?? ""
                    user.userName = item["type"] as? String ?? ""
                    user.assigneeStatus = item["assigneeStatus"] as? String ?? ""
                    configFiles.append(BPCellConfigFile(
                        imageType: BPImageType.image(UIImage(named: "one-1")!), title: ""))
                }
            }
            let layoutConfigurator = BPLayoutConfigurator(
                colorForBubbleBorders: UIColor.gray,
                colorForBubbleTitles: UIColor.clear,
                maxCharactersForBubbleTitles: 0,
                maxNumberOfBubbles: 10,
                displayForTruncatedCell: BPTruncatedCellDisplay.number(6),
                direction: .leftToRight,
                alignment: .left)
            cell.bubblePictures = BubblePictures(collectionView: cell.bubbleCollection, configFiles: configFiles, layoutConfigurator: layoutConfigurator)
            cell.bubblePictures.delegate = self
            if self.traitCollection.userInterfaceStyle == .dark {
                cell.customListView.backgroundColor = UIColor.init(red: 33/255, green: 33/255, blue: 33/255, alpha: 1)
                cell.itemTitleLabel.textColor = .white
                cell.itemSubTitleLabel.textColor = .white
            } else {
                cell.customListView.backgroundColor = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
                cell.itemTitleLabel.textColor = UIColor.init(red: 30/255, green: 30/255, blue: 30/255, alpha: 1)
                cell.itemSubTitleLabel.textColor = UIColor.init(red: 30/255, green: 30/255, blue: 30/255, alpha: 1)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Defaults.isListView == false{
            return 160
        }
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sect = self.sections[indexPath.section]
        let vc = storyboard?.instantiateViewController(withIdentifier: "HomeDetailViewController") as! HomeDetailViewController
        vc.detailData = sect.rows[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let animation = AnimationFactory.makeMoveUpWithFade(rowHeight: cell.frame.height, duration: 0.4, delayFactor: 0.05)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
}

class ListViewTableCell: UITableViewCell {
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet var badge: UILabel!
    @IBOutlet weak var itemSubTitleLabel: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet var btnStar: UIButton!
    @IBOutlet var viewUser: UIView!
    @IBOutlet var bubbleCollection: UICollectionView!
    @IBOutlet weak var customListView: UIView!
    var bubblePictures: BubblePictures!
    func getConfigFiles() -> [BPCellConfigFile] {
        return []
    }
}

extension HomeView: BPDelegate {
    func didSelectTruncatedBubble() {
        print("Selected truncated bubble")
    }
    
    func didSelectBubble(at index: Int) {
        print(index)
    }
}

