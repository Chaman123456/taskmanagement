//
//  DeleteTaskViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 09/02/22.
//

import UIKit

enum SideMenuType : String {
    case share, delete, both
}

class DeleteTaskViewController: UIViewController {
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var btnDeleteTask: UIButton!
    var leaveActionCompletion: (()->Void)?
    var sideMenuType : SideMenuType = SideMenuType.both
    var deleteTitle: String = "    Delete"
    var isFromSubtask = false

    override func viewDidLoad() {
        super.viewDidLoad()
        btnDeleteTask.setTitle(deleteTitle, for: .normal)
        if sideMenuType == .share {
            btnShare.isHidden = false
            btnDeleteTask.isHidden = true
        }
        if sideMenuType == .delete {
            btnDeleteTask.isHidden = false
            btnShare.isHidden = true
        }
        if sideMenuType == .both {
            btnDeleteTask.isHidden = false
            btnShare.isHidden = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            bottomView.backgroundColor = .black
            btnShare.setTitleColor(.white, for: .normal)
            btnDeleteTask.setTitleColor(.white, for: .normal)
            btnDeleteTask.tintColor = .white
        } else {
            bottomView.backgroundColor = UIColor.white
            btnShare.setTitleColor(.black, for: .normal)
            btnDeleteTask.setTitleColor(.black, for: .normal)
            btnDeleteTask.tintColor = .black
        }
    }
    
    @IBAction func shareAction() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShareTask"), object: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        if isFromSubtask {
            self.dismiss(animated: false, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SubTaskDeleted"), object: nil)
        } else {
            self.dismiss(animated: false, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TaskDeleted"), object: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        let touch = touches.first
        if touch?.view != self.bottomView
        { self.dismiss(animated: false, completion: nil) }
    }
}
