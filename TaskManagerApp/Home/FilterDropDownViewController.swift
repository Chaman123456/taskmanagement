//
//  FilterDropDownViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 17/12/21.
//

import UIKit

class FilterDropDownViewController: UIViewController {
    @IBOutlet var listView: UIView!
    @IBOutlet var listViewheight: NSLayoutConstraint!
    @IBOutlet var listViewWidth: NSLayoutConstraint!
    @IBOutlet var newestCheck: UIButton!
    @IBOutlet var oldestCheck: UIButton!
    @IBOutlet var allTaskCheck: UIButton!
    @IBOutlet var inProgressCheck: UIButton!
    @IBOutlet var completedCheck: UIButton!
    @IBOutlet var imageCheck: UIButton!
    @IBOutlet var listCheck: UIButton!
    @IBOutlet weak var assignedToCheck: UIButton!
    @IBOutlet weak var assignedByCheck: UIButton!
    @IBOutlet var lblShortBy: UILabel!
    @IBOutlet var btnNewest: UIButton!
    @IBOutlet var btnOldest: UIButton!
    @IBOutlet var btnAllTask: UIButton!
    @IBOutlet var btnInProgress: UIButton!
    @IBOutlet var btnCompleted: UIButton!
    @IBOutlet var btnFriends: UIButton!
    @IBOutlet var btnLocation: UIButton!
    @IBOutlet var btnImageView: UIButton!
    @IBOutlet var btnList: UIButton!
    @IBOutlet var lblFilterBy: UILabel!
    @IBOutlet var lblViewBy: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appearView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tap)
        setFilterBy()
        setSortBy()
        selectList()
        setMode()
    }
    
    func setMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            listView.backgroundColor = UIColor.black
            lblShortBy.textColor = .white
            btnNewest.setTitleColor(UIColor.white, for: .normal)
            btnOldest.setTitleColor(UIColor.white, for: .normal)
            btnAllTask.setTitleColor(UIColor.white, for: .normal)
            btnInProgress.setTitleColor(UIColor.white, for: .normal)
            btnCompleted.setTitleColor(UIColor.white, for: .normal)
            btnFriends.setTitleColor(UIColor.white, for: .normal)
            btnLocation.setTitleColor(UIColor.white, for: .normal)
            btnImageView.setTitleColor(UIColor.white, for: .normal)
            
            btnList.setTitleColor(UIColor.white, for: .normal)
            lblFilterBy.textColor = .white
            lblViewBy.textColor = .white
        } else {
            listView.backgroundColor = UIColor.white
            lblShortBy.textColor = .black
            btnNewest.setTitleColor(UIColor.black, for: .normal)
            btnOldest.setTitleColor(UIColor.black, for: .normal)
            btnAllTask.setTitleColor(UIColor.black, for: .normal)
            btnInProgress.setTitleColor(UIColor.black, for: .normal)
            btnCompleted.setTitleColor(UIColor.black, for: .normal)
            btnFriends.setTitleColor(UIColor.black, for: .normal)
            btnLocation.setTitleColor(UIColor.black, for: .normal)
            btnImageView.setTitleColor(UIColor.black, for: .normal)
            btnList.setTitleColor(UIColor.black, for: .normal)
            lblFilterBy.textColor = .black
            lblViewBy.textColor = .black
        }
    }
 
    
    @IBAction func sortByAction(_ sender: UIButton) {
        Defaults.sortBy = sender.tag
        setSortBy()
        NotificationCenter.default.post(name: Notification.Name("FilterData"), object: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func filterByAction(_ sender: UIButton) {
        Defaults.filterBy = sender.tag
        setFilterBy()
        NotificationCenter.default.post(name: Notification.Name("FilterData"), object: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    func setSortBy(){
        if Defaults.sortBy == 1 {
            newestCheck.isHidden = false
            oldestCheck.isHidden = true
        }else if Defaults.sortBy == 2 {
            newestCheck.isHidden = true
            oldestCheck.isHidden = false
        }else{
            newestCheck.isHidden = true
            oldestCheck.isHidden = true
        }
    }
    
    @IBAction func changeViewAction(_ sender: UIButton) {
        if sender.tag == 7{
            Defaults.isListView = false
            listCheck.isHidden = false
            imageCheck.isHidden = true
        }else if sender.tag == 8{
            Defaults.isListView = true
            listCheck.isHidden = false
            imageCheck.isHidden = true
        }
        NotificationCenter.default.post(name: Notification.Name("UpdateTableUIView"), object: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    func setFilterBy(){
        if Defaults.filterBy == 1 {
            allTaskCheck.isHidden = false
            inProgressCheck.isHidden = true
            completedCheck.isHidden = true
            assignedToCheck.isHidden = true
            assignedByCheck.isHidden = true
        }else if Defaults.filterBy == 2 {
            allTaskCheck.isHidden = true
            inProgressCheck.isHidden = false
            completedCheck.isHidden = true
            assignedToCheck.isHidden = true
            assignedByCheck.isHidden = true
        }else if Defaults.filterBy == 3 {
            allTaskCheck.isHidden = true
            inProgressCheck.isHidden = true
            completedCheck.isHidden = false
            assignedToCheck.isHidden = true
            assignedByCheck.isHidden = true
        } 
    }
    
    @IBAction func viewByAction(_ sender: UIButton) {
        var selData: Int?
        if sender.tag == 1 {
            selData = 0
        }else{
            selData = 1
        }
        NotificationCenter.default.post(name: Notification.Name("ViewedBy"), object: selData)
        self.dismiss(animated: false, completion: nil)
    }
    
    func selectList() {
        if Defaults.isListView == true {
            listCheck.isHidden = false
            imageCheck.isHidden = true
        }else{
            listCheck.isHidden = true
            imageCheck.isHidden = false
        }
    }
    
    func appearView() {
        UIView.animate(withDuration: 0.3,
                       delay: 0.2,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.listViewheight.constant = 530
                        self.listViewWidth.constant = 200
                        self.view.layoutIfNeeded()
                       }, completion: { (finished) -> Void in
                        // ....
                       })
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        self.dismiss(animated: false, completion: nil)
    }
}

