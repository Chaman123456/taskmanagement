//
//  TaskListViewController.swift
//  TaskManagerApp
//
//  Created by Chaman on 16/05/22.
//

import UIKit
import BubblePictures 

class TaskListViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var lblTodayTask: UILabel!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var tableViewTaskList: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var topView: UIView!
    @IBOutlet var taskView: UIView!
    var allTask: [DetailDataModel] = []
    var filteredTask: [DetailDataModel] = []
    var arrLatLong: Latlong_location?
    var userId : String?
    var myTitle: String?
    var address: String?
    var cellTitle: String?
    var isFromLocation: Bool?
    
    //MARK:- Veriable    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        setupUI()
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            topView.backgroundColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 50/255)
            taskView.backgroundColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 50/255)
            taskView.backgroundColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 50/255)
            lblTitle.textColor = .white
            lblTodayTask.textColor  = .gray
            lblNoData.textColor = .white
        } else {
            view.backgroundColor = UIColor.white
            topView.backgroundColor = UIColor.white
            taskView.backgroundColor = UIColor.white
            lblTitle.textColor = .black
            lblTodayTask.textColor = .gray
            lblNoData.textColor = .black
        }
        tableViewTaskList.reloadData()
    }
    
    
    //MARK:- Functions
    func setupUI() {
        tableViewTaskList.delegate = self
        tableViewTaskList.dataSource = self
        lblTitle.text = "Tasks with '\(myTitle ?? "")'"
        
        let data = GlobalData.allTask
        if isFromLocation == true {
            allTask = data.filter({ ($0.latlong_location?.contains(where: { $0.address == arrLatLong?.address }))!})
            self.filteredTask = allTask
        } else {
            self.allTask = data
            let mytask = self.allTask.filter({ $0.userId == userId ||  $0.assignId!.contains(where: { $0["userId"] as? String == userId }) })
            self.filteredTask = mytask
        }
        if self.filteredTask.isEmpty {
            lblNoData.isHidden = false
        } else {
            lblNoData.isHidden = true
        }
        self.tableViewTaskList.reloadData()
    }

    //MARK:- Actions
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

class TaskListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBadge: UILabel!
    @IBOutlet weak var lblSubTasks: UILabel!
    @IBOutlet weak var btnStar: UIButton!
    @IBOutlet weak var bubbleCollection: UICollectionView!
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet var taskDetailVw: UIView!
    var bubblePictures: BubblePictures!
    func getConfigFiles() -> [BPCellConfigFile] {
        return []
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}


extension TaskListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredTask.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskListTableViewCell", for: indexPath) as! TaskListTableViewCell
        let data = filteredTask[indexPath.row]
        if data.getting_started == true {
            cell.lblStatus.textColor = #colorLiteral(red: 0.1601396352, green: 0.1601396352, blue: 0.1601396352, alpha: 1)
            cell.lblStatus.text = "Getting Started"
        } else if data.in_progress == true {
            cell.lblStatus.textColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
            cell.lblStatus.text = "In Progress"
        } else if data.incomplete == true {
            cell.lblStatus.textColor = #colorLiteral(red: 0.1960784314, green: 0.6823529412, blue: 0.07058823529, alpha: 1)
            cell.lblStatus.text = "Completed"
        } else if data.wont_abble_to_perform == true {
            cell.lblStatus.text = "Won’t be able to perform"
        } else if data.getting_started == false || data.in_progress == false || data.incomplete == false || data.wont_abble_to_perform == false {
            cell.lblStatus.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.lblStatus.text = "Getting Started"
        }
        cell.lblTitle.text = data.name
        if data.subtasks?.count ?? 0 > 0{
            cell.lblBadge.isHidden = false
            cell.lblSubTasks.text = "\(data.subtasks?.count ?? 0) Subtasks"
            cell.lblBadge.text = "\(data.subtasks?.count ?? 0)"
        }else{
            cell.lblBadge.isHidden = true
            cell.lblSubTasks.text = "No Subtasks"
        }
        let path = data.backgroundImages?.first?.path
        if path != ""{
            let fullPathArr = path?.components(separatedBy: "public")
            cell.imgTitle.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "no_image"), options: .none) { result in
            }
        }
        if data.isFavorite ?? false {
            cell.btnStar.isHidden = false
        } else {
            cell.btnStar.isHidden = true
        }
        var configFiles = cell.getConfigFiles()
        if !data.assignId!.isEmpty {
            for item in data.assignId! {
                let user = User()
                user.userId = item["id"] as? String ?? ""
                user.userName = item["type"] as? String ?? ""
                user.assigneeStatus = item["assigneeStatus"] as? String ?? ""
                configFiles.append(BPCellConfigFile(
                    imageType: BPImageType.image(UIImage(named: "one-1")!), title: ""))
            }
        }
        let layoutConfigurator = BPLayoutConfigurator(
            colorForBubbleBorders: UIColor.gray,
            colorForBubbleTitles: UIColor.clear,
            maxCharactersForBubbleTitles: 0,
            maxNumberOfBubbles: 10,
            displayForTruncatedCell: BPTruncatedCellDisplay.image(BPImageType.image(UIImage(named: "one-1")!)),
            direction: .leftToRight,
            alignment: .right)
        cell.bubblePictures = BubblePictures(collectionView: cell.bubbleCollection, configFiles: configFiles, layoutConfigurator: layoutConfigurator)
        cell.bubblePictures.delegate = self
        if self.traitCollection.userInterfaceStyle == .dark {
            cell.taskDetailVw.backgroundColor = UIColor.init(red: 33/255, green: 33/255, blue: 33/255, alpha: 1)
            cell.lblTitle.textColor = .white
            cell.lblSubTasks.textColor = .gray
        } else {
            cell.taskDetailVw.backgroundColor = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
            cell.lblTitle.textColor = UIColor.init(red: 30/255, green: 30/255, blue: 30/255, alpha: 1)
            cell.lblSubTasks.textColor = .gray
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "HomeDetailViewController") as! HomeDetailViewController
        vc.detailData = self.filteredTask[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension TaskListViewController: BPDelegate {
    func didSelectTruncatedBubble() {
        print("Selected truncated bubble")
    }
    
    func didSelectBubble(at index: Int) {
        print(index)
    }
}
