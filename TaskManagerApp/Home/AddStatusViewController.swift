//
//  AddStatusViewController.swift
//  SampleUi
//
//  Created by Chaman Sharma on 10/01/22.
//

import UIKit

protocol addStatusDelegate {
    func messageData(data: Int)
}

class AddStatusViewController: UIViewController {
    @IBOutlet var gettingStarted: UIButton!
    @IBOutlet var inProgress: UIButton!
    @IBOutlet var completed: UIButton!
    @IBOutlet var wontNotBe: UIButton!
    @IBOutlet var gettingStartedButtonText: UIButton!
    @IBOutlet var inProgressButtonText: UIButton!
    @IBOutlet var completedButtonText: UIButton!
    @IBOutlet var wontNotBeButtonText: UIButton!
    @IBOutlet var bottomView: UIView?
    var AllBtnStatus = [UIButton]()
    var delegate: addStatusDelegate?
    var currentStatus:Int?
    
    enum StatusType:Int {
        case getting_started = 1
        case in_progress = 2
        case completed = 3
        case wont_abble_to_perform = 4
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tap)
        if let currentStatus = currentStatus {
            if currentStatus == StatusType.getting_started.rawValue {
                gettingStarted.setImage(UIImage(named: "radio-on-button")!, for: .normal)
            } else if currentStatus == StatusType.in_progress.rawValue {
                inProgress.setImage(UIImage(named: "radio-on-button")!, for: .normal)
            } else if currentStatus == StatusType.completed.rawValue {
                completed.setImage(UIImage(named: "radio-on-button")!, for: .normal)
            } else if currentStatus == StatusType.wont_abble_to_perform.rawValue {
                wontNotBe.setImage(UIImage(named: "radio-on-button")!, for: .normal)
            }
        }
        AllBtnStatus = [gettingStarted,inProgress,completed,wontNotBe]
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func setUpMode() {
        Timer.scheduledTimer(withTimeInterval: 0.0, repeats: false) { timer in
            if self.traitCollection.userInterfaceStyle == .dark {
                self.bottomView?.backgroundColor = UIColor(red: 30.0/255.0, green: 30.0/255.0, blue: 30.0/255.0, alpha: 1.0)
                self.gettingStartedButtonText.setTitleColor(.white, for: UIControl.State.normal)
                self.inProgressButtonText.setTitleColor(.white, for: UIControl.State.normal)
                self.completedButtonText.setTitleColor(.white, for: UIControl.State.normal)
                self.wontNotBeButtonText.setTitleColor(.white, for: UIControl.State.normal)
            } else {
                self.bottomView?.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
                self.gettingStartedButtonText.setTitleColor(.black, for: UIControl.State.normal)
                self.inProgressButtonText.setTitleColor(.black, for: UIControl.State.normal)
                self.completedButtonText.setTitleColor(.black, for: UIControl.State.normal)
                self.wontNotBeButtonText.setTitleColor(.black, for: UIControl.State.normal)
            }
        }
    }
    
    @IBAction private func buttonAction(_ sender: UIButton) {
        for button in AllBtnStatus {
                if sender.tag == button.tag{
                    button.isSelected = true;
                    button.setImage(UIImage(named: "radio-on-button")!, for: .normal)
                    self.delegate?.messageData(data: sender.tag)
                    self.presentingViewController!.dismiss(animated: true, completion: nil)
                }else{
                    button.isSelected = false;
                    button.setImage(UIImage(named: "dry-clean")!, for: .normal)
                }
            }
    }
}
