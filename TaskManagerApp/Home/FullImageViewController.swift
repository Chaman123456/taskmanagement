//
//  FullImageViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 22/07/22.
//

import UIKit
import Kingfisher

protocol FullImageViewControllerDelegate {
    func attachmentDeleted(index: Int)
}

class FullImageViewController: UIViewController {
    @IBOutlet var fullImage: UIImageView!
    var img = String()
    var taskId : String = ""
    var fileName : String = ""
    var fieldName : String = "multifiles"
    var index : Int?
    var delegate: FullImageViewControllerDelegate?
    var image: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fullImage.kf.setImage(with: URL(string: img), placeholder: UIImage(named: "no_image"), options: .none) { result in
        }
        if let _ = self.image {
            fullImage.image = self.image
        }
    }

    @IBAction func deleteAction(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "", message: DELETE_ASSETS, preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
            if self.fileName.isEmpty {
                self.view.makeToast("Attachment is deleted successfully.")
                self.delegate?.attachmentDeleted(index: self.index!)
                self.closeAction()
            } else {
                let getAllTasksOperation = GetAllTasksOperation()
                getAllTasksOperation.deleteTaskAttachment(taskId: self.taskId, fieldName: self.fieldName, fileName: self.fileName) { error, result in
                    print(result ?? "")
                    self.view.makeToast("Attachment is deleted successfully.")
                    self.delegate?.attachmentDeleted(index: self.index!)
                    self.closeAction()
                }
            }
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert .dismiss(animated: true, completion: nil)
        }))
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func closeAction() {
        self.dismiss(animated: false)
    }
}
