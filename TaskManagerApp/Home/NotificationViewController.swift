//
//  NotificationViewController.swift
//  TaskManagerApp
//
//  Created by chaman Sharma on 28/12/21.
//

import UIKit

class NotificationViewController: UIViewController {
    @IBOutlet var noDataLabel: UILabel!
    @IBOutlet var tableNotifacations: UITableView!
    var notificationData: [NotificationResult]?
    @IBOutlet var lblNotification: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        getNotifications()
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor  = .black
            lblNotification.textColor = .white
        } else {
            view.backgroundColor  = .white
            lblNotification.textColor = .black
        }
        tableNotifacations.reloadData()
    }
    
    @IBAction func beackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getNotifications() {
        let getNotifications = GetNotifications()
        getNotifications.getNotifications {str, response in
            self.notificationData = response?.result
            if (self.notificationData ?? []).isEmpty {
                self.noDataLabel.isHidden = false
            } else {
                self.noDataLabel.isHidden = true
            }
            self.tableNotifacations.reloadData()
        }
    }
}

class NotificationCell: UITableViewCell{
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    @IBOutlet var lblIMG: UILabel!
    
}

extension NotificationViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.lblTitle.text = self.notificationData?[indexPath.row].title
        cell.lblSubTitle.text = self.notificationData?[indexPath.row].time
        if self.traitCollection.userInterfaceStyle == .dark {
            cell.backgroundColor = UIColor.init(red: 33/255, green: 33/255, blue: 33/255, alpha: 1)
            cell.lblTitle.textColor = .white
        } else {
            cell.backgroundColor = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
            cell.lblTitle.textColor = .black
        }
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
