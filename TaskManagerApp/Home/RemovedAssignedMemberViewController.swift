//
//  RemovedAssignedMemberViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 14/01/22.
//

import UIKit

class RemovedAssignedMemberViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tap)
    }
    
    @IBAction func removedAssignedMemberAction(_ sender: Any) {
    }
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        self.dismiss(animated: false, completion: nil)
    }
}
