//
//  CalendarViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 11/04/22.
//

import UIKit
import Then
import SnapKit
import FSCalendar
import EventKit

class CalendarViewController: UIViewController {
    
    let dateFormatter = DateFormatter()
    let eventStore = EKEventStore()
    var titles : [String] = []
    var startDates : [Date] = []
    var endDates : [Date] = []
    
    var calendarView = FSCalendar().then {
        $0.backgroundColor = .black
        $0.appearance.selectionColor = UIColor(red: 38/255, green: 153/255, blue: 251/255, alpha: 1)
        $0.appearance.todayColor = UIColor(red: 188/255, green: 224/255, blue: 253/255, alpha: 1)
        $0.appearance.headerTitleColor = .white
        $0.appearance.titleDefaultColor = .white
        $0.appearance.titleSelectionColor = .black
        $0.allowsMultipleSelection = false
        $0.swipeToChooseGesture.isEnabled = true
        $0.scrollEnabled = true
        $0.scrollDirection = .vertical
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    
        requestAccess()
        setLayouts()
        setDateFormat()
        setDelegation()
    }
    
    func requestAccess() {
        eventStore.requestAccess(to: .event) { (granted, error) in
            if granted {
                DispatchQueue.main.async {
                    self.fetchCalenderEvent()
                }
            }
        }
    }
    func fetchCalenderEvent(){
        let calendars = eventStore.calendars(for: .event)
        for calendar in calendars {
            if calendar.title == "Calendar" {
                let oneMonthAgo = NSDate(timeIntervalSinceNow: -30*24*3600)
                let oneMonthAfter = NSDate(timeIntervalSinceNow: +30*24*3600)
                let predicate = eventStore.predicateForEvents(withStart: oneMonthAgo as Date, end: oneMonthAfter as Date, calendars: [calendar])
                let events = eventStore.events(matching: predicate)
                for event in events {
                    titles.append(event.title)
                    startDates.append(event.startDate as Date)
                    endDates.append(event.endDate as Date)
                }
            }
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension CalendarViewController {
    func setDateFormat() {
        dateFormatter.dateFormat = "yyyy-MM-dd"
    }
    func setDelegation() {
        calendarView.delegate = self
        calendarView.dataSource = self
    }
}

extension CalendarViewController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print(dateFormatter.string(from: date) + "Pro")
    }

    public func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print(dateFormatter.string(from: date) + "Pro")
    }

    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
        if let index = startDates.index(where: { dateFormatter.string(from: $0) == dateFormatter.string(from: date) }) {
            print(titles[index])
            return titles[index]
        }
        return nil
    }

    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        switch dateFormatter.string(from: date) {
        case "2022-04-25":
            return .red
        default:
            return appearance.selectionColor
        }
    }

    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        if calendar.selectedDates.count > 3 {
            return false
        } else {
            return true
        }
    }
}

extension CalendarViewController {
    func setLayouts() {
        setViewHierarchies()
        setConstraints()
    }

    func setViewHierarchies() {
        view.backgroundColor = .white
        view.addSubview(calendarView)
    }

    func setConstraints() {
        calendarView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(100)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(500)
        }
    }
}
