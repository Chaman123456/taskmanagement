//
//  LocationViewController.swift
//  TaskManagerApp
//
//  Created by Chaman on 16/05/22.
//

import UIKit

class LocationViewController: UIViewController, UITextFieldDelegate {

    //MARK:- Outlets
    @IBOutlet weak var menuButtonLocation: UIButton!
    @IBOutlet weak var txtFldSearch: UITextField! {
        didSet{
            txtFldSearch.setLeftView(image: UIImage.init(named: "search-24px")!)
        }
    }
    @IBOutlet weak var tableViewLocation: UITableView!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet var noItemView: UIView?
    @IBOutlet var topCustomView: UIView?
    @IBOutlet var secondTopCustomView: UIView?
    //MARK:- Veriable
    var arrLatLong = [Latlong_location]()
    var filterAddress = [Latlong_location]()
    var arrFiltr = [DetailDataModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        uiSet()
        print(GlobalData.allTask)
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            tableViewLocation.backgroundColor = UIColor.black
            topCustomView?.backgroundColor = UIColor.black
            secondTopCustomView?.backgroundColor = UIColor.black
            txtFldSearch.placeholderColor = UIColor.init(red: 76/255, green: 76/255, blue: 76/255, alpha: 1)
            txtFldSearch.placeholder = "Search by location name"
        } else {
            view.backgroundColor = UIColor.white
            topCustomView?.backgroundColor = UIColor.white
            secondTopCustomView?.backgroundColor = UIColor.white
            tableViewLocation.backgroundColor = UIColor.white
            tableViewLocation.backgroundColor = UIColor.white
            txtFldSearch.backgroundColor = .lightGray
            txtFldSearch.placeholderColor = .white
            txtFldSearch.textColor = .black
            txtFldSearch.placeholder = "Search by location name"
        }
        tableViewLocation.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Functions
    func uiSet() {
        
        tableViewLocation.delegate = self
        tableViewLocation.dataSource = self
        txtFldSearch.delegate = self
        UIApplication.shared.statusBarStyle = .lightContent
//        txtFldSearch.attributedPlaceholder = NSAttributedString(
//            string: "Search by location name",
//            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
//        )
        
        for a in GlobalData.allTask {
            for lat in a.latlong_location! {
                if !filterAddress.contains(where: { $0.address == lat.address }){
                    filterAddress.append(lat)
                }
            }
        }
        arrLatLong = filterAddress
        tableViewLocation.reloadData()
    }
    
    //MARK:- Actions
    @IBAction func actionMore(_ sender: Any) {
    }
    
    @IBAction func actionNotification(_ sender: Any) {
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        noItemView?.isHidden = true
        txtFldSearch.text = ""
        self.arrLatLong = self.filterAddress
        self.tableViewLocation?.reloadData()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = txtFldSearch.text as NSString? {
            let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
            arrLatLong = filterAddress.filter{ ($0.address?.localizedCaseInsensitiveContains(txtAfterUpdate)) ?? false}
            if arrLatLong.isEmpty {
                noItemView?.isHidden = false
            } else {
                noItemView?.isHidden = true
            }
            tableViewLocation.reloadData()
        }
        
        return true
    }
}

class LocationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblForCoaching: UILabel!
    
    @IBOutlet var customCellBGView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension LocationViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLatLong.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell") as! LocationTableViewCell
        cell.lblTitle.text = arrLatLong[indexPath.row].address
        let data = GlobalData.allTask
        let dataCount = data.filter({ ($0.latlong_location?.contains(where: { $0.address == arrLatLong[indexPath.row].address }))!})
        cell.lblSubTitle.text = "\(dataCount.count) task for today"
        if self.traitCollection.userInterfaceStyle == .dark {
            cell.customCellBGView.backgroundColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 1)
            cell.lblTitle.textColor = . white
            cell.lblSubTitle.textColor = .lightGray
            cell.backgroundColor = .black
        } else {
            cell.backgroundColor = .white
            cell.customCellBGView.backgroundColor = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
            cell.lblTitle.textColor = UIColor.init(red: 30/255, green: 30/255, blue: 30/255, alpha: 1)
            cell.lblSubTitle.textColor = .gray
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TaskListViewController") as! TaskListViewController
        vc.myTitle = arrLatLong[indexPath.row].address
        vc.arrLatLong = arrLatLong[indexPath.row]
        vc.isFromLocation = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

