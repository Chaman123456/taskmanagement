//
//  SearchViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 10/01/22.
//

import UIKit
import BubblePictures

class SearchViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var txtSearchField: UITextField! {
        didSet {
            txtSearchField.setLeftView(image: UIImage.init(named: "search-24px")!)
        }
    }
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblSearch: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet var itemsTableView: UITableView!
    var allTasks = [DetailDataModel]()
    @IBOutlet var noTaskLabel: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearchField.delegate = self
        textfieldCustomise()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
    }
    
    @IBAction func beckAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func textfieldCustomise() {
        txtSearchField.attributedPlaceholder = NSAttributedString(
            string: "Search",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            self.view.backgroundColor = UIColor.black
            itemsTableView.backgroundColor = UIColor.black
            btnCancel.setTitleColor(.white, for: .normal)
            lblSearch.textColor = .white
            txtSearchField.textColor = .white
        } else {
            self.view.backgroundColor = UIColor.white
            itemsTableView.backgroundColor = UIColor.white
            btnCancel.setTitleColor(.black, for: .normal)
            lblSearch.textColor = UIColor.black
            txtSearchField.textColor = .black
            txtSearchField.placeholderColor = .gray
            txtSearchField.backgroundColor = .lightGray
            
        }
        itemsTableView.reloadData()
    }
    
    
    @IBAction func cancelButtonAction(){
        txtSearchField.text = ""
        getSearch()
    }

    @objc func handleRegister(sender: UIButton){
    }
    
    func getSearch() {
        let searchDetails = [
            "userId" : Defaults.userId ?? "",
            "catId": "",
            "searchText": txtSearchField.text!
        ] as [String : Any]
        let searchOperation = SearchOperation()
        searchOperation.executeSearch(userDetail: searchDetails as [String : Any]) { str, data in
            self.allTasks.removeAll()
            if data?.recent?.count ?? 0 > 0{
                for a in (data?.recent)!{
                    self.allTasks.append(a)
                }
            }
            if data?.today?.count ?? 0 > 0{
                for a in (data?.today)!{
                    self.allTasks.append(a)
                }
            }
            if data?.upcoming?.count  ?? 0 > 0{
                for a in (data?.upcoming)!{
                    self.allTasks.append(a)
                }
            }
            if data?.later?.count  ?? 0 > 0{
                for a in (data?.later)!{
                    self.allTasks.append(a)
                }
            }
            if data?.completed?.count ?? 0 > 0{
                for a in (data?.completed)!{
                    self.allTasks.append(a)
                }
            }
            if self.allTasks.isEmpty {
                self.noTaskLabel?.isHidden = false
            } else {
                self.noTaskLabel?.isHidden = true
            }
            self.itemsTableView.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(range.location > 1) {
            getSearch()
        }else{
            allTasks.removeAll()
            self.itemsTableView.reloadData()
            self.noTaskLabel?.isHidden = false
        }
        return true
    }
}

extension UITextField {
    func setLeftView(image: UIImage) {
        let iconView = UIImageView(frame: CGRect(x: 10, y: 10, width: 25, height: 25)) // set your Own size
        iconView.image = image
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 45))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
        self.tintColor = .gray
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchListViewTableCell", for: indexPath) as! SearchListViewTableCell
        let data = allTasks[indexPath.row]
        var configFiles = cell.getConfigFiles()
        if !data.assignId!.isEmpty {
            for item in data.assignId! {
                let user = User()
                user.userId = item["id"] as? String ?? ""
                user.userName = item["type"] as? String ?? ""
                user.assigneeStatus = item["assigneeStatus"] as? String ?? ""
                configFiles.append(BPCellConfigFile(
                    imageType: BPImageType.image(UIImage(named: "one-1")!), title: ""))
            }
        }
        let layoutConfigurator = BPLayoutConfigurator(
            colorForBubbleBorders: UIColor.gray,
            colorForBubbleTitles: UIColor.clear,
            maxCharactersForBubbleTitles: 0,
            maxNumberOfBubbles: 10,
            displayForTruncatedCell: BPTruncatedCellDisplay.number(6),
            direction: .leftToRight,
            alignment: .right)
        cell.bubblePictures = BubblePictures(collectionView: cell.bubbleCollection, configFiles: configFiles, layoutConfigurator: layoutConfigurator)
        cell.bubblePictures.delegate = self
        
        let recent = allTasks[indexPath.row]
        cell.itemTitleLabel.text = recent.name
        cell.itemSubTitleLabel.text = Defaults.getFormattedDate(string: recent.assignDate ?? "", formatter: "MMM dd,yyyy")
        let path = recent.backgroundImages?.first?.path
        if path != ""{
            let fullPathArr = path?.components(separatedBy: "public")
            cell.itemImageView.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "user"), options: .none) { result in
            }
        }
        
        if recent.isFavorite ?? false {
            cell.btnStar.isHidden = false
        } else {
            cell.btnStar.isHidden = true
        }
        if self.traitCollection.userInterfaceStyle == .dark {
            cell.itemTitleLabel.textColor = .white
        } else {
            cell.itemTitleLabel.textColor = .black
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "HomeDetailViewController") as! HomeDetailViewController
        vc.detailData = allTasks[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
class SearchListViewTableCell: UITableViewCell {
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet var badge: UILabel!
    @IBOutlet var itemTitleLabel: UILabel!
    @IBOutlet weak var itemSubTitleLabel: UILabel!
    @IBOutlet var btnStar: UIButton!
    @IBOutlet var viewUser: UIView!
    @IBOutlet var bubbleCollection: UICollectionView!
    var bubblePictures: BubblePictures!
    func getConfigFiles() -> [BPCellConfigFile] {
        return [
        ]
    }
}

extension SearchViewController: BPDelegate {
    func didSelectTruncatedBubble() {
        print("Selected truncated bubble")
    }
    
    func didSelectBubble(at index: Int) {
        print(index)
    }
}
