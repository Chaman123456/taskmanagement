//
//  SubTaskViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 31/01/22.
//

import UIKit
import MobileCoreServices
import LocationPicker
import MapKit

class SubTaskViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var bottomView: UIView!
    @IBOutlet var lblSubTask: UILabel!
    @IBOutlet var imgCustomView: UIView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblDueDate: UILabel!
    @IBOutlet var lblAssign: UILabel!
    @IBOutlet var tf_name: UITextField!
    @IBOutlet var tfCategory: UITextField!
    @IBOutlet var tfDescription: UITextField!
    @IBOutlet var imgCoverImage: UIImageView!
    @IBOutlet weak var timeDatePicker: UIDatePicker!
    @IBOutlet var collectionAssign: UICollectionView!
    @IBOutlet var viewUpdateStatus: UIView!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var btnMore: UIButton!
    @IBOutlet var updateSubtaskButton: UIButton!
    @IBOutlet var collectionMap: UICollectionView!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet var titleLocation: UILabel!
    @IBOutlet var locationHeight: NSLayoutConstraint!
    @IBOutlet var btnDone: UIButton!
    let locationPicker = LocationPickerViewController()
    var arrLocation = [[String: Any]]()
    var selectedLocationIndex : Int?
    var taskId: String?
    var sub_taskId: String?
    var imagePicker: UIImagePickerController!
    var selectedDate = ""
    var arrUser = [User]()
    var isPreviousTask: Bool?
    var detailData : DetailDataModel?
    var statusType: Int?
    var selectedTask: Subtasks?
    var selIndex = -1
    var assignedUser = [User]()
    var userId : String?
    var maxDate : String?
    var sub_assignDate : String?
    @IBOutlet var btnCamera: UIButton!
    @IBOutlet var btnImage: UIButton!
    @IBOutlet var attachmentLabel: UILabel!
    @IBOutlet var collectionAttachment: UICollectionView!
    var arrAttachment = [[String: Any]]()
    @IBOutlet var titleAttachment: UILabel?
    @IBOutlet var attachmentHeight: NSLayoutConstraint?
    var isAttachmentSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.subTaskDeleted(notification:)), name: Notification.Name("SubTaskDeleted"), object: nil)
        
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.execute { str, categories in
            GlobalData.allCategories = categories ?? []
        }
        coverImageGesture()
        timeDatePicker.minimumDate = Date()
        timeDatePicker.datePickerMode = .date
        timeDatePicker.contentHorizontalAlignment = .left
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        setupInitialUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedLocationIndex = nil
        navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func locationAction() {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.tintColor = .white
        locationPicker.showCurrentLocationButton = true
        locationPicker.currentLocationButtonBackground = .black
        locationPicker.showCurrentLocationInitially = true
        locationPicker.mapType = .standard
        locationPicker.useCurrentLocationAsHint = true
        locationPicker.searchBarPlaceholder = "Search places"
        locationPicker.searchTextFieldColor = .gray
        locationPicker.searchBarStyle = .minimal
        locationPicker.searchHistoryLabel = "Search History"
        locationPicker.resultRegionDistance = 600000
        if self.traitCollection.userInterfaceStyle == .dark {
        } else {
            
        }
        if let index = selectedLocationIndex {
            let locData = self.arrLocation[index]
            if locData["placemark"] as? String != ""{
                locationPicker.location = Location(name: locData["address"] as? String, location: locData["location"] as? CLLocation, placemark: locData["placemark"] as! CLPlacemark)
                
            }
        } else {
            locationPicker.location = nil
        }
        locationPicker.completion = { [self] location in
            let locData = ["address":location!.address,
                           "location":location!.location,
                           "placemark" : location!.placemark
            ] as [String : Any]
            self.arrLocation.append(locData)
            if self.arrLocation.count > 0{
                self.locationHeight.constant = 140
                self.titleLocation.isHidden = false
            } else {
                self.locationHeight.constant = 0
                self.titleLocation.isHidden = true
            }
            collectionMap.reloadData()
        }
        self.navigationController?.pushViewController(locationPicker, animated: true)
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            bottomView.backgroundColor =  UIColor(red: 35.0/255.0, green: 35.0/255.0, blue: 35.0/255.0, alpha: 1.0)
            imgCustomView.backgroundColor = UIColor.init(red: 33/255, green: 33/255, blue: 33/255, alpha: 1)
            lblSubTask.textColor = .white
            lblName.textColor = .white
            lblDesc.textColor = .white
            lblDueDate.textColor = .white
            lblAssign.textColor = .white
            tf_name.placeholderColor = .white
            tf_name.textColor = .white
            tf_name.tintColor = .white
            
            tfCategory.placeholderColor = .white
            tfCategory.textColor = .white
            tfCategory.tintColor = .white
            
            tfDescription.placeholderColor = .white
            tfDescription.textColor = .white
            tfDescription.tintColor = .white
            
            lblStatus.textColor = .white
            timeDatePicker.overrideUserInterfaceStyle = .dark
            bottomView.backgroundColor = UIColor.init(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1)
            bottomView.borderColor = .clear
            btnLocation.setTitleColor(.white, for: .normal)
            btnCamera.setTitleColor(.white, for: .normal)
            btnImage.setTitleColor(.white, for: .normal)
            btnDone.titleLabel?.textColor = .white
            titleLocation.textColor = .white
            attachmentLabel.textColor = .white
        } else {
            titleLocation.textColor = .black
            view.backgroundColor = UIColor.white
            bottomView.backgroundColor =  UIColor(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0)
            imgCustomView.backgroundColor =  UIColor(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0)
            lblSubTask.textColor = .black
            lblName.textColor = .black
            lblDesc.textColor = .black
            lblDueDate.textColor = .black
            lblAssign.textColor = .black
            tf_name.placeholderColor = .lightGray
            tf_name.textColor = .darkGray
            tf_name.tintColor = .black
            
            tfCategory.placeholderColor = .lightGray
            tfCategory.textColor = .darkGray
            tfCategory.tintColor = .black
            
            tfDescription.placeholderColor = .lightGray
            tfDescription.textColor = .darkGray
            tfDescription.tintColor = .black
            
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            timeDatePicker.overrideUserInterfaceStyle = .light
            bottomView.backgroundColor = UIColor.init(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
            bottomView.borderColor = .gray
            btnLocation.setTitleColor(.black, for: .normal)
            btnCamera.setTitleColor(.black, for: .normal)
            btnImage.setTitleColor(.black, for: .normal)
            btnDone.titleLabel?.textColor = .white
            attachmentLabel.textColor = .black
        }
        checkDoneButton()
    }
    
    func checkDoneButton() {
        btnDone?.isEnabled = false
        btnDone?.backgroundColor = .gray
        if !(tf_name.text ?? "").isEmpty && !arrUser.isEmpty {
            btnDone?.backgroundColor = UIColor(red: 115.0/255.0, green: 146.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            btnDone?.isEnabled = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkDoneButton()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        checkDoneButton()
        return true
    }
    
    func setupInitialUI() {
        let isoDate = maxDate ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSZ"
        let date = dateFormatter.date(from: isoDate)
        if date ?? Date() > Date(){
            timeDatePicker.maximumDate = date
        }else{
            let leaveAlert = UIAlertController(title: "", message: "Due date is expired, please update due date to Create/Update Subtask.", preferredStyle: UIAlertController.Style.alert)
            leaveAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(leaveAlert, animated: true, completion: nil)
        }
        if isPreviousTask == true {
            btnMore.isHidden = false
            tf_name.text = selectedTask?.sub_name
            tfDescription.text = selectedTask?.sub_notes
            selectedDate = selectedTask?.sub_assignDate ?? ""
            let path = selectedTask?.backgroundImages?.first?.path
            if path != "" {
                let fullPathArr = path?.components(separatedBy: "public")
                imgCoverImage.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "no_image"), options: .none) { result in
                }
            }
            if !(selectedTask?.assignId?.isEmpty ?? true){
                for item in selectedTask!.assignId! {
                    let user = User()
                    user.userId = item["userId"] as? String ?? ""
                    user.userName = item["type"] as? String ?? ""
                    user.assigneeStatus = item["assigneeStatus"] as? String ?? ""
                    let resultUser = GlobalData.allUsers.filter({ checkUser in
                        return checkUser.userId == user.userId
                    })
                    if !resultUser.isEmpty {
                        user.phoneNumber = resultUser[0].phoneNumber
                        user.userName = resultUser[0].userName
                    }
                    arrUser.append(user)
                }
                collectionAssign.reloadData()
                Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { timer in
                    self.setScrollOfAssignedUserCollectionView()
                }
            }
            
            viewUpdateStatus.isHidden = false
            assignedUser = arrUser.filter { $0.userId == Defaults.userId}
            
            if Defaults.userId != userId {
                btnMore.isHidden = true
                timeDatePicker.isUserInteractionEnabled = false
                tf_name.isUserInteractionEnabled = false
                if assignedUser.count > 0 {
                    tfDescription.isUserInteractionEnabled = true
                    updateSubtaskButton.isHidden = false
                    imgCoverImage.isUserInteractionEnabled = true
                } else {
                    tfDescription.isUserInteractionEnabled = false
                    updateSubtaskButton.isHidden = true
                    imgCoverImage.isUserInteractionEnabled = false
                }
            }
            
            for imageData in selectedTask!.multifilesImages ?? [] {
                arrAttachment.append(["type": 1, "Info": imageData.path!, "fileName" : imageData.filename!])
            }
            
            if arrAttachment.count > 0 {
                titleAttachment?.isHidden = false
                attachmentHeight?.constant = 140
            } else {
                titleAttachment?.isHidden = true
                attachmentHeight?.constant = 0
            }
            collectionAttachment.reloadData()
            
            if selectedTask!.sub_latlong_location != nil {
                for location in selectedTask!.sub_latlong_location! {
                    let clLocation =  CLLocation(latitude: Double(location.latitude!)!, longitude: Double(location.logitude!)!)
                    let locData = ["address":location.address ?? "",
                                   "location":clLocation,
                                   "placemark" : ""
                    ] as [String : Any]
                    self.arrLocation.append(locData)
                }
                if self.arrLocation.count > 0 {
                    self.locationHeight.constant = 140
                    self.titleLocation.isHidden = false
                }else{
                    self.locationHeight.constant = 0
                    self.titleLocation.isHidden = true
                }
                collectionMap.reloadData()
            }
            
            let sub_due_date = dateFormatter.date(from: sub_assignDate ?? "") ?? Date()
            timeDatePicker.date = sub_due_date
            
        } else {
            titleAttachment?.isHidden = true
            attachmentHeight?.constant = 0
            titleLocation?.isHidden = true
            locationHeight?.constant = 0
            btnMore.isHidden = true
            viewUpdateStatus.isHidden = true
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSZ"
            selectedDate = dateFormatter.string(from: Date())
        }
        if selectedTask?.sub_getting_started == true{
            statusType = 1
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            lblStatus.text = "Getting Started"
        } else if selectedTask?.sub_in_progress == true {
            statusType = 2
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            lblStatus.text = "In Progress"
        } else if selectedTask?.sub_incomplete == true {
            statusType = 3
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            lblStatus.text = "Completed"
        } else if selectedTask?.sub_wont_abble_to_perform == true {
            statusType = 4
            lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
            lblStatus.text = "Won't be able to perform"
        } else {
            statusType = 1
            lblStatus.text = "Getting Started"
        }
        checkDoneButton()
    }
    
    func setScrollOfAssignedUserCollectionView() {
        if Defaults.userId != detailData?.userId {
            self.collectionAssign.scrollToItem(at: IndexPath(item: self.arrUser.count - 1, section: 0), at: UICollectionView.ScrollPosition.right, animated: false)
        } else {
            self.collectionAssign.scrollToItem(at: IndexPath(item: self.arrUser.count, section: 0), at: UICollectionView.ScrollPosition.right, animated: false)
        }
    }
    
    func coverImageGesture() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgCoverImage.isUserInteractionEnabled = true
        imgCoverImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func actionsheet() {
        let alert  = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (handler) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (handler) in
            self.openGalary()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (handler) in
            self.isAttachmentSelected = false
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let image = UIImagePickerController()
            image.allowsEditing = true
            image.delegate = self
            image.sourceType = .camera
            image.mediaTypes = [kUTTypeImage as String]
            self.present(image, animated: true, completion: nil)
        }
    }
    
    func openGalary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let image = UIImagePickerController()
            image.allowsEditing = true
            image.delegate = self
            self.present(image, animated: true, completion: nil)
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        isAttachmentSelected = false
        actionsheet()
    }
    
    @objc func subTaskDeleted(notification: Notification) {
        let alertController = UIAlertController(title: title, message: "Would you like to delete this Subtask?", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "No", style: .default) { action in
        }
        let yesAction = UIAlertAction(title: "Yes", style: .default) { action in
            let task = Task()
            task.name = self.tf_name.text ?? ""
            task.description = self.tfDescription.text ?? ""
            task.assignedDate = self.selectedDate
            task.image = self.imgCoverImage.image ?? UIImage(named: "one-1")!
            task.arrUser = self.arrUser
            task.selIndex = self.selIndex
            self.imgCoverImage.backgroundColor = .white
            let createSubTaskOperation = CreateTaskOperation(withTask: task, image: (self.imgCoverImage.image ?? UIImage(named: "one-1"))!, arrimage: [])
            createSubTaskOperation.deleteSubTask(subtaskId: self.sub_taskId ?? "", outerClosure: { str, response in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SubTaskAdded"), object: task)
                self.dismiss(animated: true, completion: nil)
            })
        }
        alertController.addAction(okAction)
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func moreAction(_ sender: Any) {
        let vc = (storyboard?.instantiateViewController(identifier: "DeleteTaskViewController")) as! DeleteTaskViewController
        vc.sideMenuType = SideMenuType.delete
        vc.deleteTitle = "Delete SubTask"
        vc.isFromSubtask = true
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func taskStatusAction(_ sender: Any) {
        if Defaults.userId != userId {
            if assignedUser.count < 1 {
                self.presentAlert(withTitle: "", message: NOT_AUTHORISED)
                return
            }
        }
        let vc = (storyboard?.instantiateViewController(identifier: "AddStatusViewController")) as! AddStatusViewController
        vc.currentStatus = statusType
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.clear
        vc.delegate = self
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func selectDate(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSZ"
        selectedDate = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func addSubTask(_ sender: Any) {
        let task = Task()
        task.name = tf_name.text ?? ""
        task.description = tfDescription.text ?? ""
        task.assignedDate = selectedDate
        task.image = imgCoverImage.image ?? nil
        task.arrUser = arrUser
        task.selIndex = selIndex
        var assignedUsers = [[String : Any]]()
        for user in arrUser {
            var dict = [String : Any]()
            if user.userId == nil {
                dict = ["userId" : "", "type": user.userName ?? "","assigneeStatus": AssigneeStatus.invited.rawValue, "phoneNumber": user.phoneNumber ?? ""]
            }else{
                dict = ["userId" : user.userId ?? "", "type": user.userName ?? "","assigneeStatus": AssigneeStatus.pending.rawValue, "phoneNumber": ""]
            }
            assignedUsers.append(dict as [String : Any])
        }
        var locations = [[String : Any]]()
        for location in arrLocation {
            let latLong = location["location"] as! CLLocation
            let dict = ["latitude" : latLong.coordinate.latitude, "logitude": latLong.coordinate.longitude, "address": location["address"] ?? ""]
            locations.append(dict as [String : Any])
        }
        
        task.location = locations
        task.assignedUser = assignedUsers
        
        if task.name.isEmpty {
            self.presentAlert(withTitle: "", message: PLEASE_ENTER_THE_TASK_NAME)
            return
        }
        if assignedUsers.isEmpty {
            self.presentAlert(withTitle: "", message: PLEASE_ASSIGN_USER)
            return
        }
        imgCoverImage.backgroundColor = .white
        
        if isPreviousTask == true {
            let attachmentArray : [[String : Any]] = arrAttachment.filter { data in
                return (data["type"] as? Int) == 0
            }
            let createSubTaskOperation = CreateTaskOperation(withTask: task, image: imgCoverImage.image, arrimage: attachmentArray)
            createSubTaskOperation.updateSubTask(taskId: sub_taskId ?? "", outerClosure: { str, response in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SubTaskAdded"), object: task)
                self.dismiss(animated: true, completion: nil)
            })
        } else {
            let createSubTaskOperation = CreateTaskOperation(withTask: task, image: imgCoverImage.image, arrimage: arrAttachment)
            createSubTaskOperation.executeSubTask(taskId: taskId ?? "", outerClosure: { str, response in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SubTaskAdded"), object: task)
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
    
    func callSubTaskStatusApi(sub_taskId: String, sub_getting_started:Int, sub_in_progress:Int, sub_incomplete:Int, sub_wont_abble_to_perform:Int) {
        let getSubStatusTaskOperation = GetSubStatusTaskOperation()
        let params = [
            "userId" : Defaults.userId ?? "",
            "sub_taskId": sub_taskId,
            "sub_getting_started": sub_getting_started,
            "sub_in_progress": sub_in_progress,
            "sub_incomplete": sub_incomplete,
            "sub_wont_abble_to_perform": sub_wont_abble_to_perform
        ] as [String : Any]
        getSubStatusTaskOperation.getSubTaskStatus(parameters: params) { error, taskStatus in
            if sub_getting_started == 1{
                self.lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
                self.lblStatus.text = "Getting Started"
            } else if sub_in_progress == 1 {
                self.lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
                self.lblStatus.text = "In Progress"
            } else if sub_incomplete == 1 {
                self.lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
                self.lblStatus.text = "Completed"
            }  else if sub_wont_abble_to_perform == 1  {
                self.lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
                self.lblStatus.text = "Won’t be able to perform"
            } else {
                self.lblStatus.text = "Getting Started"
            }
            self.presentAlert(withTitle: "", message: "Status updated successfully.")
        }
    }
    
    @IBAction func takeImage(sender: AnyObject) {
        isAttachmentSelected = true
        openCamera()
    }
    
    @IBAction func galaryAction(_ sender: Any) {
        isAttachmentSelected = true
        openGalary()
    }
}

class SubTaskLocationCell: UICollectionViewCell {
    @IBOutlet weak var lblLocationName: UILabel!
}

extension SubTaskViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == collectionAssign) {
            if Defaults.userId != userId {
                if assignedUser.count < 1 {
                    return arrUser.count
                }
            }
            return arrUser.count + 1
        } else if collectionView == collectionMap {
            return arrLocation.count
        } else if collectionView == collectionAttachment {
            return arrAttachment.count
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == collectionAssign) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssignCollectionCell", for: indexPath) as! AssignCollectionCell
            cell.imageUser.layer.cornerRadius = cell.imageUser.frame.height/2
            if indexPath.row < arrUser.count {
                cell.imageUser.backgroundColor = .white
                cell.imageUser.image = UIImage(named: "one-1")
                cell.lblAssignedName.isHidden = false
                cell.lblAssignedName.text = (arrUser[indexPath.row].userName ?? "")
                if self.traitCollection.userInterfaceStyle == .dark {
                    cell.lblAssignedName.textColor = .white
                } else {
                    cell.lblAssignedName.textColor = .black
                }
            } else {
                cell.imageUser.backgroundColor = .clear
                cell.lblAssignedName.text = ""
                cell.imageUser.image = UIImage(named: "add-friend")
            }
            return cell
        }  else if collectionView == collectionAttachment {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCollectionCell", for: indexPath) as! AttachmentCollectionCell
            if arrAttachment[indexPath.row]["type"] as? Int == 0 {
                cell.itemImageView.image = arrAttachment[indexPath.row]["Info"] as? UIImage
            } else {
                let path = arrAttachment[indexPath.row]["Info"] as! String
                let fullPathArr = path.components(separatedBy: "public")
                cell.itemImageView.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr[1])"), placeholder: UIImage(named: "no_image"), options: .none) { result in
                }
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubTaskLocationCell", for: indexPath) as! SubTaskLocationCell
            let name = arrLocation[indexPath.row]
            let randomValue = name["address"]!
            cell.lblLocationName.text = randomValue as? String
            if self.traitCollection.userInterfaceStyle == .dark {
                cell.lblLocationName.textColor = .white
                cell.lblLocationName.backgroundColor = .black
            } else {
                cell.lblLocationName.backgroundColor = .white
                cell.lblLocationName.textColor = .black
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionAssign {
            if indexPath.row >= arrUser.count {
                let sb = UIStoryboard(name: "NewTask", bundle: nil)
                let vc = (sb.instantiateViewController(identifier: "UserListViewController")) as! UserListViewController
                vc.delegate = self
                self.present(vc, animated: true, completion: nil)
            } else {
                if Defaults.userId != userId {
                    let leaveAlert = UIAlertController(title: "", message: "Name: \(arrUser[indexPath.row].userName ?? "")\nPhone: \(arrUser[indexPath.row].phoneNumber ?? "")", preferredStyle: UIAlertController.Style.alert)
                    leaveAlert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action: UIAlertAction!) in
                    }))
                    self.present(leaveAlert, animated: true, completion: nil)
                } else {
                    let leaveAlert = UIAlertController(title: "", message: "Name: \(arrUser[indexPath.row].userName ?? "")\nPhone: \(arrUser[indexPath.row].phoneNumber ?? "")", preferredStyle: UIAlertController.Style.alert)
                    leaveAlert.addAction(UIAlertAction(title: "Remove", style: .destructive, handler: { (action: UIAlertAction!) in
                        self.arrUser.remove(at: indexPath.row)
                        self.collectionAssign.reloadData()
                    }))
                    leaveAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                    }))
                    self.present(leaveAlert, animated: true, completion: nil)
                }
            }
        } else if collectionView == collectionMap {
            selectedLocationIndex = indexPath.row
            locationAction()
        } else if collectionView == collectionAttachment {
            let sb = UIStoryboard(name: "Home", bundle: nil)
            let vc = (sb.instantiateViewController(identifier: "FullImageViewController")) as! FullImageViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.9)
            if arrAttachment[indexPath.row]["type"] as? Int == 0 {
                vc.image = arrAttachment[indexPath.row]["Info"] as? UIImage
            } else {
                let path = arrAttachment[indexPath.row]["Info"] as! String
                let fullPathArr = path.components(separatedBy: "public")
                vc.img = "\(BaseURL)\(fullPathArr[1])"
            }
            vc.taskId = self.detailData?._id ?? ""
            vc.fileName = arrAttachment[indexPath.row]["fileName"] as? String ?? ""
            vc.delegate = self
            vc.index = indexPath.row
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionAssign {
            return CGSize(width: 60, height: 70)
        } else if collectionView == collectionMap {
            return CGSize(width: 120, height: 120)
        } else {
            return CGSize(width: collectionView.frame.width/1.5, height: 140)
        }
    }
}

extension SubTaskViewController: assignedUserProtocol{
    func userAssigned(user: User) {
        if arrUser.count >= 4 {
            let alert = UIAlertController(title: "", message: "Assign user limit exceeded.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        if arrUser.contains(where: { $0.userId == user.userId }) {
            DispatchQueue.main.async {
                let leaveAlert = UIAlertController(title: "", message: USER_ALREADR_ADDED, preferredStyle: UIAlertController.Style.alert)
                leaveAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                }))
                self.present(leaveAlert, animated: true, completion: nil)
            }
            return
        }
        arrUser.append(user)
        collectionAssign.reloadData()
        checkDoneButton()
    }
}

extension SubTaskViewController: addStatusDelegate {
    func messageData(data: Int) {
        statusType = data
        if data == 1 {
            self.callSubTaskStatusApi(sub_taskId: sub_taskId ?? "", sub_getting_started: data, sub_in_progress: 0, sub_incomplete: 0, sub_wont_abble_to_perform: 0)
        } else if data == 2 {
            self.callSubTaskStatusApi(sub_taskId: sub_taskId ?? "", sub_getting_started: 0, sub_in_progress: 1, sub_incomplete: 0, sub_wont_abble_to_perform: 0)
        } else if data == 3 {
            self.callSubTaskStatusApi(sub_taskId: sub_taskId ?? "", sub_getting_started: 0, sub_in_progress: 0, sub_incomplete: 1, sub_wont_abble_to_perform: 0)
        } else if data == 4 {
            self.callSubTaskStatusApi(sub_taskId: sub_taskId ?? "", sub_getting_started: 0, sub_in_progress: 0, sub_incomplete: 0, sub_wont_abble_to_perform: 1)
        }
    }
}

extension SubTaskViewController: FullImageViewControllerDelegate {
    func attachmentDeleted(index: Int) {
        arrAttachment.remove(at: index)
        collectionAttachment.reloadData()
        checkDoneButton()
    }
}

extension SubTaskViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        if isAttachmentSelected == true {
            let arr = ["type": 0,
                       "Info": tempImage] as [String : Any]
            arrAttachment.append(arr)
            if arrAttachment.count > 0 {
                titleAttachment?.isHidden = false
                attachmentHeight?.constant = 140
            } else {
                titleAttachment?.isHidden = true
                attachmentHeight?.constant = 0
            }
            collectionAttachment.reloadData()
        } else {
            imgCoverImage.image = tempImage
        }
        self.dismiss(animated: true, completion: nil)
        checkDoneButton()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension SubTaskViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let arr = ["type": 1,
                   "Info": urls[0]] as [String : Any]
        arrAttachment.append(arr)
        collectionAttachment.reloadData()
        checkDoneButton()
    }
}
