//
//  SplashViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 28/03/22.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet var bottomLogo : UIImageView?
    @IBOutlet var logo : UIImageView?
    @IBOutlet var bottomConstraint : NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        startAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUpMode()
    }
    
    func startAnimation() {
        self.logo!.transform = CGAffineTransform(scaleX: 0.0, y: 0.25)

        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { timer in
            self.nextToScreen()
        }
        
        UIView.animateKeyframes(withDuration: 0.9, delay: 0.0, options: UIView.KeyframeAnimationOptions.init(rawValue: 0)) {
            self.bottomConstraint?.constant = 60.0
            self.bottomLogo?.alpha = 1.0
            self.logo!.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.view.layoutIfNeeded()
        } completion: { success in
            
        }
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            self.view.backgroundColor = UIColor.black
            bottomLogo!.backgroundColor = UIColor.clear
        } else {
            self.view.backgroundColor = #colorLiteral(red: 0.4196078431, green: 0.5764705882, blue: 1, alpha: 1)
            bottomLogo!.backgroundColor = UIColor.black
        }
    }

    func nextToScreen() {
        if Defaults.isUserLoggedIn ?? false {
            Utility.gotoTabbar()
        } else {
            Utility.gotoLoginView()
        }
    }
}
