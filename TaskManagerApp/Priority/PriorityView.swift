//
//  PriorityView.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 09/12/21.
//

import UIKit
import BubblePictures

class PriorityView: UIViewController {
    @IBOutlet var viewCategory: UIView!
    @IBOutlet var tableFavouriteTask: UITableView!
    var loginViewModel = HomeViewModel()
    @IBOutlet var noTaskLabel: UILabel?
    var recentModel : [DetailDataModel]?
    var todayModel : [DetailDataModel]?
    var upcomingModel : [DetailDataModel]?
    var laterModel : [DetailDataModel]?
    var completedModel : [DetailDataModel]?
    var selCateogry: String?
    var allTask: [DetailDataModel] = []
    var filteredTask: [DetailDataModel] = []
    var categoryScroll : CategoryScroll?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.filterData(notification:)), name: Notification.Name("FilterData"), object: nil)
        print(filteredTask.count)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()

    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = UIColor.black
            tableFavouriteTask.separatorColor = UIColor.clear
            tableFavouriteTask.backgroundColor = .black
            noTaskLabel?.textColor = .white
        } else {
            view.backgroundColor = UIColor.white
            tableFavouriteTask.separatorColor = UIColor.clear
            tableFavouriteTask.backgroundColor = .white
            noTaskLabel?.textColor = .black
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCategories()
        getFevoriteTask()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func getCategories() {
        let getAllCategoriesOperation = GetAllCategoriesOperation()
        getAllCategoriesOperation.execute { str, categories in
            GlobalData.allCategories = categories ?? []
            self.loginViewModel.delegate = self
            self.loginViewModel.setupCategoryScorll(categoryArray: GlobalData.allCategories)
        }
    }
    
    func getFevoriteTask() {
        let getAllTasksOperation = GetAllTasksOperation()
        getAllTasksOperation.execute {str, response in
            self.allTask.removeAll()
            var tempTask: [DetailDataModel] = []
            if response?.recent?.count ?? 0 > 0{
                for a in (response?.recent)!{
                    tempTask.append(a)

                }
            }
            if response?.today?.count ?? 0 > 0{
                for a in (response?.today)!{
                    tempTask.append(a)
                }
            }
            if response?.upcoming?.count  ?? 0 > 0{
                for a in (response?.upcoming)!{
                    tempTask.append(a)
                }
            }
            if response?.later?.count  ?? 0 > 0{
                for a in (response?.later)!{
                    tempTask.append(a)
                }
            }
            if response?.completed?.count ?? 0 > 0{
                for a in (response?.completed)!{
                    tempTask.append(a)
                }
            }
            
            for task in tempTask {
                if task.isFavorite ?? false {
                    self.allTask.append(task)
                }
            }
            
            if Defaults.filterBy == 1 {
                self.filteredTask = Utility.applyTaskFilter(allTasks: self.allTask)
            } else if Defaults.filterBy == 2 {
                let mytask = Utility.applyTaskFilter(allTasks: self.allTask).filter({ $0.in_progress == true })
                self.filteredTask = mytask
            } else if Defaults.filterBy == 3 {
                let mytask = Utility.applyTaskFilter(allTasks: self.allTask).filter({ $0.incomplete == true })
                self.filteredTask = mytask
            }
            
            
            if self.filteredTask.isEmpty {
                self.noTaskLabel?.isHidden = false
            } else {
                self.noTaskLabel?.isHidden = true
            }
            self.tableFavouriteTask.reloadData()
        }
    }
    
    @objc func filterData(notification: Notification) {
        if Defaults.filterBy == 1 {
            self.filteredTask = Utility.applyTaskFilter(allTasks: self.allTask)
        } else if Defaults.filterBy == 2 {
            let mytask = Utility.applyTaskFilter(allTasks: self.allTask).filter({ $0.in_progress == true })
            self.filteredTask = mytask
        } else if Defaults.filterBy == 3 {
            let mytask = Utility.applyTaskFilter(allTasks: self.allTask).filter({ $0.incomplete == true })
            self.filteredTask = mytask
        }
        
        if self.filteredTask.isEmpty {
            self.noTaskLabel?.isHidden = false
        } else {
            self.noTaskLabel?.isHidden = true
        }
        
        self.tableFavouriteTask.reloadData()
    }
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        sideMenuController?.revealMenu()
    }
    
    @IBAction func moreAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Priority", bundle: nil)
        let vc = (sb.instantiateViewController(identifier: "PriortyFilterViewController")) as! PriortyFilterViewController
        vc.modalPresentationStyle = .overFullScreen
        vc.view.backgroundColor = UIColor.clear
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func NotificationAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.isNavigationBarHidden = true
        self.present(navigationController, animated: true, completion: nil)
    }
}

class PriorityViewTableCell: UITableViewCell{
    @IBOutlet var bubbleCollection: UICollectionView!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var imageBg: UIImageView!
    @IBOutlet var btnStar: UIButton!
    
    var bubblePictures: BubblePictures!
    func getConfigFiles() -> [BPCellConfigFile] {
        return []
    }
}

extension PriorityView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredTask.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PriorityViewTableCell", for: indexPath) as! PriorityViewTableCell
        if filteredTask[indexPath.row].getting_started == true {
            cell.lblStatus.text = "Getting Started"
        } else if filteredTask[indexPath.row].in_progress == true {
            cell.lblStatus.text = "In Progress"
            cell.lblStatus.textColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        } else if filteredTask[indexPath.row].incomplete == true {
            cell.lblStatus.text = "Completed"
            cell.lblStatus.textColor = #colorLiteral(red: 0.7921568627, green: 0.9921568627, blue: 0.6039215686, alpha: 1)
        } else if filteredTask[indexPath.row].getting_started == false || filteredTask[indexPath.row].in_progress == false || filteredTask[indexPath.row].incomplete == false || filteredTask[indexPath.row].wont_abble_to_perform == false {
            cell.lblStatus.text = "Getting Started"
        }
        cell.lblTitle.text = filteredTask[indexPath.row].name
        cell.lblDate.text = filteredTask[indexPath.row].date
        if allTask[indexPath.row].isFavorite == true{
            cell.btnStar.isHidden = false
        }else{
            cell.btnStar.isHidden = true
        }
        let path = allTask[indexPath.row].backgroundImages?.first?.path
        if path != ""{
            let fullPathArr = path?.components(separatedBy: "public")
            cell.imageBg.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "user"), options: .none) { result in
            }
        }
        
        var configFiles = cell.getConfigFiles()
        if !filteredTask[indexPath.row].assignId!.isEmpty {
            for item in filteredTask[indexPath.row].assignId! {
                let user = User()
                user.userId = item["id"] as? String ?? ""
                user.userName = item["type"] as? String ?? ""
                user.assigneeStatus = item["assigneeStatus"] as? String ?? ""
                configFiles.append(BPCellConfigFile(
                    imageType: BPImageType.image(UIImage(named: "one-1")!), title: ""))
            }
        }
        let layoutConfigurator = BPLayoutConfigurator(
            colorForBubbleBorders: UIColor.gray,
            colorForBubbleTitles: UIColor.clear,
            maxCharactersForBubbleTitles: 0,
            maxNumberOfBubbles: 10,
            displayForTruncatedCell: BPTruncatedCellDisplay.image(BPImageType.image(UIImage(named: "one-1")!)),
            direction: .leftToRight,
            alignment: .right)
        cell.bubblePictures = BubblePictures(collectionView: cell.bubbleCollection, configFiles: configFiles, layoutConfigurator: layoutConfigurator)
        cell.bubblePictures.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "HomeDetailViewController") as! HomeDetailViewController
        vc.detailData = filteredTask[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: self.tableFavouriteTask.frame.width,
                                              height: 50))
        headerView.backgroundColor = #colorLiteral(red: 0.1176470588, green: 0.1176470588, blue: 0.1176470588, alpha: 1)
        let label = UILabel(frame: headerView.bounds)
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont(name: "SFProDisplay-Bold", size: 16)
        label.text = "Today"
        headerView.addSubview(label)
        return headerView
    }
}

extension PriorityView : HomeViewModelProtocol {
    func populateCategries(categoryItemArray: [Any]) {
    }
    
    func setupCategoryScorllDone(categoryArray: [Category]) {
        if !categoryArray.isEmpty {
            for view in (categoryScroll?.subviews ?? []) {
                view.removeFromSuperview()
            }
            
            let category = Category(dictionary: [:])
            category?._id = "all_cat_id"
            category?.name = "All Category"
            category?.updatedAt = categoryArray[0].updatedAt
            category?.createdAt = categoryArray[0].createdAt
            category?.description = categoryArray[0].description
            category?.published = categoryArray[0].published
            
            let categoryAhowAll = Category(dictionary: [:])
            categoryAhowAll?._id = "show_all_cat_id"
            categoryAhowAll?.name = "+Show All"
            category?.updatedAt = categoryArray[0].updatedAt
            categoryAhowAll?.createdAt = categoryArray[0].createdAt
            categoryAhowAll?.description = categoryArray[0].description
            categoryAhowAll?.published = categoryArray[0].published
            
            var addedCategoryArray = [Category]()
            if categoryArray.count > 2{
                for a in categoryArray{
                    if addedCategoryArray.count < 2{
                        addedCategoryArray.append(a)
                    }
                }
                addedCategoryArray.insert(category!, at: 0)
                addedCategoryArray.insert(categoryAhowAll!, at: 3)
            } else {
                addedCategoryArray = categoryArray
                addedCategoryArray.insert(category!, at: 0)
            }
            
            categoryScroll = CategoryScroll(frame: CGRect(x: 0, y: 100, width: ScreenSize.SCREEN_WIDTH, height: 38))
            categoryScroll!.setupScroll(itemArray: addedCategoryArray)
            categoryScroll!.categoryScrollDelegate = self
            self.view.addSubview(categoryScroll!)
        }
    }
}

extension PriorityView : CategoryScrollProtocol {
    func categoryChoosed(selectedItemName: String) {
        selCateogry = selectedItemName
        if selCateogry == "All Category" {
            self.filteredTask = self.allTask
        } else if selCateogry == "+Show All" {
            let sb = UIStoryboard(name: "NewTask", bundle: nil)
            let vc = (sb.instantiateViewController(identifier: "AllCategoriesViewController")) as! AllCategoriesViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.priorityViewObject = self
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.present(vc, animated: false, completion: nil)
        } else {
            self.filteredTask = self.allTask.filter({ myTask in
                let categoryArr = myTask.category
                return categoryArr!.contains(where: { cat in
                    cat.name == self.selCateogry
                })
            })
        }
        if self.filteredTask.isEmpty {
            self.noTaskLabel?.isHidden = false
        } else {
            self.noTaskLabel?.isHidden = true
        }
        self.tableFavouriteTask.reloadData()
    }
}

extension PriorityView: BPDelegate {
    func didSelectTruncatedBubble() {
        print("Selected truncated bubble")
    }
    
    func didSelectBubble(at index: Int) {
        print(index)
    }
}
