//
//  PriortyFilterViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 10/01/22.
//

import UIKit

class PriortyFilterViewController: UIViewController {
    @IBOutlet var allTaskCheck: UIButton!
    @IBOutlet var inPendingCheck: UIButton!
    @IBOutlet var completedCheck: UIButton!
    @IBOutlet var menuView: UIView!
    @IBOutlet var lblShortBy: UILabel!
    @IBOutlet var btnAllTask: UIButton!
    @IBOutlet var btnCompletedTask: UIButton!
    @IBOutlet var btnPending: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setFilterBy()
        setUpMode()
    }
    
    func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            menuView.backgroundColor = .black
            lblShortBy.textColor = .white
            btnAllTask.setTitleColor(UIColor.white, for: .normal)
            btnCompletedTask.setTitleColor(UIColor.white, for: .normal)
            btnPending.setTitleColor(UIColor.white, for: .normal)
        } else {
            menuView.backgroundColor = UIColor.white
            lblShortBy.textColor = .black
            btnAllTask.setTitleColor(UIColor.black, for: .normal)
            btnCompletedTask.setTitleColor(UIColor.black, for: .normal)
            btnPending.setTitleColor(UIColor.black, for: .normal)
        }
    }
    
    @IBAction func filterByAction(_ sender: UIButton) {
        Defaults.filterBy = sender.tag
        setFilterBy()
        NotificationCenter.default.post(name: Notification.Name("FilterData"), object: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch?.view != self.menuView
        { self.dismiss(animated: false, completion: nil) }
    }
    
    func setFilterBy() {
        if Defaults.filterBy == 1 {
            allTaskCheck.isHidden = false
            inPendingCheck.isHidden = true
            completedCheck.isHidden = true
        } else if Defaults.filterBy == 2 {
            allTaskCheck.isHidden = true
            inPendingCheck.isHidden = false
            completedCheck.isHidden = true
        } else if Defaults.filterBy == 3 {
            allTaskCheck.isHidden = true
            inPendingCheck.isHidden = true
            completedCheck.isHidden = false
        }
    }
}

