//
//  TabbarView.swift

//
//  Created by Chaman Sharma on 14/05/22.
//

import UIKit

class TabbarView: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = selectedTabIndex
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        updateData()
    }
    
    func updateData() {
        
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            tabBar.barTintColor = UIColor(named: "Black50")
            tabBar.backgroundColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 1)
            tabBar.tintColor = .white
            tabBar.borderColor = .clear
        } else {
            self.tabBar.unselectedItemTintColor = .gray
            tabBar.barTintColor = .white
            tabBar.tintColor = UIColor.init(red: 115/255, green: 146/255, blue: 245/255, alpha: 1)
            tabBar.isTranslucent = false
            tabBar.borderColor = .gray
            tabBar.backgroundColor = .white
            tabBar.layer.borderWidth = 0.3
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    }
}
