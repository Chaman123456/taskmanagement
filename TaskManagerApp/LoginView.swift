//
//  LoginView.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 07/12/21.
//

import UIKit
import SKCountryPicker
import SwiftUI
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices

class LoginView: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    var scrollView : UIScrollView?
    @IBOutlet var loginButton : UIButton?
    @IBOutlet var pagerControl: UIPageControl?
    @IBOutlet var phoneNumberText: UITextField?
    var countrycode = "+91"
    var loginViewModel = LoginViewModel()
    @IBOutlet var countryCodeButton : UIButton?
    @IBOutlet weak var tf_Email: UITextField!
    @IBOutlet weak var viewEmailBg: UIView!
    @IBOutlet weak var viewPhoneBg: UIView!
    var googleSignIn = GIDSignIn.sharedInstance
    @IBOutlet var lblSignIn: UILabel!
    @IBOutlet var lblLogin: UILabel!
    @IBOutlet var lblOr: UILabel!
    @IBOutlet var lblemailTitle: UILabel!
    @IBOutlet var lblPhoneTitle: UILabel!
    @IBOutlet var termLebel: UILabel!
    
    var isNotch: Bool {
       return (UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0) > 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        loginViewModel.delegate = self
        setUpMode()
        if let token = AccessToken.current,
           !token.isExpired {
            // User is logged in, do work such as go to next view controller.
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        termLebel.isUserInteractionEnabled = true
        termLebel.addGestureRecognizer(tap)
        //By signing in to task manager. you agree to the Terms of service and privacy policy
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
    }
    
    @objc
    func tapFunction(sender:UITapGestureRecognizer) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Account", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PrivatePolicy") as! PrivatePolicyViewController
        self.present(newViewController, animated:false, completion:nil)
    }
    
    @IBAction func loginAction() {
        self.view.endEditing(true)
        loginViewModel.loginButtonAction(phoneNumber: phoneNumberText!.text!, email: tf_Email.text ?? "")
    }
    
    @IBAction func countryPickerAction() {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { country in
            self.countryCodeButton?.setTitle(country.dialingCode ?? "", for: UIControl.State.normal)
            self.countrycode = country.dialingCode ?? ""
        }
        countryController.flagStyle = .circular
        countryController.favoriteCountriesLocaleIdentifiers = ["IN"]
          
        if self.traitCollection.userInterfaceStyle == .dark {
            countryController.labelColor = UIColor.white
            countryController.detailColor = UIColor.white
            countryController.separatorLineColor = UIColor.lightGray
            countryController.tableView.backgroundColor = .black
            countryController.navigationController?.navigationBar.tintColor = .white
            countryController.navigationController?.navigationBar.backgroundColor = .black
        } else {
            countryController.labelColor = UIColor.black
            countryController.detailColor = UIColor.black
            countryController.separatorLineColor = UIColor.lightGray
            countryController.tableView.backgroundColor = .white
            countryController.navigationController?.navigationBar.tintColor = .black
            countryController.navigationController?.navigationBar.backgroundColor = .white
        }
    }
    
    func setupScrollView() {
        if isNotch {
            scrollView?.removeFromSuperview()
            scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT / 3))
            scrollView!.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH*4, height: 0)
            scrollView!.isPagingEnabled = true
            scrollView!.isScrollEnabled = true
            scrollView!.showsHorizontalScrollIndicator = false
            scrollView!.delegate = self
            self.view.addSubview(scrollView!)
            
            var xPosition : CGFloat = 0.0;
            for i in 0...3 {
                var labelFirstText = " "
                var imageName = " "
                if i == 0 {
                    imageName = "intro1"
                    labelFirstText = "Assign, Manage & Track Tasks"
                } else if i == 1 {
                    imageName = "intro2"
                    labelFirstText = "Share tasks with Your mates"
                } else if i == 2 {
                    imageName = "intro3"
                    labelFirstText = "Have conversations Over assigned task"
                } else {
                    imageName = "intro4"
                    labelFirstText = "Set locations and Share Images"
                }
                
                let view = UIView()
                view.frame = CGRect(x: xPosition, y: 0, width: scrollView!.frame.size.width, height: scrollView!.frame.size.height)
                scrollView!.addSubview(view)
                
                let imageView = UIImageView()
                imageView.frame = CGRect(x: view.frame.size.width/2 - 80, y: 60, width: 150, height: 150)
                imageView.image = UIImage(named: imageName)
                imageView.contentMode = UIView.ContentMode.scaleAspectFit
                view.addSubview(imageView)
                
                let labelFirst = UILabel()
                labelFirst.frame = CGRect(x: 40, y: (imageView.frame.origin.y + imageView.frame.size.height + 5), width: ScreenSize.SCREEN_WIDTH - 80, height: 50)
                
                if self.traitCollection.userInterfaceStyle == .dark {
                    labelFirst.textColor = .white
                    viewEmailBg.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 0.8979201159)
                    viewPhoneBg.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 0.8979201159)
                } else {
                    labelFirst.textColor = UIColor.black
                    viewEmailBg.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.9049824089)
                    viewPhoneBg.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.9049824089)
                }
                
                labelFirst.textAlignment = NSTextAlignment.center
                labelFirst.numberOfLines = 0
                labelFirst.lineBreakMode = .byWordWrapping
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineHeightMultiple = 0.92
                paragraphStyle.alignment = .center
                labelFirst.attributedText = NSMutableAttributedString(string: labelFirstText, attributes: [NSAttributedString.Key.kern: 1, NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: FontUtility.getFont(with: FontUtility.FontType.display, withTrait: FontUtility.TraitType.Bold, ofSize: FontUtility.FontSize.custom(25))])
                labelFirst.sizeToFit()
                labelFirst.frame = CGRect(x: view.frame.size.width/2 - labelFirst.frame.size.width/2, y: labelFirst.frame.origin.y, width: labelFirst.frame.size.width, height: labelFirst.frame.size.height)
                view.addSubview(labelFirst)
                xPosition = xPosition + ScreenSize.SCREEN_WIDTH
            }
        } else {
            scrollView?.removeFromSuperview()
            scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT / 3))
            scrollView!.contentSize = CGSize(width: ScreenSize.SCREEN_WIDTH*4, height: 0)
            scrollView!.isPagingEnabled = true
            scrollView!.isScrollEnabled = true
            scrollView!.showsHorizontalScrollIndicator = false
            scrollView!.delegate = self
            pagerControl!.isHidden = true;
            self.view.addSubview(scrollView!)
            
            var xPosition : CGFloat = 0.0;
            for i in 0...3 {
                var labelFirstText = " "
                var imageName = " "
                if i == 0 {
                    imageName = "intro1"
                    labelFirstText = "Assign, Manage & Track Tasks"
                } else if i == 1 {
                    imageName = "intro2"
                    labelFirstText = "Share tasks with Your mates"
                } else if i == 2 {
                    imageName = "intro3"
                    labelFirstText = "Have conversations Over assigned task"
                } else {
                    imageName = "intro4"
                    labelFirstText = "Set locations and Share Images"
                }
                
                let view = UIView()
                view.frame = CGRect(x: xPosition, y: 0, width: scrollView!.frame.size.width, height: scrollView!.frame.size.height)
                scrollView!.addSubview(view)
                
                let imageView = UIImageView()
                imageView.frame = CGRect(x: view.frame.size.width/2 - 80, y: 60, width: 150, height: 80)
                imageView.image = UIImage(named: imageName)
                imageView.contentMode = UIView.ContentMode.scaleAspectFit
                view.addSubview(imageView)
                
                let labelFirst = UILabel()
                labelFirst.frame = CGRect(x: 40, y: (imageView.frame.origin.y + imageView.frame.size.height + 5), width: ScreenSize.SCREEN_WIDTH - 80, height: 50)
                
                if self.traitCollection.userInterfaceStyle == .dark {
                    labelFirst.textColor = .white
                    viewEmailBg.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 0.8979201159)
                    viewPhoneBg.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 0.8979201159)
                } else {
                    labelFirst.textColor = UIColor.black
                    viewEmailBg.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.9049824089)
                    viewPhoneBg.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.9049824089)
                }
                
                labelFirst.textAlignment = NSTextAlignment.center
                labelFirst.numberOfLines = 0
                labelFirst.lineBreakMode = .byWordWrapping
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineHeightMultiple = 0.92
                paragraphStyle.alignment = .center
                labelFirst.attributedText = NSMutableAttributedString(string: labelFirstText, attributes: [NSAttributedString.Key.kern: 1, NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: FontUtility.getFont(with: FontUtility.FontType.display, withTrait: FontUtility.TraitType.Bold, ofSize: FontUtility.FontSize.custom(14))])
                labelFirst.sizeToFit()
                labelFirst.frame = CGRect(x: view.frame.size.width/2 - labelFirst.frame.size.width/2, y: labelFirst.frame.origin.y, width: labelFirst.frame.size.width, height: labelFirst.frame.size.height)
                view.addSubview(labelFirst)
                xPosition = xPosition + ScreenSize.SCREEN_WIDTH
            }
        }
    }
    
    @objc func setUpMode() {
        setupScrollView()
        if self.traitCollection.userInterfaceStyle == .dark {
            lblLogin.textColor = .white
            lblOr.textColor = .white
            lblemailTitle.textColor = .white
            lblPhoneTitle.textColor = .white
            lblSignIn.textColor = .white
            tf_Email.tintColor = .white
            phoneNumberText?.tintColor = .white
            pagerControl?.currentPageIndicatorTintColor = UIColor.white
            countryCodeButton?.setTitleColor(UIColor.white, for: .normal)
            phoneNumberText?.textColor = UIColor.white
            tf_Email?.textColor = UIColor.white
            countryCodeButton?.setTitleColor(UIColor.white, for: .normal)
            pagerControl?.currentPageIndicatorTintColor = UIColor.white
            self.view.backgroundColor = UIColor.black
        } else {
            lblLogin.textColor = .black
            lblOr.textColor = .black
            lblemailTitle.textColor = .black
            lblPhoneTitle.textColor = .black
            lblSignIn.textColor = .black
            tf_Email.tintColor = .black
            phoneNumberText?.tintColor = .black
            loginButton?.backgroundColor = UIColor(red: 115/256, green: 146/256, blue: 244/256, alpha: 1)
            phoneNumberText?.textColor = UIColor.black
            phoneNumberText?.placeholderColor  = UIColor.init(red: 174/255, green: 174/255, blue: 174/255, alpha: 1)
            tf_Email?.textColor = UIColor.black
            tf_Email?.placeholderColor  = UIColor.init(red: 174/255, green: 174/255, blue: 174/255, alpha: 1)
            countryCodeButton?.setTitleColor(UIColor.darkGray, for: .normal)
            pagerControl?.currentPageIndicatorTintColor = UIColor.black
            self.view.backgroundColor = UIColor.white
        }
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pagerControl!.currentPage) * pagerControl!.frame.size.width
        scrollView!.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pagerControl!.currentPage = Int(pageNumber)
    }
    
    @IBAction func facebookAction() {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { result, error in
            if let error = error {
                print("Encountered Erorr: \(error)")
            } else if let result = result, result.isCancelled {
                print("Cancelled")
            } else {
                print("Logged In")
                self.getUserProfile(token: result?.token, userId: result?.token?.userID)
            }
        }
    }
    
    @IBAction func googleAction(_ sender: Any) {
        self.googleAuthLogin()
    }

    @IBAction func appleAction(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
    }
    
    func googleAuthLogin() {
        let googleConfig = GIDConfiguration(clientID: "1089886493759-gsd2mf9qh8hr6pfsunp7n4egkm1mi6o0.apps.googleusercontent.com")
        self.googleSignIn.signIn(with: googleConfig, presenting: self) { user, error in
            if error == nil {
                guard let user = user else {
                    print("Uh oh. The user cancelled the Google login.")
                    return
                }
                let userId = user.userID ?? ""
                print("Google User ID: \(userId)")
                let userIdToken = user.authentication.idToken ?? ""
                print("Google ID Token: \(userIdToken)")
                let userFirstName = user.profile?.givenName ?? ""
                print("Google User First Name: \(userFirstName)")
                let userLastName = user.profile?.familyName ?? ""
                print("Google User Last Name: \(userLastName)")
                let userEmail = user.profile?.email ?? ""
                print("Google User Email: \(userEmail)")
                let googleProfilePicURL = user.profile?.imageURL(withDimension: 150)?.absoluteString ?? ""
                print("Google Profile Avatar URL: \(googleProfilePicURL)")
                
                //API social login
                let socialData = [
                    "accountCreatedVia": "Google",
                    "accessToken": userId,
                    "fname": userFirstName,
                    "lname": userLastName,
                    "email": userEmail
                ] as [String : Any]
                self.socialLoginAPi(socialData: socialData)
            }
        }
    }
    
    func getUserProfile(token: AccessToken?, userId: String?) {
            let graphRequest: GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields": "id, first_name, middle_name, last_name, name, picture, email"])
            graphRequest.start { _, result, error in
                if error == nil {
                    let data: [String: AnyObject] = result as! [String: AnyObject]
                    
                    var facebookId: String
                    var facebookFirstName: String
                    var facebookLastName: String
                    var facebookEmail: String
                    
                    // Facebook Id
                    facebookId = data["id"] as? String ?? ""
                    // Facebook First Name
                    facebookFirstName = data["first_name"] as? String ?? ""
                    // Facebook Last Name
                    facebookLastName = data["last_name"] as? String ?? ""
                    // Facebook Name
                    if let facebookName = data["name"] as? String {
                        print("Facebook Name: \(facebookName)")
                    } else {
                        print("Facebook Name: Not exists")
                    }
                    // Facebook Profile Pic URL
                    let facebookProfilePicURL = "https://graph.facebook.com/\(userId ?? "")/picture?type=large"
                    print("Facebook Profile Pic URL: \(facebookProfilePicURL)")
                    // Facebook Email
                    facebookEmail = data["email"] as? String ?? ""
                    print("Facebook Access Token: \(token?.tokenString ?? "")")
                    
                    //API social login
                    let socialData = [
                        "accountCreatedVia": "Facebook",
                        "accessToken": facebookId,
                        "fname": facebookFirstName,
                        "lname": facebookLastName,
                        "email": facebookEmail
                    ] as [String : Any]
                    self.socialLoginAPi(socialData: socialData)
                    
                } else {
                    print("Error: Trying to get user's info")
                }
            }
        }
    
    func socialLoginAPi(socialData: [String:Any]){
        let social = LoginOperation()
        social.executeSocialLogin(detailData: socialData) { error, userLoginModel in
            let isSuccess = userLoginModel?.success ?? false
            if isSuccess {
                Defaults.isUserLoggedIn = true
                Defaults.Token = userLoginModel?.token
                Defaults.userId = userLoginModel?.userId
                Utility.gotoTabbar()
            } else {
                self.view.makeToast(SOMETHING_WENT_WRONG_STR)
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tf_Email{
            viewEmailBg.borderColor = #colorLiteral(red: 0.3190019131, green: 0.4265828133, blue: 0.7188324332, alpha: 1)
            viewPhoneBg.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }else if textField == phoneNumberText{
            viewEmailBg.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            viewPhoneBg.borderColor = #colorLiteral(red: 0.3190019131, green: 0.4265828133, blue: 0.7188324332, alpha: 1)
        }
    }
}

extension LoginView : LoginViewModelProtocol {
    func loginButtonPressed(sucess: Bool, error: String) {
        if sucess {
            let otpInfo = OTPData()
            var phoneNumberValue = phoneNumberText?.text ?? ""
            if !countrycode.isEmpty && !phoneNumberValue.isEmpty{
                phoneNumberValue = "\(countrycode)\(phoneNumberValue)"
            }
            otpInfo.phoneNumber = phoneNumberValue
            otpInfo.email = "\(tf_Email.text ?? "")"
            let otpOperation = OTPOperation(withOTPDetails: otpInfo)
            otpOperation.executeGetOTP { str, otpModelData in
                let isSuccess = otpModelData?.success ?? false
                if isSuccess {
                    Defaults.Token = otpModelData?.token
                    let sb = UIStoryboard(name: "Main", bundle: nil)
                    let vc = sb.instantiateViewController(withIdentifier: "VerifyEmailViewController") as! VerifyEmailViewController
                    vc.email = "\(self.tf_Email!.text!)"
                    vc.phone = "\(phoneNumberValue)"
                    self.present(vc, animated: true, completion: nil)
                } else {
                    self.view.makeToast(SOMETHING_WENT_WRONG_STR)
                }
            }
        } else {
            self.view.makeToast(error)
        }
    }
}

extension LoginView: ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

extension LoginView: ASAuthorizationControllerDelegate {
    // Authorization Failed
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }

    // Authorization Succeeded
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let userId = appleIDCredential.user
            let userEmail = appleIDCredential.email
                let socialData = [
                "accountCreatedVia": "Apple",
                "appletoken": userId,
                "email": userEmail ?? ""
            ] as [String : Any]
            self.socialLoginAPi(socialData: socialData)
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
        }
    }
}
