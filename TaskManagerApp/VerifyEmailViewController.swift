//
//  VerifyEmailViewController.swift
//  ReplyMe
//
//  Created by chaman Sharma on 04/01/21.
//

import UIKit
import OTPTextField
import SwiftUI

class VerifyEmailViewController: UIViewController, OTPTextFieldDelegate {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnReceiveOtp: UIButton!
    @IBOutlet weak var resendOtp: UIButton!
    @IBOutlet weak var otpView: OTPTextField!
    
    var phone = String()
    var email = String()
    var otp = String()
    var user = User()
    let mutableString = NSMutableAttributedString(string: "Receive OTP on alternate email / number. Click here")//"Receive OTP on alternate number. Click here"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard))
        self.view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        lblTitle.text = "Enter OTP sent to email/number"
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
        setupOtpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //showAlertOtp(OTP: otp)
    }
    
    @objc func hideKeyBoard(sender: UITapGestureRecognizer? = nil){
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpMode()
    }
    
    func showAlertOtp(OTP: String){
        let leaveAlert = UIAlertController(title: "", message: "Your OTP is \n\(OTP)", preferredStyle: UIAlertController.Style.alert)
        leaveAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
        }))
        self.present(leaveAlert, animated: true, completion: nil)
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            self.view.backgroundColor = UIColor.black
            mutableString.addAttribute(.foregroundColor, value: UIColor.init(red: 169/255, green: 169/255, blue: 169/255, alpha: 1), range: NSRange(location: 0, length: 39))
            mutableString.addAttribute(.foregroundColor, value: UIColor.init(red: 107/255, green: 147/255, blue: 247/255, alpha: 1), range: NSRange(location: 40, length: 11))
            btnReceiveOtp.setAttributedTitle(mutableString, for: .normal)
        } else {
            self.view.backgroundColor = UIColor.white
            btnReceiveOtp.setTitleColor(UIColor.init(red: 107/255, green: 147/255, blue: 247/255, alpha: 1), for: .normal)
            btnReceiveOtp.shadowRadius = 0.0
            btnReceiveOtp.shadowOpacity = 0.0
            btnReceiveOtp.shadowOffset = CGSize(width: 0.0, height: 0.0)
            resendOtp.shadowRadius = 0.0
            resendOtp.shadowOpacity = 0.0
            resendOtp.shadowOffset = CGSize(width: 0.0, height: 0.0)
        }
    }
    
    func setupOtpView(){
        otpView.otpDelegate = self
        otpView.textContentType = .oneTimeCode
        otpView.becomeFirstResponder()
    }
    
    @IBAction func submitAction(_ sender: Any) {
        guard !((otp.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)).isEmpty) else {
            self.view.makeToast(OTP)
            return
        }
        
        let userDetails = [
            "token": Defaults.Token,
            "otp": otp,
            "devicetoken" : Defaults.APNSTokenSave,
            "deviceid": UIDevice.current.identifierForVendor!.uuidString,
            "mobile": phone,
            "email": email
        ]
        let loginOperation = LoginOperation()
        loginOperation.executeLogin(userDetail: userDetails as [String : Any]) { error, userLoginModel in
            let isSuccess = userLoginModel?.success ?? false
            if isSuccess {
                Defaults.isUserLoggedIn = true
                Defaults.Token = userLoginModel?.token
                Defaults.userId = userLoginModel?.userId
                Utility.gotoTabbar()
            } else {
                self.view.makeToast(error ?? SOMETHING_WENT_WRONG_STR)
            }
        }
    }
    
    @IBAction func resendAction(_ sender: Any) {
        let otpInfo = OTPData()
        otpInfo.phoneNumber = self.phone
        otpInfo.email = self.email
        let otpOperation = OTPOperation(withOTPDetails: otpInfo)
        otpOperation.executeGetOTP { str, otpModelData in
            let isSuccess = otpModelData?.success ?? false
            if isSuccess {
                self.view.makeToast(RESEND_OTP)
                //self.showAlertOtp(OTP: otpModelData?.otp ?? self.otp)
            }else{
                self.view.makeToast(SOMETHING_WENT_WRONG_STR)
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func otpTextField(_ textField: OTPTextField, didChange otpCode: String) {
        if otpCode.count > 5{
            otp = otpCode
            self.view.endEditing(true)
        }else{
            otp = ""
        }
    }
}
