//
//  InitialViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 15/12/22.
//

import UIKit

class InitialViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        setUpMode()
    }

    @IBAction func actionGettingStarted(_ sender: Any) {
        Utility.gotoLoginView()
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            self.view.backgroundColor = UIColor.black
        } else {
            self.view.backgroundColor = #colorLiteral(red: 0.4196078431, green: 0.5764705882, blue: 1, alpha: 1)
        }
    }
}
