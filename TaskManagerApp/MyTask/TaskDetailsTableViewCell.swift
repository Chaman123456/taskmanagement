//
//  TaskDetailsTableViewCell.swift
//  TaskManager_Demo
//
//  Created by Anand Patel on 12/09/22.
//

import UIKit

class TaskDetailsTableViewCell: UITableViewCell {
    @IBOutlet var lblTaskName: UILabel!
    @IBOutlet var lblDueDate: UILabel!
    @IBOutlet var imgTask: UIImageView!
    @IBOutlet var lblTaskCategories: UILabel!
    @IBOutlet var lblCreatedDate: UILabel!
    @IBOutlet var viewStatus: UIView!
    @IBOutlet var btnStatus: UIButton!
    @IBOutlet var btnDeclineTask: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
