//
//  MyTask.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 17/09/22.
//

import UIKit

class MyTask: UIViewController {
    var taskNamesArray = ["Study", "Eating"]
    var dueDateArray = ["Due Date: 15 Sep", "Due Date: 16 Sep"]
    var taskImageArray = ["party","student"]
    var taskCatArray = ["Birthday Celebration cake","College First Day"]
    var allTask: [DetailDataModel] = []
    var filteredTask: [DetailDataModel] = []
    @IBOutlet var noTaskAvaible: UILabel!
    @IBOutlet var backButton: UIButton?
    @IBOutlet var taskDetailsTableView: UITableView!
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        self.setUpMode()
        super.viewDidLoad()
        getAllTask()
    }
    
    @IBAction func backAction() {
        Utility.gotoTabbar()
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            backButton?.setTitleColor(.white, for: .normal)
            view.backgroundColor = .black
        } else {
            backButton?.setTitleColor(.black, for: .normal)
            view.backgroundColor = .white
        }
        taskDetailsTableView.reloadData()
    }
    
    func getAllTask() {
        let getAllTasksOperation = GetAllTasksOperation()
        getAllTasksOperation.execute {str, response in
            self.allTask.removeAll()
            if response?.recent?.count ?? 0 > 0{
                for a in (response?.recent)!{
                    self.allTask.append(a)
                }
            }
            if response?.today?.count ?? 0 > 0{
                for a in (response?.today)!{
                    self.allTask.append(a)
                }
            }
            if response?.upcoming?.count  ?? 0 > 0{
                for a in (response?.upcoming)!{
                    self.allTask.append(a)
                }
            }
            if response?.later?.count  ?? 0 > 0{
                for a in (response?.later)!{
                    self.allTask.append(a)
                }
            }
            if response?.completed?.count ?? 0 > 0{
                for a in (response?.completed)!{
                    self.allTask.append(a)
                }
            }
            //Apply Filter
            self.filteredTask = Utility.applyTaskFilterForMyRequest(allTasks: self.allTask).sorted {($0.created_at)! > ($1.created_at)!}
            self.taskDetailsTableView.reloadData()
            
            if self.filteredTask.count > 0{
                self.noTaskAvaible.isHidden = true
            }else{
                self.noTaskAvaible.isHidden = false
            }
        }
    }
    
    func updateStatus(newAssigneeStatus: String, assigneeId: String){
        let getAllTasksOperation = GetAllTasksOperation()
        getAllTasksOperation.updateAssignStatus(newAssigneeStatus: newAssigneeStatus, assigneeId: assigneeId) { error, response in
            print(response ?? "")
            self.getAllTask()
        }
    }
    
    @objc func acceptAction(sender: UIButton!) {
        if sender?.titleLabel?.text != "Status"{
            let data = filteredTask[sender.tag]
            let info = data.assignId?.filter({ $0["userId"] as? String == Defaults.userId ?? "" })
            print(info ?? "")
            updateStatus(newAssigneeStatus: AssigneeStatus.accepted.rawValue, assigneeId: info?.last?["_id"] as? String ?? "")
        }
    }
    @objc func declineAction(sender: UIButton!) {
        if sender?.titleLabel?.text != "Getting Started"{
            let data = filteredTask[sender.tag]
            let info = data.assignId?.filter({ $0["userId"] as? String == Defaults.userId ?? "" })
            print(info ?? "")
            updateStatus(newAssigneeStatus: AssigneeStatus.declined.rawValue , assigneeId: info?.last?["_id"] as? String ?? "")
        }
    }
}

extension MyTask: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredTask.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskDetailsTableViewCell", for: indexPath) as! TaskDetailsTableViewCell
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.init(red: 87/255, green: 87/255, blue: 87/255, alpha: 1)
            cell.lblTaskName.textColor = UIColor.init(red: 116/255, green: 146/255, blue: 248/255, alpha: 1)
            cell.viewStatus.backgroundColor = .gray
            cell.viewStatus.layer.borderWidth = 0.5
            cell.viewStatus.layer.borderColor = UIColor.lightGray.cgColor
            cell.viewStatus.backgroundColor = UIColor.init(red: 38/255, green: 38/255, blue: 38/255, alpha: 1)
            cell.imgTask.layer.cornerRadius = 2
            cell.btnStatus.setTitleColor(UIColor.init(red: 116/255, green: 146/255, blue: 248/255, alpha: 1), for: .normal)
            cell.btnDeclineTask.setTitleColor(UIColor.init(red: 116/255, green: 146/255, blue: 248/255, alpha: 1), for: .normal)
        } else {
            cell.lblTaskName.textColor = .white
            cell.viewStatus.backgroundColor = UIColor.init(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)
            cell.backgroundColor = .black
            cell.btnStatus.setTitleColor(UIColor.white, for: .normal)
            cell.btnDeclineTask.setTitleColor(UIColor.init(red: 116/255, green: 146/255, blue: 248/255, alpha: 1), for: .normal)
        }
        let data = filteredTask[indexPath.row]
        let path = data.backgroundImages?.first?.path
        if path != ""{
            let fullPathArr = path?.components(separatedBy: "public")
            cell.imgTask.kf.setImage(with: URL(string: "\(BaseURL)\(fullPathArr?[1] ?? "")"), placeholder: UIImage(named: "image"), options: .none) { result in
            }
        }
        cell.btnStatus.tag = indexPath.row
        cell.btnDeclineTask.tag = indexPath.row
        cell.btnStatus.addTarget(self, action: #selector(acceptAction), for: .touchUpInside)
        cell.btnDeclineTask.addTarget(self, action: #selector(declineAction), for: .touchUpInside)
        cell.lblDueDate.text = Defaults.getFormattedDate(string: data.assignDate ?? "", formatter: "MMM dd,yyyy")
        cell.lblCreatedDate.text = Defaults.getFormattedDate(string: data.created_at ?? "", formatter: "MMM dd,yyyy")
        cell.lblTaskName.text = data.category?.last?.name
        cell.lblTaskCategories.text = data.name
        let info = data.assignId?.filter({ $0["userId"] as? String == Defaults.userId ?? "" })
        if (info?.first?["assigneeStatus"] as? String == AssigneeStatus.pending.rawValue || info?.first?["assigneeStatus"] as? String == AssigneeStatus.invited.rawValue){
            cell.btnStatus.setTitle("Accept Task", for: .normal)
            cell.btnDeclineTask.setTitle("Decline Task", for: .normal)
        }else{
            cell.btnStatus.setTitle("Status", for: .normal)
            cell.btnDeclineTask.setTitle("Getting Started", for: .normal)
        }

        let tintView = UIView()
        tintView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        tintView.frame = CGRect(x: 0, y: 0, width: cell.imgTask.frame.width, height: cell.imgTask.frame.height)
        cell.imgTask.addSubview(tintView)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 285
    }
}

