//
//  AccountView.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 09/12/21.
//

import UIKit
import MobileCoreServices
import Kingfisher
import SwiftUI
import Toast
import SKCountryPicker

class AccountView: UIViewController, UITextFieldDelegate {
    @IBOutlet var imageProfile: UIImageView!
    @IBOutlet var txtFieldName: UITextField!
    @IBOutlet var txtFieldEmail: UITextField!
    @IBOutlet var txtFieldPhoneNumber: UITextField!
    @IBOutlet var btnPrivacyCheck: UIButton!
    @IBOutlet var profileImageText: UILabel!
    @IBOutlet var deleteAccountBtn : UIButton!
    @IBOutlet weak var btnProfileTop: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnRead: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var deleteButton: UIButton?
    @IBOutlet weak var separatorLine_Second: UILabel!
    @IBOutlet weak var separatorLine_Third: UILabel!
    @IBOutlet weak var separatorLine_Fourth: UILabel!
    @IBOutlet var countryCodeButton : UIButton?
    var countrycode = "+91"
    var tempImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtFieldPhoneNumber.delegate = self
        self.txtFieldName.isUserInteractionEnabled = true
        self.txtFieldEmail.isUserInteractionEnabled = true
        btnPrivacyCheck.setImage(UIImage(named:"Checkmark"), for: .normal)
        btnPrivacyCheck.setImage(UIImage(named:"Checkmarkempty"), for: .selected)
        txtFieldPhoneNumber.text = Defaults.userPhoneNumber
        NotificationCenter.default.addObserver(self, selector: #selector(self.setUpMode), name: Notification.Name("ThemeChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.profileUpdate), name: Notification.Name("ProfileUpdated"), object: nil)
        setDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpMode()
        Utility.callInitialData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @objc func profileUpdate() {
        self.setDetails()
    }
    
    @objc func setUpMode() {
        if self.traitCollection.userInterfaceStyle == .dark {
            self.view.backgroundColor = .black
            btnProfileTop.titleLabel?.textColor = .white
            txtFieldName.textColor = .white
            txtFieldEmail.textColor = .white
            txtFieldName.tintColor = .white
            txtFieldEmail.tintColor = .white
            txtFieldPhoneNumber.textColor = .white
            btnPrivacyCheck.setTitleColor(UIColor.white, for: .normal)
            btnLogout.setTitleColor(UIColor.white, for: .normal)
            btnLogout.tintColor = UIColor.white
            btnEdit.setTitleColor(UIColor.white, for: .normal)
            deleteButton?.setTitleColor(UIColor.white, for: .normal)
            deleteButton?.tintColor = UIColor.white
        } else {
            self.view.backgroundColor = UIColor.white
            btnProfileTop.titleLabel?.textColor = UIColor.black
            txtFieldName.textColor = UIColor.black
            txtFieldEmail.textColor = UIColor.black
            txtFieldName.tintColor = .black
            txtFieldEmail.tintColor = .black
            txtFieldPhoneNumber.textColor = UIColor.black
            btnRead.setTitleColor((UIColor.init(red: 44/256, green: 103/256, blue: 210/256, alpha: 1)), for: .normal)
            btnPrivacyCheck.setTitleColor(UIColor.black, for: .normal)
            btnLogout.setTitleColor(UIColor.black, for: .normal)
            btnLogout.tintColor = UIColor.black
            separatorLine_Second.backgroundColor = UIColor.init(red: 232/256, green: 232/256, blue: 232/256, alpha: 232/256)
            separatorLine_Third.backgroundColor = UIColor.init(red: 232/256, green: 232/256, blue: 232/256, alpha: 232/256)
            separatorLine_Fourth.backgroundColor = UIColor.init(red: 232/256, green: 232/256, blue: 232/256, alpha: 232/256)
            btnEdit.setTitleColor(UIColor.black, for: .normal)
            deleteButton?.setTitleColor(UIColor.black, for: .normal)
            deleteButton?.tintColor = UIColor.black
        }
    }
    
    func setProfileImage() {
        if imageProfile.image == UIImage(named: "user") {
            profileImageText.isHidden = false
            let nameString = txtFieldName.text
            if nameString!.isEmpty {
                profileImageText.isHidden = true
            } else {
                let splitArray = nameString!.split(separator: " ")
                if splitArray.count == 1 {
                    let firstName = splitArray[0]
                    let conString = firstName.substring(to: firstName.index(firstName.startIndex, offsetBy: 2)).uppercased()
                    self.profileImageText.layer.masksToBounds = true
                    self.profileImageText.layer.cornerRadius = self.profileImageText.bounds.width / 2
                    profileImageText.backgroundColor = UIColor.purple
                    profileImageText.text = conString
                } else if splitArray.count == 2 {
                    let firstLetter = String(splitArray[0].first!.uppercased())
                    let secondLetter = String(splitArray[1].first!.uppercased())
                    let conString = firstLetter + secondLetter
                    self.profileImageText.layer.masksToBounds = true
                    self.profileImageText.layer.cornerRadius = self.profileImageText.bounds.width / 2
                    profileImageText.backgroundColor = UIColor.purple
                    profileImageText.text = conString
                }
            }
        } else {
            profileImageText.isHidden = true
        }
    }
    
    func setDetails() {
        self.setProfileImage()
        self.txtFieldName.text = Defaults.userName
        self.txtFieldEmail.text = Defaults.userEmail
        imageProfile.kf.setImage(with: URL(string: Defaults.userImageUrl ?? ""), placeholder: UIImage(named: "user"), options: .none) { result in
        }
    }
    
    @IBAction func editButtonAction(_ sender: Any) {
        actionsheet()
    }
    
    func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logOutButtonAction(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "", message: LOG_OUT, preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            Utility.logoutAction()
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert .dismiss(animated: true, completion: nil)
        }))
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func deleteAccountAction(){
        let refreshAlert = UIAlertController(title: "", message: DELETE_ACCOUNT, preferredStyle: UIAlertController.Style.alert)
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
            let profileOperation = ProfileOperation()
            profileOperation.deleteAccount(userId: Defaults.userId ?? "") { error, data in
                Utility.logoutAction()
            }
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert .dismiss(animated: true, completion: nil)
        }))
        self.present(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func privacycheckMarkTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    func actionsheet() {
        let alert  = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (handler) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (handler) in
            self.openGalary()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (handler) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        //        if UIImagePickerController.isSourceTypeAvailable(.camera) {
        //            let image = UIImagePickerController()
        //            image.allowsEditing = false
        //            image.delegate = self
        ////            image.sourceType = .camera
        //            image.sourceType = UIImagePickerController.SourceType.camera
        //            image.mediaTypes = [kUTTypeImage as String]
        //            self.present(image, animated: true, completion: nil)
        //        }
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerController.SourceType.camera
            image.allowsEditing = false
            self.present(image, animated: true, completion: nil)
        }else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func openGalary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let image = UIImagePickerController()
            image.allowsEditing = true
            image.delegate = self
            self.present(image, animated: true, completion: nil)
        }
    }
    
    //MARK - UITextField Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For mobile numer validation
        if textField == txtFieldPhoneNumber {
            if txtFieldPhoneNumber.text?.count ?? 0 < 17 {
                let allowedCharacters = CharacterSet(charactersIn:"+0123456789 ")
                let characterSet = CharacterSet(charactersIn: string)
                return allowedCharacters.isSuperset(of: characterSet)
            } else {
                let maxLength = 16
                let currentString: NSString = (textField.text ?? "") as NSString
                let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
        }
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        self.view.endEditing(true)
        sideMenuController?.revealMenu()
    }
    
    @IBAction func readButtonAction (_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Account", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "PrivatePolicy") as! PrivatePolicyViewController
        self.present(newViewController, animated:false, completion:nil)
    }
    
    @IBAction func saveData() {
        var style = ToastManager.shared.style
        style.backgroundColor =  self.btnLogout.tintColor
        style.messageColor = self.view.backgroundColor!
        if (txtFieldName.text ?? "").isEmpty || (txtFieldEmail.text ?? "").isEmpty || (txtFieldPhoneNumber.text ?? "").isEmpty {
            self.view.makeToast("Please fill the details first", duration: 3, position: .center, title: nil, image: nil, style: style) { didTap in
            }
            return
        }
        var imageShare: UIImage?
        if self.tempImage == nil {
            imageShare = nil
        }else{
            imageShare = imageProfile.image
        }
        self.view.endEditing(true)
        let profileOperation = ProfileOperation()
        let userDetails = ["fname": txtFieldName.text ?? "", "email" : txtFieldEmail.text ?? "", "userId" : Defaults.userId ?? "", "mobile": txtFieldPhoneNumber.text ?? ""]
        profileOperation.updateUserProfile(userDetails: userDetails as [String : Any], image: imageShare) { error, result in
            if let _ = error {
                self.view.makeToast(error, duration: 3, position: .center, title: nil, image: nil, style: style) { didTap in
                }
            } else {
                self.tempImage = nil
                let data = result?["data"] as? [String : Any] ?? [:]
                Defaults.userName = data["fname"] as? String ?? ""
                Defaults.userEmail = data["email"] as? String ?? ""
                Defaults.userPhoneNumber = data["mobile"] as? String ?? ""
                let imageArray = data["profileimage"] as? [[String : Any]] ?? []
                if !imageArray.isEmpty {
                    let imageDict = imageArray[0]
                    let imagePath = imageDict["path"] as? String ?? ""
                    let fullPathArr = imagePath.components(separatedBy: "public")
                    Defaults.userImageUrl =  "\(BaseURL)\(fullPathArr[1])"
                }
                self.view.makeToast("Your profile is updated", duration: 3, position: .center, title: nil, image: nil, style: style) { didTap in
                }
            }
        }
    }
    
    @IBAction func countryPickerAction() {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { country in
            self.countryCodeButton?.setTitle(country.dialingCode ?? "", for: UIControl.State.normal)
            self.countrycode = country.dialingCode ?? ""
        }
        countryController.flagStyle = .circular
        countryController.favoriteCountriesLocaleIdentifiers = ["IN"]
        
        if self.traitCollection.userInterfaceStyle == .dark {
            countryController.labelColor = UIColor.white
            countryController.detailColor = UIColor.white
            countryController.separatorLineColor = UIColor.lightGray
            countryController.tableView.backgroundColor = .black
            countryController.navigationController?.navigationBar.tintColor = .white
            countryController.navigationController?.navigationBar.backgroundColor = .black
        } else {
            countryController.labelColor = UIColor.black
            countryController.detailColor = UIColor.black
            countryController.separatorLineColor = UIColor.lightGray
            countryController.tableView.backgroundColor = .white
            countryController.navigationController?.navigationBar.tintColor = .black
            countryController.navigationController?.navigationBar.backgroundColor = .white
        }
    }
}

extension AccountView: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let tempImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        self.dismiss(animated: true) {
            self.profileImageText.isHidden = true
            self.imageProfile.image = tempImage
            self.tempImage = tempImage
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

