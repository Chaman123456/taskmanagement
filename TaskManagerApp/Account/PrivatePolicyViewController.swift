//
//  PrivatePolicyViewController.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 21/06/22.
//

import UIKit
import WebKit
class PrivatePolicyViewController: UIViewController , WKUIDelegate{
    @IBOutlet var backbtn : UIButton!
    @IBOutlet var  webView : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let myUrl = URL(string: "\(BaseURL)/privacypolicy")
        let myRequest = URLRequest(url: myUrl!)
        webView.load(myRequest)
        backbtn.setTitle("", for: .normal)
        if self.traitCollection.userInterfaceStyle == .dark {
            self.view.backgroundColor = UIColor.black
        } else {
            self.view.backgroundColor = UIColor.white
        }
    }
    
    @IBAction func backBtnAction(){
        self.dismiss(animated: true)
    }
    
}
