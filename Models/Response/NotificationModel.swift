
import Foundation

public class NotificationModel {
	public var status : Int?
	public var result : Array<NotificationResult>?

    public class func modelsFromDictionaryArray(array:NSArray) -> [NotificationModel]{
        var models:[NotificationModel] = []
        for item in array{
            models.append(NotificationModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
	required public init?(dictionary: NSDictionary) {
		status = dictionary["status"] as? Int
        if (dictionary["result"] != nil) { result = NotificationResult.modelsFromDictionaryArray(array: dictionary["result"] as! NSArray) }
	}


	public func dictionaryRepresentation() -> NSDictionary {
		let dictionary = NSMutableDictionary()
		dictionary.setValue(self.status, forKey: "status")
		return dictionary
	}

}

public class NotificationResult {
    public var imges : String?
    public var title : String?
    public var time : String?
    public var date : String?


    public class func modelsFromDictionaryArray(array:NSArray) -> [NotificationResult]
    {
        var models:[NotificationResult] = []
        for item in array{
            models.append(NotificationResult(dictionary: item as! NSDictionary)!)
        }
        return models
    }


    required public init?(dictionary: NSDictionary) {
        imges = dictionary["imges"] as? String
        title = dictionary["title"] as? String
        time = dictionary["time"] as? String
        date = dictionary["date"] as? String
    }

    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.imges, forKey: "imges")
        dictionary.setValue(self.title, forKey: "title")
        dictionary.setValue(self.time, forKey: "time")
        dictionary.setValue(self.date, forKey: "date")
        return dictionary
    }
}
