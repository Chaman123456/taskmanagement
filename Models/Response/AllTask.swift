//
//  AllTask.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 27/01/22.
//

import Foundation

public class AllTaskModel {
    public var upcoming : [DetailDataModel]?
    public var recent : [DetailDataModel]?
    public var later : [DetailDataModel]?
    public var today : [DetailDataModel]?
    public var completed : [DetailDataModel]?
    
    public class func modelsFromDictionary(dict:NSDictionary) -> [AllTaskModel] {
        var models:[AllTaskModel] = []
        models.append(AllTaskModel(dictionary: dict)!)
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        var resultDict = dictionary["result"] as? [String : Any] ?? [:]
        if resultDict.isEmpty {
            resultDict = dictionary["results"] as? [String : Any] ?? [:]
        }
        upcoming = DetailDataModel.modelsFromDictionaryArray(array: (resultDict["upcomming"] as? NSArray ?? []))
        recent = DetailDataModel.modelsFromDictionaryArray(array: (resultDict["recent"] as? NSArray ?? []))
        later = DetailDataModel.modelsFromDictionaryArray(array: (resultDict["later"] as? NSArray ?? []))
        today = DetailDataModel.modelsFromDictionaryArray(array: (resultDict["today"] as? NSArray ?? []))
        completed = DetailDataModel.modelsFromDictionaryArray(array: (resultDict["completed"] as? NSArray ?? []))
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.upcoming, forKey: "upcoming")
        dictionary.setValue(self.recent, forKey: "recent")
        dictionary.setValue(self.later, forKey: "later")
        dictionary.setValue(self.today, forKey: "today")
        dictionary.setValue(self.completed, forKey: "completed")
        return dictionary
    }
}


public class DetailDataModel {
    public var _id : String?
    public var listFavorite : Array<String>?
    public var assignId : [[String : Any]]?
    public var latlong_location : Array<Latlong_location>?
    public var backgroundImages : Array<Attachments>?
    public var multifilesImages : Array<Attachments>?
    public var getting_started : Bool?
    public var in_progress : Bool?
    public var isFavorite : Bool?
    public var incomplete : Bool?
    public var wont_abble_to_perform : Bool?
    public var status : Bool?
    public var notes : String?
    public var name : String?
    public var description : String?
    public var catId : String?
    public var assignDate : String?
    public var created_at : String?
    public var updated_at : String?
    public var __v : Int?
    public var category : Array<Category>?
    public var subtasks : Array<Subtasks>?
    public var history : Array<String>?
    public var date : String?
    public var changeDate : String?
    public var startDate : String?
    public var isDeleteTaskStatus : Bool?
    public var task_status : Bool?
    public var userId : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [DetailDataModel] {
        var models:[DetailDataModel] = []
        for item in array {
            models.append(DetailDataModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        _id = dictionary["_id"] as? String
        if (dictionary["listFavorite"] != nil) { listFavorite = dictionary["listFavorite"] as? Array<String> }
        
        assignId = [[String : Any]]()
        if (dictionary["assignId"] != nil) {
            assignId = dictionary["assignId"] as? [[String : Any]]
        }
        if (dictionary["latlong_location"] != nil) {
            let data = JsonSerialization.getArrayFromJsonString(arrayString: ((dictionary["latlong_location"] as! NSArray).firstObject as? String ?? ""))
            latlong_location = Latlong_location.modelsFromDictionaryArray(array: data as NSArray)
        }
        if (dictionary["attachments"] != nil) {
            let attachmentArray = dictionary["attachments"] as? [[String : Any]] ?? []
            if !attachmentArray.isEmpty {
                let attachmentDict : [String: Any] = attachmentArray[0] as [String : Any]
                if !attachmentDict.isEmpty {
                    self.backgroundImages = Attachments.modelsFromDictionaryArray(array: attachmentDict["backgroundimages"] as? NSArray ?? NSArray())
                }
                multifilesImages = Array<Attachments>()
                for attachmentDict in attachmentArray {
                    self.multifilesImages?.append(contentsOf: Attachments.modelsFromDictionaryArray(array: attachmentDict["multifiles"] as? NSArray ?? NSArray()))
                }
            }
        }
        getting_started = dictionary["getting_started"] as? Bool
        in_progress = dictionary["in_progress"] as? Bool
        isFavorite = dictionary["isFavorite"] as? Bool
        incomplete = dictionary["incomplete"] as? Bool
        wont_abble_to_perform = dictionary["wont_abble_to_perform"] as? Bool
        status = dictionary["status"] as? Bool
        notes = dictionary["notes"] as? String
        name = dictionary["name"] as? String
        description = dictionary["description"] as? String
        catId = dictionary["catId"] as? String
        assignDate = dictionary["assignDate"] as? String
        created_at = dictionary["created_at"] as? String
        updated_at = dictionary["updated_at"] as? String
        __v = dictionary["__v"] as? Int
        startDate = dictionary["startDate"] as? String
        isDeleteTaskStatus = dictionary["isDeleteTaskStatus"] as? Bool
        task_status = dictionary["task_status"] as? Bool
        userId = dictionary["userId"] as? String
        if (dictionary["category"] != nil) { category = Category.modelsFromDictionaryArray(array: dictionary["category"] as! NSArray) }
        if (dictionary["subtasks"] != nil) { subtasks = Subtasks.modelsFromDictionaryArray(array: dictionary["subtasks"] as! NSArray) }
        if (dictionary["history"] != nil) { history = dictionary["history"] as? Array<String> }
        date = dictionary["date"] as? String
        changeDate = dictionary["changeDate"] as? String
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self._id, forKey: "_id")
        dictionary.setValue(self.getting_started, forKey: "getting_started")
        dictionary.setValue(self.in_progress, forKey: "in_progress")
        dictionary.setValue(self.isFavorite, forKey: "isFavorite")
        dictionary.setValue(self.incomplete, forKey: "incomplete")
        dictionary.setValue(self.wont_abble_to_perform, forKey: "wont_abble_to_perform")
        dictionary.setValue(self.isDeleteTaskStatus, forKey: "isDeleteTaskStatus")
        dictionary.setValue(self.task_status, forKey: "task_status")
        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.notes, forKey: "notes")
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.description, forKey: "description")
        dictionary.setValue(self.userId, forKey: "userId")
        dictionary.setValue(self.catId, forKey: "catId")
        dictionary.setValue(self.assignDate, forKey: "assignDate")
        dictionary.setValue(self.startDate, forKey: "startDate")
        dictionary.setValue(self.created_at, forKey: "created_at")
        dictionary.setValue(self.updated_at, forKey: "updated_at")
        dictionary.setValue(self.__v, forKey: "__v")
        dictionary.setValue(self.date, forKey: "date")
        dictionary.setValue(self.changeDate, forKey: "changeDate")
        return dictionary
    }
}

public class Attachments {
    public var fieldname : String?
    public var originalname : String?
    public var encoding : String?
    public var mimetype : String?
    public var destination : String?
    public var filename : String?
    public var path : String?
    public var size : Int?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Attachments] {
        var models:[Attachments] = []
        for item in array {
            models.append(Attachments(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        fieldname = dictionary["fieldname"] as? String
        originalname = dictionary["originalname"] as? String
        encoding = dictionary["encoding"] as? String
        mimetype = dictionary["mimetype"] as? String
        destination = dictionary["destination"] as? String
        filename = dictionary["filename"] as? String
        path = dictionary["path"] as? String
        size = dictionary["size"] as? Int
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.fieldname, forKey: "fieldname")
        dictionary.setValue(self.originalname, forKey: "originalname")
        dictionary.setValue(self.encoding, forKey: "encoding")
        dictionary.setValue(self.mimetype, forKey: "mimetype")
        dictionary.setValue(self.destination, forKey: "destination")
        dictionary.setValue(self.filename, forKey: "filename")
        dictionary.setValue(self.path, forKey: "path")
        dictionary.setValue(self.size, forKey: "size")
        return dictionary
    }
}

public class Category {
    public var _id : String?
    public var name : String?
    public var description : String?
    public var published : Bool?
    public var createdAt : String?
    public var updatedAt : String?
    public var __v : Int?
    public class func modelsFromDictionaryArray(array:NSArray) -> [Category] {
        var models:[Category] = []
        for item in array {
            models.append(Category(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        _id = dictionary["id"] as? String
        if _id == nil{
            _id = dictionary["_id"] as? String
        }
        name = dictionary["name"] as? String
        description = dictionary["description"] as? String
        published = dictionary["published"] as? Bool
        createdAt = dictionary["createdAt"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        __v = dictionary["__v"] as? Int
        
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self._id, forKey: "_id")
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.description, forKey: "description")
        dictionary.setValue(self.published, forKey: "published")
        dictionary.setValue(self.createdAt, forKey: "createdAt")
        dictionary.setValue(self.updatedAt, forKey: "updatedAt")
        dictionary.setValue(self.__v, forKey: "__v")
        return dictionary
    }
}

public class Latlong_location {
    public var latitude : String?
    public var logitude : String?
    public var address : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Latlong_location]{
        var models:[Latlong_location] = []
        for item in array {
            models.append(Latlong_location(dictionary: item as? NSDictionary ?? [:])!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        latitude = "\(dictionary["latitude"] as? Double ?? 0.0)"
        logitude = "\(dictionary["logitude"] as? Double ?? 0.0)"
        address = dictionary["address"] as? String
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.latitude, forKey: "latitude")
        dictionary.setValue(self.logitude, forKey: "logitude")
        dictionary.setValue(self.address, forKey: "address")
        return dictionary
    }
}

public class Subtasks {
    public var _id : String?
    public var assignId : [[String : Any]]?
    public var subtask_images : Array<Subtask_images>?
    public var backgroundImages : Array<Attachments>?
    public var multifilesImages : Array<Attachments>?
    public var isDeleteSubtaskStatus : Bool?
    public var subtaskStatus : Bool?
    public var sub_getting_started : Bool?
    public var sub_in_progress : Bool?
    public var sub_isFavorite : Bool?
    public var sub_incomplete : Bool?
    public var sub_wont_abble_to_perform : Bool?
    public var taskId : String?
    public var sub_notes : String?
    public var sub_name : String?
    public var sub_latlong_location : Array<Latlong_location>?
    public var sub_assignDate : String?
    public var sub_created_at : String?
    public var sub_updated_at : String?
    public var __v : Int?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Subtasks]{
        var models:[Subtasks] = []
        for item in array {
            models.append(Subtasks(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        _id = dictionary["_id"] as? String
        assignId = [[String : Any]]()
        if (dictionary["sub_assignId"] != nil) {
            let array = dictionary["sub_assignId"] as? [String] ?? []
            if !array.isEmpty {
                let assignIdArray = JsonSerialization.getArrayFromJsonString(arrayString: array[0])
                for item in assignIdArray {
                    assignId?.append(item)
                }
            } else {
                let assignIdArray = dictionary["sub_assignId"] as? [[String : Any]] ?? []
                for item in assignIdArray {
                    assignId?.append(item)
                }
            }
        }
        if (dictionary["sub_latlong_location"] != nil) {
            sub_latlong_location =
            Latlong_location.modelsFromDictionaryArray(array: dictionary["sub_latlong_location"] as! NSArray)
        }
        if (dictionary["subtask_attachment"] != nil) {
            let attachmentArray = dictionary["subtask_attachment"] as? [[String : Any]] ?? []
            if !attachmentArray.isEmpty {
                let attachmentDict : [String: Any] = attachmentArray[0] as [String : Any]
                if !attachmentDict.isEmpty {
                    self.backgroundImages = Attachments.modelsFromDictionaryArray(array: attachmentDict["subtask_images"] as? NSArray ?? NSArray())
                }
                multifilesImages = Array<Attachments>()
                for attachmentDict in attachmentArray {
                    self.multifilesImages?.append(contentsOf: Attachments.modelsFromDictionaryArray(array: attachmentDict["multifiles"] as? NSArray ?? NSArray()))
                }
            }
        }
        
        isDeleteSubtaskStatus = dictionary["isDeleteSubtaskStatus"] as? Bool
        subtaskStatus = dictionary["subtaskStatus"] as? Bool
        sub_getting_started = dictionary["sub_getting_started"] as? Bool
        sub_in_progress = dictionary["sub_in_progress"] as? Bool
        sub_isFavorite = dictionary["sub_isFavorite"] as? Bool
        sub_incomplete = dictionary["sub_incomplete"] as? Bool
        sub_wont_abble_to_perform = dictionary["sub_wont_abble_to_perform"] as? Bool
        taskId = dictionary["taskId"] as? String
        sub_notes = dictionary["sub_notes"] as? String
        sub_name = dictionary["sub_name"] as? String
        sub_assignDate = dictionary["sub_assignDate"] as? String
        sub_created_at = dictionary["sub_created_at"] as? String
        sub_updated_at = dictionary["sub_updated_at"] as? String
        __v = dictionary["__v"] as? Int
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self._id, forKey: "_id")
        dictionary.setValue(self.isDeleteSubtaskStatus, forKey: "isDeleteSubtaskStatus")
        dictionary.setValue(self.subtaskStatus, forKey: "subtaskStatus")
        dictionary.setValue(self.sub_getting_started, forKey: "sub_getting_started")
        dictionary.setValue(self.sub_in_progress, forKey: "sub_in_progress")
        dictionary.setValue(self.sub_isFavorite, forKey: "sub_isFavorite")
        dictionary.setValue(self.sub_incomplete, forKey: "sub_incomplete")
        dictionary.setValue(self.sub_wont_abble_to_perform, forKey: "sub_wont_abble_to_perform")
        dictionary.setValue(self.taskId, forKey: "taskId")
        dictionary.setValue(self.sub_notes, forKey: "sub_notes")
        dictionary.setValue(self.sub_name, forKey: "sub_name")
        dictionary.setValue(self.sub_latlong_location, forKey: "sub_latlong_location")
        dictionary.setValue(self.sub_assignDate, forKey: "sub_assignDate")
        dictionary.setValue(self.sub_created_at, forKey: "sub_created_at")
        dictionary.setValue(self.sub_updated_at, forKey: "sub_updated_at")
        dictionary.setValue(self.sub_latlong_location, forKey: "sub_latlong_location")
        dictionary.setValue(self.assignId, forKey: "sub_assignId")
        dictionary.setValue(self.__v, forKey: "__v")
        return dictionary
    }
    
}

public class Subtask_images {
    public var fieldname : String?
    public var originalname : String?
    public var encoding : String?
    public var mimetype : String?
    public var destination : String?
    public var filename : String?
    public var path : String?
    public var size : Int?
    public class func modelsFromDictionaryArray(array:NSArray) -> [Subtask_images]{
        var models:[Subtask_images] = []
        for item in array{
            models.append(Subtask_images(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        fieldname = dictionary["fieldname"] as? String
        originalname = dictionary["originalname"] as? String
        encoding = dictionary["encoding"] as? String
        mimetype = dictionary["mimetype"] as? String
        destination = dictionary["destination"] as? String
        filename = dictionary["filename"] as? String
        path = dictionary["path"] as? String
        size = dictionary["size"] as? Int
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.fieldname, forKey: "fieldname")
        dictionary.setValue(self.originalname, forKey: "originalname")
        dictionary.setValue(self.encoding, forKey: "encoding")
        dictionary.setValue(self.mimetype, forKey: "mimetype")
        dictionary.setValue(self.destination, forKey: "destination")
        dictionary.setValue(self.filename, forKey: "filename")
        dictionary.setValue(self.path, forKey: "path")
        dictionary.setValue(self.size, forKey: "size")
        return dictionary
    }
}
