

import Foundation
import CoreData

class User {
    public var userName: String?
    public var userId: String?
    public var phoneNumber: String?
    public var email = String()
    public var imageUrl = String()
    public var isContect = Bool()
    public var assigneeStatus = String()
}

class Info {
    public var newText = String()
    public var context = String()
    public var postType = String()
    public var id = String()
}
