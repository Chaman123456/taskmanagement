//
//  OTPModel.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 14/01/22.
//

import Foundation

public class OTPModel {
    public var success : Bool?
    public var message : String?
    public var otp : String?
    public var token : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [OTPModel] {
        var models:[OTPModel] = []
        for item in array {
            models.append(OTPModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
        message = dictionary["message"] as? String
        otp = dictionary["otp"] as? String
        token = dictionary["token"] as? String
    }

    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.success, forKey: "status")
        dictionary.setValue(self.message, forKey: "message")
        dictionary.setValue(self.otp, forKey: "otp")
        dictionary.setValue(self.token, forKey: "token")
        return dictionary
    }
}
