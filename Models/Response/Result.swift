
import Foundation
 
public class Result {
    public var success : Bool?
	public var userId : Int?
	public var email : String?
	public var firstName : String?
	public var lastName : String?
	public var profileImage : String?
	public var role : String?
	public var token : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [Result] {
        var models:[Result] = []
        for item in array {
            models.append(Result(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
		userId = dictionary["userId"] as? Int
		email = dictionary["email"] as? String
		firstName = dictionary["firstName"] as? String
		lastName = dictionary["lastName"] as? String
		profileImage = dictionary["profileImage"] as? String
		role = dictionary["role"] as? String
		token = dictionary["token"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()
        dictionary.setValue(self.success, forKey: "success")
		dictionary.setValue(self.userId, forKey: "userId")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.firstName, forKey: "firstName")
		dictionary.setValue(self.lastName, forKey: "lastName")
		dictionary.setValue(self.profileImage, forKey: "profileImage")
		dictionary.setValue(self.role, forKey: "role")
		dictionary.setValue(self.token, forKey: "token")
		return dictionary
	}

}
