
import Foundation
 
public class UserLoginModel {
	public var success : Bool?
	public var message : String?
	public var token : String?
    public var id : String?
    public var name : String?
    public var email : String?
    public var imageUrl : String?
    public var mobile : String?
    public var userId : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [UserLoginModel] {
        var models:[UserLoginModel] = []
        for item in array {
            models.append(UserLoginModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {
        success = dictionary["success"] as? Bool
		message = dictionary["message"] as? String
        token = dictionary["token"] as? String
        userId = dictionary["userId"] as? String
        let data = dictionary["data"] as? [String : Any] ?? [:]
        id = data["_id"] as? String
        name = data["fname"] as? String
        email = data["email"] as? String
        mobile = data["mobile"] as? String
        let imageArray = data["profileimage"] as? [[String : Any]] ?? []
        if !imageArray.isEmpty {
            let imageDict = imageArray[0]
            let imagePath = imageDict["path"] as? String ?? ""
            let fullPathArr = imagePath.components(separatedBy: "public")
            imageUrl = "\(BaseURL)\(fullPathArr[1])"
        }
        
	}

	public func dictionaryRepresentation() -> NSDictionary {
		let dictionary = NSMutableDictionary()
		dictionary.setValue(self.success, forKey: "success")
		dictionary.setValue(self.message, forKey: "message")
		dictionary.setValue(self.token, forKey: "token")
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.mobile, forKey: "mobile")
        dictionary.setValue(self.mobile, forKey: "userId")
		return dictionary
	}
}
