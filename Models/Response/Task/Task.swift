//
//  Task.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 18/01/22.
//

import Foundation
import UIKit

class Task {
    public var name = String()
    public var description = String()
    public var categoryIds = [Category]()
    public var notes = String()
    public var assignedDate = String()
    public var location = [[String : Any]]()
    public var assignedUser = [[String : Any]]()
    public var attachments = [String]()
    public var image: UIImage?
    public var arrUser = [User]()
    public var selIndex = Int()
}
