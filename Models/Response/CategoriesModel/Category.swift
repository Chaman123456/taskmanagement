//
//  Category.swift
//  TaskManagerApp
//
//  Created by Rajesh Kumar on 18/01/22.
//

import Foundation

class Category {
    public var name = String()
    public var description = String()
    public var published = String()
    public var createdAt = String()
    public var updatedAt = String()
    public var id = String()
}
