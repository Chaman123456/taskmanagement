//
//  ContactDetail.swift
//
//  TaskManagerApp
//
//  Created by Chaman Sharma on 07/12/21.
//
import Foundation

struct ContactDetail {
    let detailType: ContactDetailType
    let detailLabel: String
    let detailValue: String
}

enum ContactDetailType {
    case number
    case address
    case email
    case birthDate
}
