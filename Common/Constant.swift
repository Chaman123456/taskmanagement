//
//  Constant.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 07/12/21.
//

import UIKit

var DeviceId : String = UIDevice.current.identifierForVendor!.uuidString
let AppVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String
let BundleID = Bundle.main.bundleIdentifier

let Button_Height : CGFloat = 50.0
let Button_Width : CGFloat = 180.0
let Button_Font_Size : CGFloat = 18.0
let Title_Font_Size : CGFloat = 19.0

var Helvetica_Bold = "Helvetica-Bold"
var Metropolis_Medium_Font = "Metropolis-Medium"
var Metropolis_SemiBold_Font = "Metropolis-SemiBold"

var Device_Token : String = ""
var selectedTabIndex : Int = 0
var isFromMenu = false

var my_date_format = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

#if DEBUG  
let AppMode = "DevelopmentMode"
let BaseURL : String = "https://mydigitalassets.in"
let IsLogEnabled : Bool = true
#else
let AppMode = "ProductionMode"
let BaseURL : String = "https://mydigitalassets.in"
let IsLogEnabled : Bool = false
#endif

#if arch(i386) || arch(x86_64)
let IsSimulator = true
#else
let IsSimulator = false
#endif

class Constant: NSObject {
    static let googleClientID: String = "170665834985-g1tupdb9t60oon3r8dmt19sf8n76hi26.apps.googleusercontent.com"
}
