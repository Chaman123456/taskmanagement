//
//  Enum.swift
//
//  Created by Chaman Sharma on 24/04/22.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

enum MediaType: String {
    case video = "video"
    case image = "image"
    case pdf = "pdf"
}

enum MediaStorageType: String {
    case local = "local"
    case server = "server"
}


enum AssigneeStatus: String {
    case invited = "Invited"
    case pending = "Pending"
    case accepted = "Accepted"
    case declined = "Declined"
}
