//
//  StringConstant.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 09/12/21.
//

import UIKit

let Phone_Number_Not_Valid_String_Constant = "Entered Phone number is not valid."

var LOG_OUT : String {
    get {
        return "Are you sure you want to Logout?"
    }
}

var ADD_PHONE : String {
    get {
        return "Phone number is mendatory to create task, please add phone number first."
    }
}

var DELETE_ACCOUNT : String {
    get {
        return "Are you sure you want to Delete your account? This will permanently erase your account."
    }
}

var EMAIL : String {
    get {
        return "Please enter Email Address."
    }
}

var LOGIN_STR : String {
    get {
        return "Login"
    }
}

var LOGIN_TO_YOUR_ACCOUNT_STR : String {
    get {
        return "Login to your account."
    }
}


var INTERNET_NOT_AVAILABLE_STR : String {
    get {
        return "Internet is not Available."
    }
}

var SOMETHING_WENT_WRONG_STR : String {
    get {
        return "Something went wrong."
    }
}

var DO_YOU_WANT_TO_LOGOUT : String {
    get {
        return "Do you want to logout?"
    }
}

var PLEASE_ATTACH_COVER_IMAGE : String {
    get {
        return "Please attach cover image."
    }
}

var PLEASE_ENTER_THE_TASK_NAME : String {
    get {
        return "Please enter the name of task."
    }
}

var PLEASE_ENTER_THE_CATEGORY : String {
    get {
        return "Please enter the category."
    }
}

var PLEASE_ENTER_THE_ASSIGN_DATE : String {
    get {
        return "Please enter the assign date."
    }
}

var PLEASE_ASSIGN_USER : String {
    get {
        return "Please assign user."
    }
}

var ERROR_OCCURRED : String {
    get {
        return "Error Occurred"
    }
}


var ADD_ROLE_STR : String {
    get {
        return "Add Role"
    }
}

var OTP : String {
    get {
        return "Please enter valid activation code."
    }
}
var RESEND_OTP : String {
    get {
        return "Activation code sent successfully."
    }
}

var LEAVE_TASK_TEXT : String {
    get {
        return "Would you like to leave this task?"
    }
}

var TASK_ADDED : String {
    get {
        return "Task added successfully!"
    }
}
var TASK_UPDATED : String {
    get {
        return "Task updated successfully!"
    }
}

var TASK_DELETED : String {
    get {
        return "Task deleted successfully!"
    }
}

var USER_ALREADR_ADDED : String {
    get {
        return "User already added."
    }
}

var DELETE_ASSIGNED_USER : String {
    get {
        return "Are you sure you want to delete?"
    }
}

var NOT_AUTHORISED : String {
    get {
        return "You are not authorised to edit this task."
    }
}

var DELETE_ASSETS : String {
    get {
        return "Would you like to delete this attachment?"
    }
}
