//
//  GlobalData.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 26/01/22.
//

import UIKit

class GlobalData: NSObject {
    static var allCategories = [Category]()
    static var allUsers = [User]()
    static var allTask = [DetailDataModel]()
    static var assignedUser = [String]()
}
