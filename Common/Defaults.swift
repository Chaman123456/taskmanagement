//
//  Defaults.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 07/12/21.


import UIKit

class Defaults {
    static var Token: String? {
        get {
            return UserDefaults.standard.object(forKey: "Token") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "Token")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var userPhoneNumber: String? {
        get {
            return UserDefaults.standard.object(forKey: "UserPhoneNumber") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "UserPhoneNumber")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var userId: String? {
        get {
            return UserDefaults.standard.object(forKey: "UserId") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "UserId")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var isListView: Bool? {
        get {
            return UserDefaults.standard.object(forKey: "ListView") as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "ListView")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var isFirstTimeLogin: Bool? {
        get {
            return UserDefaults.standard.object(forKey: "LoginFirstTime") as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "LoginFirstTime")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var userName: String? {
        get {
            return UserDefaults.standard.object(forKey: "UserName") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "UserName")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var userEmail: String? {
        get {
            return UserDefaults.standard.object(forKey: "UserEmail") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "UserEmail")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var isUserLoggedIn: Bool? {
        get {
            return UserDefaults.standard.object(forKey: "IsUserLoggedIn") as? Bool ?? false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "IsUserLoggedIn")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var userImageUrl: String? {
        get {
            return UserDefaults.standard.object(forKey: "userImageUrl") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "userImageUrl")
            UserDefaults.standard.synchronize()
        }
    }
    
    static func getFormattedDate(string: String , formatter:String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd, yyyy"
        let date: Date? = dateFormatterGet.date(from: string)
        return dateFormatterPrint.string(from: date!);
    }
    
    static func getFormattedDateStr(string: String , formatter:String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = formatter
        let date: Date? = dateFormatterGet.date(from: string)
        return dateFormatterPrint.string(from: date!);
    }
    
    static var CatId: String? {
           get {
               return UserDefaults.standard.object(forKey: "CatId") as? String ?? ""
           }
           set {
               UserDefaults.standard.set(newValue, forKey: "CatId")
               UserDefaults.standard.synchronize()
           }
       }
    
    static var sortBy: Int? {
        get {
            return UserDefaults.standard.object(forKey: "sortBy") as? Int ?? 1
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "sortBy")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var filterBy: Int? {
        get {
            return UserDefaults.standard.object(forKey: "filterBy") as? Int ?? 1
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "filterBy")
            UserDefaults.standard.synchronize()
        }
    }
    static var APNSTokenSave: String? {
        get {
            if let previousAppVersion = UserDefaults.standard.object(forKey: "apnstoken") as? String {
                return previousAppVersion
            }
            return ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "apnstoken")
            UserDefaults.standard.synchronize()
        }
    }
    static var recentCategoryTypeOne: String? {
        get {
            return UserDefaults.standard.object(forKey: "CategoryTypeOne") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "CategoryTypeOne")
            UserDefaults.standard.synchronize()
        }
    }
    
    static var recentCategoryTypeTwo: String? {
        get {
            return UserDefaults.standard.object(forKey: "CategoryTypeTwo") as? String ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "CategoryTypeTwo")
            UserDefaults.standard.synchronize()
        }
    }
    
}
