//
//  CategoryScroll.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 12/12/21.
//

import UIKit
protocol CategoryScrollProtocol {
    func categoryChoosed(selectedItemName: String)
}

class CategoryScroll: UIScrollView {
    var categoryScrollDelegate: CategoryScrollProtocol?
    var labelArray = [UILabel]()
    var selectedItem: Int?
    var itemArray = [Category]()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupScroll(itemArray: [Category]) {
        for view in self.subviews {
            view.removeFromSuperview()
        }
        labelArray.removeAll()
        self.itemArray = itemArray
        
        var xPosition : CGFloat = 10.0;
        if itemArray.count < 1{
            return
        }
        for i in 0...itemArray.count - 1  {
            let itemLabel = UILabel()
            itemLabel.frame = CGRect(x: xPosition, y: 0.0, width: 0, height: self.frame.size.height)
            itemLabel.textAlignment = NSTextAlignment.center
            itemLabel.textColor = UIColor.white
            itemLabel.layer.borderColor = UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
            itemLabel.layer.borderWidth = 1.0
            itemLabel.numberOfLines = 0
            itemLabel.lineBreakMode = .byWordWrapping
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 0.92
            let displayItem = "    " + (itemArray[i].name ?? "") + "    "
            itemLabel.attributedText = NSMutableAttributedString(string: displayItem, attributes: [NSAttributedString.Key.kern: 1, NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font: FontUtility.getFont(with: FontUtility.FontType.display, withTrait: FontUtility.TraitType.Regular, ofSize: FontUtility.FontSize.custom(18))])
            itemLabel.sizeToFit()
            itemLabel.frame = CGRect(x: itemLabel.frame.origin.x, y: itemLabel.frame.origin.y, width: itemLabel.frame.size.width, height: self.frame.size.height)
            itemLabel.layer.cornerRadius =  10//self.frame.size.height / 2.0
            self.addSubview(itemLabel)
            labelArray.append(itemLabel)
            
            let button = UIButton(frame: CGRect(x: xPosition, y: 0.0, width: itemLabel.frame.size.width, height: self.frame.size.height))
            button.tag = i
            button.addTarget(self, action: #selector(selectCategoryAction(sender:)), for: UIControl.Event.touchUpInside)
            self.addSubview(button)

            xPosition = xPosition + itemLabel.frame.size.width + 8
        }
        self.contentSize = CGSize(width: xPosition, height: 0)
        self.updateItem(selectedValue: selectedItem ?? 0)
    }
    
    @objc func selectCategoryAction(sender : UIButton) {
        if !(self.itemArray[sender.tag].name ?? "").contains("+Show All")  {
            self.updateItem(selectedValue: sender.tag)
        }
        categoryScrollDelegate?.categoryChoosed(selectedItemName: self.itemArray[sender.tag].name ?? "")
    }
    
    func updateItem(selectedValue: Int) {
        if !labelArray.isEmpty {
            for index in 0...labelArray.count - 1  {
                if selectedValue == index {
                    labelArray[index].textColor = UIColor(named: "Black50")
                    labelArray[index].layer.backgroundColor = UIColor.white.cgColor
                    labelArray[index].layer.borderColor = UIColor.clear.cgColor
                } else {
                    labelArray[index].textColor = UIColor.white
                    labelArray[index].layer.backgroundColor = UIColor.clear.cgColor
                        if self.traitCollection.userInterfaceStyle == .dark {
                            labelArray[index].layer.borderColor = UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
                        } else {
                            labelArray[index].layer.borderColor = UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
                            labelArray[index].textColor = UIColor(named: "Black50")
                    }
                }
            }
        }
    }
}
