//
//  GetAllTasksOperation.swift
//  TaskManagerApp
//
//  Created by Rajesh Kumar on 17/01/22.
//

import Foundation

class GetAllTasksOperation {
    
    let webRequest = BaseWebService()
    let endPoint = EndPoints()
    
    init() {
    }
    
    func execute(outerClosure: @escaping ((String?, AllTaskModel?) -> ())) -> () {
        webRequest.processRequestUsingGetMethod(url: "\(BaseURL)\(endPoint.getAllTask)/\(Defaults.userId ?? "")", parameters: nil, showLoader: true) { error, val, result, statusCode   in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let taskDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, AllTaskModel.init(dictionary: taskDetails as NSDictionary))
        }
    }
    
    func deleteTaskAttachment(taskId: String, fieldName: String, fileName : String, outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.deleteAttachment)/\(taskId)/\(fieldName)/\(fileName)", parameters: nil, showLoader: true, contentType: ContentType.json) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, result as? [String : Any] ?? [:])
        }
    }
    
    func updateAssignStatus(newAssigneeStatus: String, assigneeId: String, outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        let param = ["newAssigneeStatus": newAssigneeStatus,
                     "assigneeId": assigneeId]
        webRequest.processRequestUsingPutMethod(url: "\(BaseURL)\(endPoint.updateAssignStatus)/\(Defaults.userId ?? "")", parameters: param) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, result as? [String : Any] ?? [:])
        }
    }
}
