//
//  SearchOperation.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 31/01/22.
//

import Foundation

class SearchOperation: NSObject {
    let webRequest = BaseWebService()
    
    let endPoint = EndPoints()
    func executeSearch(userDetail: [String: Any], outerClosure: @escaping ((String?, AllTaskModel?) -> ())) -> () {
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.search)", parameters: userDetail, showLoader: true, contentType: .json) { error, val, result, statusCode   in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, AllTaskModel(dictionary: userDetails as NSDictionary))
        }
    }
}
    
