//
//  OTPOperation.swift
//  TaskManagerApp
//
//  Created by Rajesh Kumar on 14/01/22.
//

import Foundation

class OTPOperation {
    private var otp: OTPData
    let webRequest = BaseWebService()
    let endPoint = EndPoints()
    init(withOTPDetails: OTPData) {
        self.otp = withOTPDetails
    }
    func executeGetOTP(outerClosure: @escaping ((String?, OTPModel?) -> ())) -> () {
        var accountCreatedVia = "Mobile"
        if self.otp.phoneNumber.isEmpty {
            accountCreatedVia = "Email"
        }
        let otpDetails = [
            "accountCreatedVia": accountCreatedVia,
            "mobile": self.otp.phoneNumber,
            "email": self.otp.email
        ]
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.GetOtp)", parameters: otpDetails, showLoader: true, contentType: .json) { error, val, result, statusCode   in
            print(result ?? "")
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let otpDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, OTPModel(dictionary: otpDetails as NSDictionary))
        }
    }
}
