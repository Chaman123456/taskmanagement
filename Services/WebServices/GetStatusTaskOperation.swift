//
//  GetStatusTaskOperation.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 10/02/22.
//

import Foundation

class GetStatusTaskOperation: NSObject {
    let webRequest = BaseWebService()
    let endPoint = EndPoints()
    
    func getTaskStatus(parameters : [String: Any], outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.taskStatus)", parameters: parameters, showLoader: true, contentType: .json) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let responseData = (result as? [String: Any]) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, responseData)
        }
    }
}

