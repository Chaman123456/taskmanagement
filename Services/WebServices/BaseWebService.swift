//
//  BaseWebService.swift
//  DesignApp
//
//  Created by Chaman Sharma on 13/04/22.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

struct EndPoints {
    let GetOtp: String = "/api/getotp"
    let LogIn: String = "/api/login"
    let getCategories = "/category/published"
    let createNewTask = "/create-task"
    let getAllTask = "/get-all-task"
    let updateTask = "/update-task"
    let search = "/search"
    let createSubTask = "/create-sub-task"
    let statusTask = "/get-status-task"
    let completedTask = "/completed-task"
    let getFavTasks = "/get-all-favorite"
    let makeFavTask = "/create-favorite"
    let createCategory = "/category"
    let deleteTask = "/delete-task"
    let taskStatus = "/get-status-task"
    let subTaskStatus = "/get-status-subtask"
    let updateSubTask = "/update-sub-task"
    let deleteSubTask = "/delete-sub-task"
    let getListNofification = "/get-list-nofification"
    let deleteCategory = "/category"
    let deleteMultipleCat = "/category/mutipleDelete"
    let deleteUserAccount : String = "/delete-user-account"
    let deleteAttachment = "/delete-task-attachment"
    let updateAssignStatus = "/update-assign-status"
    let socialLogin = "/auth/login"
    let getUserDetils = "/get-user-detils"
    let updateUser = "/update-user"
    let getAllUser = "/get-all-user"
}

enum ContentType: String {
    case multipart = "multipart/form-data"
    case json = "application/json"
    case urlEncoded  = "application/x-www-form-urlencoded"
    case none = ""
}

class BaseWebService {
    let reachabilityManager = NetworkReachabilityManager()
    func setLoader() {
        var config : SwiftLoader.Config = SwiftLoader.Config()
        config.size = 100
        config.spinnerColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        config.foregroundColor = .black
        config.foregroundAlpha = 0.5
        SwiftLoader.setConfig(config: config)
    }
    
    func getHeader(contentType: ContentType = .json) ->  [String: String] {
        var header: [String: String] = [:]
        if !(Defaults.Token ?? "").isEmpty {
            header["token"] = "\(Defaults.Token ?? "")"
        }
        if contentType == .multipart {
            header["Content-Type"] = "multipart/form-data"
        } else if contentType == .json  {
            header["Content-Type"] = "application/json"
        } else if contentType == .urlEncoded  {
            header["Content-Type"] = "application/x-www-form-urlencoded"
        } else if contentType == .none {
        }
        return header
    }
    
    func processRequestUsingPostMethod(url: String, parameters: [String: Any]?, showLoader: Bool, contentType: ContentType, closure:@escaping (String?, Bool?, Any?, Int?) -> ()) {
        setLoader()
        if !reachabilityManager!.isReachable {
            closure(INTERNET_NOT_AVAILABLE_STR, false, nil, nil)
            return
        }
        if showLoader == true {
            SwiftLoader.show(title: "Loading...", animated: true)
        }
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders(getHeader(contentType: contentType)))
            .responseJSON { response in
                SwiftLoader.hide()
                if let statusCode = response.response?.statusCode {
                    if (statusCode == 401) {
                        let dict = response.value as? [String : Any] ?? [:]
                        closure(dict["message"] as? String ?? ERROR_OCCURRED, false, nil, response.response?.statusCode)
                        Utility.tokenExpiredAction()
                        return
                    }
                    if statusCode == 200 || statusCode == 201 {
                        closure(nil, true, response.value, statusCode)
                    } else {
                        let dict = response.value as? [String : Any] ?? [:]
                        closure(dict["message"] as? String ?? ERROR_OCCURRED, false, dict, statusCode)
                    }
                } else {
                    closure(SOMETHING_WENT_WRONG_STR, false, nil, response.response?.statusCode)
                }
            }
    }
    
    func processRequestUsingGetMethod(url: String, parameters: [String: Any]?, showLoader: Bool,  closure:@escaping (String?, Bool?, Any?, Int?) -> ()) {
        setLoader()
        if !reachabilityManager!.isReachable {
            closure(INTERNET_NOT_AVAILABLE_STR, false, nil, nil)
            return
        }
        if showLoader {
            DispatchQueue.main.async() { 
                SwiftLoader.show(title: "Loading...", animated: true)
            }
        }
        AF.request(url, method: .get, parameters: parameters, headers: HTTPHeaders(getHeader()))
            .responseJSON { response in
                SwiftLoader.hide()
                if let statusCode = response.response?.statusCode {
                    if (statusCode == 401) {
                        Utility.tokenExpiredAction()
                        return
                    }
                    if statusCode == 200 || statusCode == 201 {
                        closure(nil, true, response.value, statusCode)
                    } else {
                        let dict = response.value as? [String : Any] ?? [:]
                        closure(dict["message"] as? String ?? ERROR_OCCURRED, false, dict, statusCode)
                    }
                } else {
                    closure(SOMETHING_WENT_WRONG_STR, false, nil, response.response?.statusCode)
                }
            }
    }
    
    func processRequestUsingPutMethod(url: String, parameters: [String: Any]?,  closure:@escaping (String?, Bool?, Any?, Int?) -> ()) {
        if !reachabilityManager!.isReachable {
            closure(INTERNET_NOT_AVAILABLE_STR, false, nil, nil)
            return
        }
        AF.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: HTTPHeaders(getHeader(contentType: .json))) .responseJSON { response in
            if let statusCode = response.response?.statusCode {
                if (statusCode == 401) {
                    Utility.tokenExpiredAction()
                    return
                }
                if statusCode == 200 || statusCode == 201 {
                    closure(nil, true, response.value, statusCode)
                } else {
                    let dict = response.value as? [String : Any] ?? [:]
                    closure(dict["message"] as? String ?? ERROR_OCCURRED, false, dict, statusCode)
                }
            } else {
                closure(SOMETHING_WENT_WRONG_STR, false, nil, response.response?.statusCode)
            }
        }
    }
    
    func processRequestUsingDeleteMethod(url: String, parameters: [String: Any]?,  closure:@escaping (String?, Bool?, Any?, Int?) -> ()) {
        if !reachabilityManager!.isReachable {
            closure(INTERNET_NOT_AVAILABLE_STR, false, nil, nil)
            return
        }
        AF.request(url, method: .delete, parameters: parameters, headers: HTTPHeaders(getHeader()))
            .responseJSON { response in
                if let statusCode = response.response?.statusCode {
                    if (statusCode == 401) {
                        Utility.tokenExpiredAction()
                        return
                    }
                    if statusCode == 200 || statusCode == 201 {
                        closure(nil, true, response.value, statusCode)
                    } else {
                        let dict = response.value as? [String : Any] ?? [:]
                        closure(dict["message"] as? String ?? ERROR_OCCURRED, false, dict, statusCode)
                    }
                } else {
                    closure(SOMETHING_WENT_WRONG_STR, false, nil, response.response?.statusCode)
                }
            }
    }
    
    func multipartUpload(url: String, parameters: [[String: Any]]?, isEdit: Bool = false,  closure:@escaping (String?, Bool?, Any?, Int?) -> ()) {
        if !reachabilityManager!.isReachable {
            closure(INTERNET_NOT_AVAILABLE_STR, false, nil, nil)
            return
        }
        var httpMethod = HTTPMethod(rawValue: "POST")
        if isEdit {
            httpMethod =  HTTPMethod(rawValue: "PUT")
        }
        AF.upload(multipartFormData: { multiPart in
            for dict in parameters! {
                if let temp = dict["value"] as? String {
                    multiPart.append(temp.data(using: .utf8)!, withName: dict["key"] as! String)
                }
                if (dict["type"] as! String).contains(MediaType.image.rawValue) {
                    let image = UIImage(contentsOfFile: Utility.getMediaDirectoryPath().appendingPathComponent(dict["value"]  as! String)!.absoluteString)
                    let data = image!.jpegData(compressionQuality: 0.5)
                    multiPart.append(data!, withName: dict["key"] as! String, fileName: dict["value"] as? String ?? "", mimeType: "image/png")
                }
                if (dict["type"] as! String).contains(MediaType.pdf.rawValue) {
                    do {
                        let path = "file://" + Utility.getMediaDirectoryPath().appendingPathComponent(dict["value"]  as! String)!.absoluteString
                        let data = try Data(contentsOf: URL(string: path)!)
                        multiPart.append(data, withName: dict["key"] as! String, fileName: dict["value"] as? String ?? "", mimeType: "application/pdf")
                    } catch {
                        print(error)
                    }
                }
                if (dict["type"] as! String).contains(MediaType.video.rawValue)  {
                    do {
                        let path = "file://" + Utility.getMediaDirectoryPath().appendingPathComponent(dict["value"]  as! String)!.absoluteString
                        let videodata = try Data(contentsOf: URL(string: path)!, options: Data.ReadingOptions.alwaysMapped)
                        multiPart.append(videodata, withName: dict["key"] as! String, fileName: dict["value"] as? String ?? "", mimeType: "video/mp4")
                    } catch {
                        print(error)
                    }
                }
            }
        }, to: url, method: httpMethod , headers: HTTPHeaders(getHeader(contentType: .multipart)))
            .uploadProgress(queue: .main, closure: { progress in
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { data in
                let dict = data.value as? [String : Any] ?? [:]
                if(data.response!.statusCode == 200) {
                    closure(dict["message"] as? String ?? "", true, nil, data.response!.statusCode)
                } else {
                    closure(dict["message"] as? String ?? "", false, nil, data.response!.statusCode)
                }
            })
    }
    
    func multipartUploadImagewithParam(url: String, parameters: [String: Any]?, image: UIImage?, multupleImages: [UIImage], isUpdate : Bool = false, imageFieldName : String, closure:@escaping (String?, Bool?, Any?, Int?) -> ()) {
        if !reachabilityManager!.isReachable {
            closure(INTERNET_NOT_AVAILABLE_STR, false, nil, nil)
            return
        }
        setLoader()
        SwiftLoader.show(title: "Loading...", animated: true)
        var httpMethod = HTTPMethod(rawValue: "POST")
        if isUpdate {
            httpMethod = HTTPMethod(rawValue: "PUT")
        }
        AF.upload(multipartFormData:{
            (multipartFormData) in
            if let _ = image {
                multipartFormData.append((image?.jpegData(compressionQuality: 0.5)!)!, withName: imageFieldName, fileName: "file.jpeg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            for img in multupleImages {
                multipartFormData.append((img.jpegData(compressionQuality: 0.5)!), withName: "multifiles", fileName: "\(Utility.getUniqueName()).jpeg", mimeType: "image/jpeg")
            }
            print(multipartFormData)
        }, to: url, method: httpMethod , headers: HTTPHeaders(getHeader(contentType: .multipart)))
            .uploadProgress(queue: .main, closure: { progress in
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { response in
                SwiftLoader.hide()
                if let statusCode = response.response?.statusCode {
                    if (statusCode == 401) {
                        Utility.tokenExpiredAction()
                        return
                    }
                    if statusCode == 200 || statusCode == 201 {
                        closure(nil, true, response.value, statusCode)
                    } else {
                        let dict = response.value as? [String : Any] ?? [:]
                        closure(dict["message"] as? String ?? ERROR_OCCURRED, false, dict, statusCode)
                    }
                } else {
                    closure(SOMETHING_WENT_WRONG_STR, false, nil, response.response?.statusCode)
                }
            })
       }
}
