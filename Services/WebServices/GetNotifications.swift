//
//  GetNotifications.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 02/03/22.
//

import Foundation

class GetNotifications: NSObject {
    let webRequest = BaseWebService()
    let endPoint = EndPoints()
    
    func getNotifications(outerClosure: @escaping ((String?, NotificationModel?) -> ())) -> () {
        webRequest.processRequestUsingGetMethod(url: "\(BaseURL)\(endPoint.getListNofification)/\(Defaults.userId ?? "")", parameters: nil, showLoader: true) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let responseData = (result as? [String: Any]) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, NotificationModel(dictionary: responseData as NSDictionary))
        }
    }
}
