//
//  SignUpOperation.swift
//  ReplyMe
//
//  Created by chaman Sharma on 25/01/21.
//

import Foundation
import UIKit

class LoginOperation {
    let webRequest = BaseWebService()
    let endPoint = EndPoints()
    
    func executeLogin(userDetail: [String: Any], outerClosure: @escaping ((String?, UserLoginModel?) -> ())) -> () {
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.LogIn)", parameters: userDetail, showLoader: true, contentType: .json) { error, val, result, statusCode in
            print(result ?? "")
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, UserLoginModel(dictionary: userDetails as NSDictionary))
        }
    }
    
    func executeSocialLogin(detailData: [String:Any], outerClosure: @escaping ((String?, UserLoginModel?) -> ())) -> () {
       
        let info = [
            "data": detailData
        ]
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.socialLogin)", parameters: info, showLoader: true, contentType: .json) { error, val, result, statusCode   in
            print(result ?? "")
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, UserLoginModel(dictionary: userDetails as NSDictionary))
        }
    }
}
