//
//  CompletedTaskOperation.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 01/02/22.
//

import Foundation

class CompletedTaskOperation: NSObject {
    let webRequest = BaseWebService()
    
    let endPoint = EndPoints()
    
    func executeCompletedTask(outerClosure: @escaping ((String?, AllTaskModel?) -> ())) -> () {
        let dict = ["userId": Defaults.userId]
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.completedTask)", parameters: dict as [String : Any], showLoader: true, contentType: .json) { error, val, result, statusCode   in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, AllTaskModel(dictionary: userDetails as NSDictionary))
        }
    }
}
    
