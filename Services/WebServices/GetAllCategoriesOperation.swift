//
//  GetAllCategoriesOperation.swift
//  TaskManagerApp
//
//  Created by Chaman on 17/01/22.
//

import Foundation

class GetAllCategoriesOperation {
    let webRequest = BaseWebService()
    let endPoint = EndPoints()
    var showLoader : Bool = true
    
    init() {
    }
    
    func execute(outerClosure: @escaping ((String?, [Category]?) -> ())) -> () {
        webRequest.processRequestUsingGetMethod(url: "\(BaseURL)\(endPoint.getCategories)", parameters: nil, showLoader: self.showLoader) { error, val, result, statusCode   in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let categories = (result as? [[String: Any]]) else {
                outerClosure(error, nil)
                return
            }
            var allCategories = [Category]()
            for dict in categories {
                let category = Category(dictionary: (dict as NSDictionary))
                allCategories.append(category!)
            }
            outerClosure(nil, allCategories)
        }
    }
    
    func createCategory(parameters : [String: Any], outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.createCategory)", parameters: parameters, showLoader: self.showLoader, contentType: .json) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let responseData = (result as? [String: Any]) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, responseData)
        }
    }
    
    func deleteTask(taskId : String, outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        webRequest.processRequestUsingPutMethod(url: "\(BaseURL)\(endPoint.deleteTask)/\(taskId)", parameters: nil) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let responseData = (result as? [String: Any]) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, responseData)
        }
    }
    func deleteCategory(categoryId : String, outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        webRequest.processRequestUsingDeleteMethod(url: "\(BaseURL)\(endPoint.deleteCategory)/\(categoryId)", parameters: nil) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let responseData = (result as? [String: Any]) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, responseData)
        }
    }
    
    func deleteMultipleCategory(parameters : [String: Any], outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.deleteMultipleCat)", parameters: parameters, showLoader: self.showLoader, contentType: .json) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let responseData = (result as? [String: Any]) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, responseData)
        }
    }
}
