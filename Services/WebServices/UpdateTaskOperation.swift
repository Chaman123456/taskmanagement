//
//  UpdateTaskOperation.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 31/01/22.
//

import Foundation
import UIKit


class UpdateTaskOperation {
    private var task: Task
    let webRequest = BaseWebService()
    let endPoint = EndPoints()
    var image = UIImage()
    var arrimage = [[String: Any]]()

    init(withTask taskDetails: Task, image: UIImage, arrimage : [[String: Any]]) {
        self.task = taskDetails
        self.image = image
        self.arrimage = arrimage
    }
    
    func execute(taskId: String, outerClosure: @escaping ((String?, UserLoginModel?) -> ())) -> () {
        var locationString = "[]"
        if !self.task.location.isEmpty {
            locationString = JsonSerialization.getJsonString(array: self.task.location)
        }
        var assignIdString = ""
        if !self.task.assignedUser.isEmpty {
            assignIdString = JsonSerialization.getJsonString(array: self.task.assignedUser)
        }
        let params = [
            "userId" : Defaults.userId ?? "",
            "assignId": assignIdString,
            "notes": self.task.notes,
            "name": task.name,
            "description": task.description,
            "catId": task.categoryIds[0]._id ?? "",
            "assignDate" : task.assignedDate,
            "location" : locationString,
            "status" : "1",
            "isFavirate" : "0"
        ] as [String : Any]
        let attachedImage = arrimage.map({ $0["Info"] }) as? [UIImage] ?? []
        webRequest.multipartUploadImagewithParam(url: "\(BaseURL)\(endPoint.updateTask)/\(taskId)", parameters: params, image: self.image, multupleImages: attachedImage, isUpdate: true, imageFieldName: "backgroundimages") { error, val,result,statusCode   in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, UserLoginModel(dictionary: userDetails as NSDictionary))
        }
    }
}
