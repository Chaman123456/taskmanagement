//
//  CreateTaskOperation.swift
//  TaskManagerApp
//
//  Created by Chaman  on 18/01/22.
//

import Foundation
import UIKit

class CreateTaskOperation {
    private var task: Task
    let webRequest = BaseWebService()
    let endPoint = EndPoints()
    var image: UIImage?
    var arrimage = [[String: Any]]()

    init(withTask taskDetails: Task, image: UIImage?, arrimage: [[String: Any]]) {
        self.task = taskDetails
        self.image = image
        self.arrimage = arrimage
    }
    
    func execute(outerClosure: @escaping ((String?, UserLoginModel?, Bool?) -> ())) -> () {
        var locationString = ""
        if !self.task.location.isEmpty {
            locationString = JsonSerialization.getJsonString(array: self.task.location)
        }
        var assignIdString = ""
        if !self.task.assignedUser.isEmpty {
            assignIdString = JsonSerialization.getJsonString(array: self.task.assignedUser)
        }
        
        let params = [
            "userId" : Defaults.userId ?? "",
            "assignId": assignIdString,
            "notes": self.task.notes,
            "name": task.name,
            "description": task.description,
            "catId": task.categoryIds[0]._id ?? "",
            "assignDate" : task.assignedDate,
            "location" : locationString,
            "status" : "1",
            "isFavirate" : "0"
        ] as [String : Any]
        
        let attachedImage = arrimage.map({ $0["Info"] }) as? [UIImage] ?? []
        webRequest.multipartUploadImagewithParam(url: "\(BaseURL)\(endPoint.createNewTask)", parameters: params, image: self.image,  multupleImages: attachedImage, imageFieldName: "backgroundimages") { error, val, result, statusCode   in
            guard error == nil else {
                outerClosure(error, nil, false)
                return
            }
            guard let userDetails = result as? [[String: Any]] else {
                outerClosure(error, nil, false)
                return
            }
            outerClosure(nil, UserLoginModel(dictionary: userDetails[0] as NSDictionary), true)
        }
    }
    
    func executeSubTask(taskId: String,outerClosure: @escaping ((String?, UserLoginModel?) -> ())) -> () {
        var assignIdString = ""
        var locationString = ""
        if !self.task.location.isEmpty {
            locationString = JsonSerialization.getJsonString(array: self.task.location)
        }
        if !self.task.assignedUser.isEmpty {
            assignIdString = JsonSerialization.getJsonString(array: self.task.assignedUser)
        }
        let params = [
            "userId" : Defaults.userId ?? "",
            "sub_notes": task.description,
            "sub_name": task.name,
            "sub_assignDate": task.assignedDate,
            "sub_assignId": assignIdString,
            "sub_latlong_location": locationString
        ] as [String : Any]
        let attachedImage = arrimage.map({ $0["Info"] }) as? [UIImage] ?? []
        webRequest.multipartUploadImagewithParam(url: "\(BaseURL)\("\(endPoint.createSubTask)/\(taskId)")", parameters: params, image: self.image, multupleImages: attachedImage, imageFieldName: "subtask_images") { error, val, result, statusCode   in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, UserLoginModel(dictionary: userDetails as NSDictionary))
        }
    }
    
    func updateSubTask(taskId: String,outerClosure: @escaping ((String?, UserLoginModel?) -> ())) -> () {
        var assignIdString = ""
        if !self.task.assignedUser.isEmpty {
            assignIdString = JsonSerialization.getJsonString(array: self.task.assignedUser)
        }
        var locationString = ""
        if !self.task.location.isEmpty {
            locationString = JsonSerialization.getJsonString(array: self.task.location)
        }
        let params = [
            "userId" : Defaults.userId ?? "",
            "sub_notes": task.description,
            "sub_name": task.name,
            "sub_assignDate": task.assignedDate,
            "sub_assignId": assignIdString,
            "sub_latlong_location": locationString
        ] as [String : Any]
        let attachedImage = arrimage.map({ $0["Info"] }) as? [UIImage] ?? []
        webRequest.multipartUploadImagewithParam(url: "\(BaseURL)\("\(endPoint.updateSubTask)/\(taskId)")", parameters: params, image: self.image, multupleImages: attachedImage, isUpdate: true, imageFieldName: "subtask_images") { error, val, result, statusCode   in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, UserLoginModel(dictionary: userDetails as NSDictionary))
        }
    }
    
    
    func deleteSubTask(subtaskId: String,outerClosure: @escaping ((String?, UserLoginModel?) -> ())) -> () {
        webRequest.processRequestUsingDeleteMethod(url: "\(BaseURL)\(endPoint.deleteSubTask)/\(subtaskId)", parameters: nil) { error, val, result, statusCode   in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, UserLoginModel(dictionary: userDetails as NSDictionary))
        }
    }
}
