//
//  ProfileOperation.swift
//  TaskManagerApp
//
//  Created by Chaman Sharma on 24/01/22.
//

import UIKit

class ProfileOperation: NSObject {
    let webRequest = BaseWebService()
    let endPoint = EndPoints()

    func updateUserProfile(userDetails: [String: Any], image: UIImage?, outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        webRequest.multipartUploadImagewithParam(url: "\(BaseURL)\(endPoint.updateUser)", parameters: userDetails, image: image, multupleImages: [], isUpdate: true, imageFieldName: "profileimage") { error, val, result, statusCode   in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, userDetails)
        }
    }
    
    func getUserDetails(userId: String, outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        webRequest.processRequestUsingGetMethod(url: "\(BaseURL)\(endPoint.getUserDetils)/\(userId)", parameters: nil, showLoader: false) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, userDetails)
        }
    }
    
    func getAllUsers(outerClosure: @escaping ((String?, [User]?) -> ())) -> () {
        webRequest.processRequestUsingGetMethod(url: "\(BaseURL)\(endPoint.getAllUser)", parameters: nil, showLoader: false) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let usersData = (result as? [[String: Any]]) else {
                outerClosure(error, nil)
                return
            }
            let utility = Utility()
            var userArray = [User]()
            for userDict in usersData {
                let user = User()
                user.userName = utility.getActualValue(key: "fname", dict: userDict)
                user.userId = utility.getActualValue(key: "_id", dict: userDict)
                user.phoneNumber = utility.getActualValue(key: "mobile", dict: userDict)
                user.email = utility.getActualValue(key: "email", dict: userDict)
                user.imageUrl = utility.getActualValue(key: "image", dict: userDict)
                userArray.append(user)
            }
            outerClosure(nil, userArray)
        }
    }
    
    func deleteAccount(userId: String, outerClosure: @escaping ((String?, [String: Any]?) -> ())) -> () {
        webRequest.processRequestUsingDeleteMethod(url: "\(BaseURL)\(endPoint.deleteUserAccount)/\(userId)", parameters: nil) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, userDetails)
        }
    }
}
