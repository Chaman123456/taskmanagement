//
//  FavoriteTaskOperation.swift
//  TaskManagerApp
//
//  Created by Apple on 01/02/22.
//

import Foundation

class FavoriteTaskOperation: NSObject {
    let webRequest = BaseWebService()
    let endPoint = EndPoints()
    
    func createTaskFavorite(isFavorite: Bool, taskId: String, outerClosure: @escaping ((String?, AllTaskModel?, Bool) -> ())) -> () {
        let param = ["userid": Defaults.userId ?? "", "isFavorite": isFavorite ? "1" : "0"]
        webRequest.processRequestUsingPostMethod(url: "\(BaseURL)\(endPoint.makeFavTask)/\(taskId)", parameters: param as [String : Any], showLoader: true, contentType: .json) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil, false)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil, false)
                return
            }
            outerClosure(nil, AllTaskModel(dictionary: userDetails as NSDictionary), true)
        }
    }
    
    func getFavoriteTask(outerClosure: @escaping ((String?, AllTaskModel?) -> ())) -> () {
        webRequest.processRequestUsingGetMethod(url: "\(BaseURL)\(endPoint.getFavTasks)/\(Defaults.userId ?? "")", parameters: nil, showLoader: true) { error, val, result, statusCode in
            guard error == nil else {
                outerClosure(error, nil)
                return
            }
            guard let userDetails = (result as? Dictionary<String, Any>) else {
                outerClosure(error, nil)
                return
            }
            outerClosure(nil, AllTaskModel(dictionary: userDetails as NSDictionary))
        }
    }
}
